package com.pragma.sapi.shiro;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;

import java.util.Date;

import org.apache.shiro.subject.Subject;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.KeyStoreManager;
import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.datasource.FlatFileConnection;
import com.pragma.sapi.core.directory.JndiLdapContextFactory;
import com.pragma.sapi.core.services.auth.SapiIdentityRepositoryImpl;

public class SapiShiroTest extends AbstractSapiShiroTest {

    private static final Logger log = LoggerFactory.getLogger(SapiShiroTest.class);

    @Test
    public void testSimple() {
        // 1. Create a mock authenticated Subject instance for the test to run:
        Subject subjectUnderTest = createNiceMock(Subject.class);
        expect(subjectUnderTest.isAuthenticated()).andReturn(true);

        // 2. Bind the subject to the current thread:
        setSubject(subjectUnderTest);

        // perform test logic here. Any call to
        // SecurityUtils.getSubject() directly (or nested in the
        // call stack) will work properly.
    }

    @Test
    public void sapiIdentityRepository() {
        // Configuración del directorio TDS (EMPRESAS)
        JndiLdapContextFactory contextFactoryTds = new JndiLdapContextFactory();
        contextFactoryTds.setUrl("ldap://vaxtpmde09.proteccion.com.co:389");
        contextFactoryTds.setSystemUsername("cn=gjmartin");
        contextFactoryTds.setSystemPassword("3408hjdfj!");
        contextFactoryTds.setPoolingEnabled(true);

        // Configuración del manejador de llavero tiene un Java Key Store con un cert y clave privata para firmar JWTs
        KeyStoreManager keyStoreManager = new KeyStoreManager();
        keyStoreManager.setKeyStoreFile("classpath:certificado/sapi-demo.jks");
        keyStoreManager.setKeyStorePassword("sapisecretkey");

        // Configuracion de la base de datos por referencia a un datasource
        // JNDIConnection jndiConnection = new JNDIConnection();
        // jndiConnection.setResRefName("java:comp/env/jdbc/SapiDS");
        FlatFileConnection flatFileConnection = new FlatFileConnection();
        flatFileConnection.setFilePath("C:/temp/");

        SecuredApplication securedApplication = new SecuredApplication();
        securedApplication.setName("sapi-demo-spring-boot");
        securedApplication.setAllowOrigin("*");
        securedApplication.setGenericDataSource(flatFileConnection);
        securedApplication.setCertificatePassword("sapisecretkey");

        SapiIdentityRepositoryImpl sapiIdentityRepository = new SapiIdentityRepositoryImpl();
        sapiIdentityRepository.setLdapContextFactory(contextFactoryTds);
        sapiIdentityRepository.setSearchBase("o=portalpruebas,c=co");
        sapiIdentityRepository.setPrincipalSuffix("cn=%s,ou=usuarios,o=portalpruebas,c=co");
        sapiIdentityRepository.setPatternGroup("*");
        sapiIdentityRepository.setSecuredApplication(securedApplication);
        sapiIdentityRepository.setKeyStoreManager(keyStoreManager);

        SapiEnvironment.setSecuredApplication(securedApplication);
    }

    @Test
    public void testArguments() {
        log.info(String.format("\"timestamp\": %d,", new Date().getTime()));
        log.info(String.format("{ \'session\': %s, \'eventType\': \'%s\' }", "session", "SESSION_CREATED"));
        log.info(String.format("{ \'session\': %s, \'eventType\': \'%s\' }", "session", "SESSION_CLOSED"));
        log.info(String.format(
                "\'requestAndRolesDTO\': { \'app\': \'%s\', \'url\':\'%s\', \'method\':\'%s\', \'command\':\'%s\', \'roles\': [", "appl",
                "/url", "POST", "Command"));
        log.info(String.format("Obteniendo restricciones para [\'%s\' : \'%s\' : \'%s\']", "requestAndRolesDTO.getApp()",
                "requestAndRolesDTO.getMethod()", "requestAndRolesDTO.getUrl()"));
        log.info(String.format("{ \"error\": %s, \"mensaje\": \"%s\" }", "400", "Test Arguments"));
        log.info(String.format("{ \"error\": %d, \"mensaje\": \"%s\" }", Integer.valueOf("500"), "Test Arguments"));
        log.info(String.format(" --- Verificar filtro comando [ %s != null ] = %s", "command", String.valueOf("/command")));
        log.info(String.format(" --- La restriccion seleccionada es:\n\n%s\n", "url/match"));
        log.info(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro regexpr", "tmpOp1", "op2", Boolean.TRUE));
        log.info(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro de roles", "tmpRoles", "roles2", Boolean.TRUE));
        log.info(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro de evento", "op1", "op2", Boolean.TRUE));

    }

    @After
    public void tearDownSubject() {
        // 3. Unbind the subject from the current thread:
        clearSubject();
    }
}
