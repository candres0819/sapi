package com.pragma.sapi.shiro.realm;

import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;

public class SapiShiroAutenticationInfo extends SimpleAuthenticationInfo {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String identificacion;

    public SapiShiroAutenticationInfo(Object principal, Object credentials, String realmName) {
        super(principal, credentials, realmName);
    }

    public SapiShiroAutenticationInfo(PrincipalCollection principals, Object credentials) {
        super(principals, credentials);
    }

    public String getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}