package com.pragma.sapi.shiro.services.usecase;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.shiro.services.usecase.SearchUseCase.Request;
import com.pragma.sapi.shiro.services.usecase.SearchUseCase.Response;
import com.pragma.sapi.shiro.services.usecase.settings.BeanableMethodUseCase;

public class SearchUseCase extends BeanableMethodUseCase<Request, Response> {

    private static final Logger log = LoggerFactory.getLogger(SearchUseCase.class);

    protected void executeUseCase(Request requestValues) {
        log.debug("Se ejecuta el caso para realizar consultas");
        setUpBeans();

        String username = requestValues.username;
        if ((null == username) || ("".equals(username))) {
            getUseCaseCallback().onError(400, new AuthenticationException("No hay datos para la consulta."));
            return;
        }

        Map<String, SapiIdentityRepository> sapiIdentityRepositories = this.getSapiIdentityRepositories();
        SapiIdentityRepository idRepository = (SapiIdentityRepository) sapiIdentityRepositories.get("LdapAuthenticationRealm");

        log.debug(String.format("validUserName \'%s\' ", username));
        UserProperties userProperties = idRepository.userName(username);
        userProperties.setIpAddress(requestValues.ipAddress);

        getUseCaseCallback().onSuccess(new Response(getStandardResponse(userProperties, "Consulta Exitosa")));
    }

    public static StandardResponse getStandardResponse(UserProperties userProperties, String mensaje) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("userProperties", userProperties);

        StandardResponse standardResponse = new StandardResponse();
        StandardResponse.Body body = new StandardResponse.Body();
        body.setData(data);

        standardResponse.setBody(body);
        standardResponse.setHeader(new StandardResponse.Header(200, mensaje));

        return standardResponse;
    }

    public static final class Request implements UseCase.RequestValues {

        private final String username;
        private final String ipAddress;

        public Request(String username, String ipAddress) {
            this.username = username;
            this.ipAddress = ipAddress;
        }
    }

    public static final class Response implements UseCase.ResponseValue {

        public final StandardResponse standardResponse;

        public Response(StandardResponse response) {
            this.standardResponse = response;
        }
    }
}
