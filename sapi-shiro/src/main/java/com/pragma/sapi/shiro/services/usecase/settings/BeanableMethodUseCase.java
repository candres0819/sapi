package com.pragma.sapi.shiro.services.usecase.settings;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pragma.sapi.core.model.GroupInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.Realm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.directory.DirectoryFacade;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.shiro.realm.SapiRealm;
import com.pragma.sapi.shiro.services.usecase.UseCase;
import com.pragma.sapi.shiro.services.usecase.UseCase.RequestValues;
import com.pragma.sapi.shiro.services.usecase.UseCase.ResponseValue;
import com.pragma.sapi.token.ObjectJWT;

public abstract class BeanableMethodUseCase<Q extends RequestValues, P extends ResponseValue> extends UseCase<Q, P> implements Beanable {

    private static final Logger log = LoggerFactory.getLogger(BeanableMethodUseCase.class);

    private Map<String, SapiIdentityRepository> sapiIdentityRepositoryMap;
    private SecuredApplication securedApplication;

    public SecuredApplication getSecuredApplication() {
        return this.securedApplication;
    }

    public Map<String, SapiIdentityRepository> getSapiIdentityRepositories() {
        return this.sapiIdentityRepositoryMap;
    }

    protected List<String> determineRoles(ObjectJWT objectJWT) {
        log.info("determineRoles");
        @SuppressWarnings("unchecked")
        List<String> roles = (List<String>) objectJWT.getClaims().get("roles");
        if (null == roles) {
            roles = this.queryRoles(objectJWT.getSubject(), (String) objectJWT.getClaims().get("canonicalNameAuth"));
        }

        return roles;
    }

    protected List<String> queryRoles(String subject, String canonicalRealm) {
        List<String> roles = null;
        DirectoryFacade df = (DirectoryFacade) this.getSapiIdentityRepositories().get(canonicalRealm);

        if (null != df) {
            roles = (List<String>) df.getUserRoles(subject).stream().map(GroupInfo::getDisplayName).collect(Collectors.toList());
        }

        return roles;
    }

    public void setUpBeans() {
        Collection<Realm> realms = ((RealmSecurityManager) SecurityUtils.getSecurityManager()).getRealms();
        if (null == this.sapiIdentityRepositoryMap) {
            this.sapiIdentityRepositoryMap = new HashMap<String, SapiIdentityRepository>();
        }

        realms.stream().filter((realm) -> realm instanceof SapiRealm).forEach((realm) -> {
            String realmName = realm.getName();
            this.sapiIdentityRepositoryMap.put(realmName, ((SapiRealm) realm).getSapiIdentityRepository());
            if (null == this.securedApplication) {
                this.securedApplication = ((SapiRealm) realm).getSapiIdentityRepository().getSecuredApplication();
            }
        });
    }
}