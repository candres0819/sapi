package com.pragma.sapi.shiro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class ClientIPResolver {

    private static Pattern PATTERN_IPV4 = Pattern.compile(
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    private static Pattern PATTERN_IPV6 = Pattern.compile(
            "(::|(([a-fA-F0-9]{1,4}):){7}(([a-fA-F0-9]{1,4}))|(:(:([a-fA-F0-9]{1,4})){1,6})|((([a-fA-F0-9]{1,4}):){1,6}:)|((([a-fA-F0-9]{1,4}):)(:([a-fA-F0-9]{1,4})){1,6})|((([a-fA-F0-9]{1,4}):){2}(:([a-fA-F0-9]{1,4})){1,5})|((([a-fA-F0-9]{1,4}):){3}(:([a-fA-F0-9]{1,4})){1,4})|((([a-fA-F0-9]{1,4}):){4}(:([a-fA-F0-9]{1,4})){1,3})|((([a-fA-F0-9]{1,4}):){5}(:([a-fA-F0-9]{1,4})){1,2}))$");

    public static String getClientIpAddr(ServletRequest request) {
        return getClientIpAddr((HttpServletRequest) request);
    }

    public static String getClientIpAddr(HttpServletRequest request) {
        List<String> targetHeaders = new ArrayList<String>(Arrays.asList(new String[] { "X-Forwarded-For", "Proxy-Client-IP",
                "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR", "remote_addr" }));
        List<String> targetValues = new ArrayList<String>();

        Enumeration<?> headersDelRequest = request.getHeaderNames();
        if (headersDelRequest != null) {
            Collections.list(headersDelRequest).forEach((h) -> {
                if (targetHeaders.contains(h)) {
                    targetValues.add(request.getHeader((String) h));
                }
            });
        }

        Optional<String> posibleIP = targetValues.stream().filter(StringUtils::isNotBlank).filter(ClientIPResolver::validarFormatoDireccion)
                .findFirst();
        return posibleIP.map(s -> (String) s).orElseGet(request::getRemoteAddr);
    }

    public static boolean validarFormatoDireccion(String ip) {
        return PATTERN_IPV4.matcher(ip).matches() || PATTERN_IPV6.matcher(ip).matches();
    }
}
