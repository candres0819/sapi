package com.pragma.sapi.shiro.services.usecase;

import java.util.Map;

import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.KeyStoreManager;
import com.pragma.sapi.core.exception.SapiConfigurationException;
import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.StandardResponse.Header;
import com.pragma.sapi.core.repository.impl.DataRepository;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.shiro.services.usecase.UseCase.RequestValues;
import com.pragma.sapi.shiro.services.usecase.UseCase.ResponseValue;
import com.pragma.sapi.shiro.services.usecase.settings.BeanableMethodUseCase;
import com.pragma.sapi.shiro.token.TokenOfAuthHeader;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.ValidatorToken.Valid;
import com.pragma.sapi.token.firm.Firm;

/**
 * 
 * @author carlos.cardona
 *
 */
public class LogoutTokenUseCase extends BeanableMethodUseCase<LogoutTokenUseCase.Request, LogoutTokenUseCase.Response> {

    private static final Logger log = LoggerFactory.getLogger(LogoutTokenUseCase.class);

    public LogoutTokenUseCase() {
        super();
    }

    protected void executeUseCase(LogoutTokenUseCase.Request requestValues) {
        String token = requestValues.token;
        Boolean closeAllSessions = requestValues.closeAllSessions;

        if (null != token && !"".equals(token)) {
            this.setUpBeans();

            try {
                ObjectJWT objectJWTValidated = this.validarFirmaDeJwt(token);
                if (null != objectJWTValidated) {
                    DataRepository repo = this.getSecuredApplication().getDataRepository();
                    if (closeAllSessions.booleanValue()) {
                        repo.closeAllActiveSessions(this.getSecuredApplication().getName(), objectJWTValidated.getSubject());
                        log.debug("Sesiones activas de {} en {} cerradas.", objectJWTValidated.getSubject(),
                                this.getSecuredApplication().getName());
                    } else {
                        repo.closeSession(objectJWTValidated.getUuid());
                        log.debug("Sesion {} de {} cerrada.", objectJWTValidated.getUuid(), objectJWTValidated.getSubject());
                    }

                    this.getUseCaseCallback().onSuccess(new LogoutTokenUseCase.Response(this.responseSuccess()));
                } else {
                    this.getUseCaseCallback().onError(400,
                            new AuthenticationException("No se puede cerrar Sesion. El token no es valido."));
                }
            } catch (Exception e) {
                log.error("Error", e);
                this.getUseCaseCallback().onError(500, new AuthenticationException("Error en servidor"));
            }
        } else {
            log.warn("No se presentaron las credenciales para cerrar sesion.");
            this.getUseCaseCallback().onError(400, new AuthenticationException("Credenciales no presentadas"));
        }
    }

    /**
     * 
     * @return
     */
    private StandardResponse responseSuccess() {
        StandardResponse result = new StandardResponse();
        result.setHeader(new Header(200, "Cerrado Sesion. Operacion exitosa"));
        return result;
    }

    /**
     * 
     * @param token
     * @return
     */
    private ObjectJWT validarFirmaDeJwt(String token) {
        try {
            ObjectJWT jwtParsed = TokenOfAuthHeader.getTokenParsed(token);
            log.debug(String.format("Claims [%s]", jwtParsed.getClaims()));

            Object claim = jwtParsed.getClaims().get("canonicalNameAuth");
            if (null == claim) {
                claim = "LdapAuthenticationRealm";
            }

            Map<String, SapiIdentityRepository> sapiIdentityRepositories = this.getSapiIdentityRepositories();
            SapiIdentityRepository idRepository = (SapiIdentityRepository) sapiIdentityRepositories.get(claim);

            KeyStoreManager ksm = idRepository.getKeyStoreManager();

            Firm<?> firma = ksm.getPrivateCert(this.getSecuredApplication().getName(),
                    this.getSecuredApplication().getCertificatePassword());

            if (null == firma) {
                throw new SapiConfigurationException("No es posible validar el token, no hay configurada una firma privada");
            } else {
                ObjectJWT jwtValidated = (new Valid(firma, token)).getObjectJWT();

                if (null != jwtValidated) {
                    log.debug(String.format("Cerrando sesion de usuario [%s]", jwtValidated.getSubject()));
                    return jwtValidated;
                } else {
                    log.warn("El token presentado no ha pasado la validacion de firma.");
                    return null;
                }
            }
        } catch (Exception e) {
            log.error("Error", e);
            return null;
        }
    }

    public static class Response implements ResponseValue {

        public final StandardResponse standardResponse;

        public Response(StandardResponse response) {
            this.standardResponse = response;
        }
    }

    public static class Request implements RequestValues {

        private final String token;
        private final boolean closeAllSessions;

        public Request(String token, boolean closeAllSessions) {
            this.token = token;
            this.closeAllSessions = closeAllSessions;
        }
    }
}
