package com.pragma.sapi.shiro;

import javax.servlet.ServletContextEvent;

import org.apache.shiro.web.env.EnvironmentLoaderListener;
import org.apache.shiro.web.env.IniWebEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.schedule.SapiSchedulingService;

public class SapiEnvironmentLoaderListener extends EnvironmentLoaderListener {

    private static final Logger log = LoggerFactory.getLogger(SapiEnvironmentLoaderListener.class);

    public void contextInitialized(ServletContextEvent sce) {
        super.contextInitialized(sce);
        this.establecerSecuredApplication(sce);
        this.startScheduler();
    }

    protected void establecerSecuredApplication(ServletContextEvent sce) {
        IniWebEnvironment env = (IniWebEnvironment) sce.getServletContext().getAttribute(ENVIRONMENT_ATTRIBUTE_KEY);
        SecuredApplication app = (SecuredApplication) env.getObject("securedApplication", SecuredApplication.class);
        SapiEnvironment.setSecuredApplication(app);
    }

    protected void startScheduler() {
        try {
            SapiSchedulingService.startService(SapiEnvironment.getSecuredApplication());
        } catch (Exception e) {
            log.error("Error iniciando scheduler", e);
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        SapiSchedulingService.stopService();
        super.contextDestroyed(sce);
    }
}