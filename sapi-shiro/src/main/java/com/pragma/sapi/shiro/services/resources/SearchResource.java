package com.pragma.sapi.shiro.services.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.shiro.ClientIPResolver;
import com.pragma.sapi.shiro.services.usecase.SearchUseCase;
import com.pragma.sapi.shiro.services.usecase.UseCase;
import com.pragma.sapi.shiro.services.usecase.UseCaseHandler;

@Path("/sapi/search")
@Produces({ "application/json" })
public class SearchResource {

    private StandardResponse result;

    public SearchResource() {
        super();
    }

    @GET
    public Response search(@QueryParam("username") String username, @Context HttpServletRequest request) {
        UseCaseHandler.getInstance().execute(new SearchUseCase(),
                new SearchUseCase.Request(username, ClientIPResolver.getClientIpAddr(request)),
                new UseCase.UseCaseCallback<SearchUseCase.Response>() {
                    @Override
                    public void onSuccess(SearchUseCase.Response response) {
                        SearchResource.this.result = response.standardResponse;
                    }

                    @Override
                    public void onError(int status, RuntimeException e) {
                        SearchResource.this.result = new StandardResponse();
                        SearchResource.this.result.setHeader(new StandardResponse.Header(status, e.getMessage()));
                    }
                });
        if (this.result.getHeader().getStatus() == 200) {
            return Response.ok(this.result).build();
        }
        return Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
    }
}