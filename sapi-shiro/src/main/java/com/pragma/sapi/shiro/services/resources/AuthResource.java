package com.pragma.sapi.shiro.services.resources;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.pragma.sapi.core.exception.SapiAuthenticationException;
import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.UserLogin;
import com.pragma.sapi.shiro.ClientIPResolver;
import com.pragma.sapi.shiro.services.usecase.LoginTokenUseCase;
import com.pragma.sapi.shiro.services.usecase.LoginUserAndPasswordUseCase;
import com.pragma.sapi.shiro.services.usecase.UseCase;
import com.pragma.sapi.shiro.services.usecase.UseCaseHandler;

@Path("/sapi/auth")
@Produces({ "application/json" })
public class AuthResource {

    private StandardResponse result;

    public AuthResource() {
        super();
    }

    @GET
    public Response loginWithTokenJWT(@QueryParam("token") String token, @Context HttpServletRequest request) {
        UseCaseHandler.getInstance().execute(new LoginTokenUseCase(),
                new LoginTokenUseCase.Request(token, ClientIPResolver.getClientIpAddr(request)),
                new UseCase.UseCaseCallback<LoginTokenUseCase.Response>() {
                    @Override
                    public void onSuccess(LoginTokenUseCase.Response response) {
                        AuthResource.this.result = response.standardResponse;
                    }

                    @Override
                    public void onError(int status, RuntimeException e) {
                        AuthResource.this.result = new StandardResponse();
                        AuthResource.this.result.setHeader(new StandardResponse.Header(status, e.getMessage()));
                    }
                });
        if (this.result.getHeader().getStatus() == 200) {
            return Response.ok(this.result).build();
        }
        return Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
    }

    @POST
    public Response loginWithObjectLogin(UserLogin userLogin, @Context HttpServletRequest request) {
        if (("".equals(userLogin.getTokenJWT())) || (null == userLogin.getTokenJWT())) {
            String ipAddress = ClientIPResolver.getClientIpAddr(request);
            if (null != userLogin.getIpAddress() && !"".equals(userLogin.getIpAddress())) {
                ipAddress = userLogin.getIpAddress();
            }

            UseCaseHandler.getInstance().execute(new LoginUserAndPasswordUseCase(),
                    new LoginUserAndPasswordUseCase.Request(userLogin, ipAddress),
                    new UseCase.UseCaseCallback<LoginUserAndPasswordUseCase.Response>() {
                        @Override
                        public void onSuccess(LoginUserAndPasswordUseCase.Response response) {
                            AuthResource.this.result = response.standardResponse;
                        }

                        @Override
                        public void onError(int status, RuntimeException e) {
                            AuthResource.this.result = new StandardResponse();
                            AuthResource.this.result.setHeader(new StandardResponse.Header(status, e.getMessage()));

                            if (e instanceof SapiAuthenticationException) {
                                SapiAuthenticationException sapiLdapException = (SapiAuthenticationException) e;

                                Map<String, Object> data = new HashMap<String, Object>();
                                data.put("code", sapiLdapException.getCode());
                                data.put("description", sapiLdapException.getMessage());

                                StandardResponse.Body body = new StandardResponse.Body();
                                body.setData(data);

                                AuthResource.this.result.setBody(body);
                            }
                        }
                    });
            if (this.result.getHeader().getStatus() == 200) {
                return Response.ok(this.result).build();
            }
            return Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
        }
        return loginWithTokenJWT(userLogin.getTokenJWT(), request);
    }
}