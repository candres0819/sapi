package com.pragma.sapi.shiro.filter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.directory.tds.LdapFacade;
import com.pragma.sapi.core.model.StandardRequest;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.core.services.validators.ResourceADValidatorRequest;
import com.pragma.sapi.core.services.validators.ResourceTDSValidatorRequest;
import com.pragma.sapi.core.services.validators.ValidatorRequest;
import com.pragma.sapi.core.services.validators.ValidatorResponse;
import com.pragma.sapi.shiro.RequestMetadata;
import com.pragma.sapi.shiro.services.AllFilters;
import com.pragma.sapi.shiro.services.extension.ResettableStreamHttpServletRequest;
import com.pragma.sapi.shiro.token.JWTOnlyAuthenticationToken;
import com.pragma.sapi.shiro.token.TokenOfAuthHeader;
import com.pragma.sapi.shiro.token.UsernameHostToken;

/**
 * 
 * @author carlos.cardona
 *
 */
public class ResourceAuthorizationFilter extends RolesAuthorizationFilter {

    private static final Logger log = LoggerFactory.getLogger(ResourceAuthorizationFilter.class);
    private static final String TAG = "ResourceAuthorizationFilter";

    private SecuredApplication securedApplication;

    protected final ResourceRequest resource = new ResourceRequest();

    public ResourceAuthorizationFilter() {
        super();
    }

    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {
        synchronized (this.resource) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            if ("OPTIONS".equalsIgnoreCase(httpRequest.getMethod())) {
                this.resource.setAccessAllowed(true);
            } else {
                Subject currentUser = SecurityUtils.getSubject();
                if (null == currentUser) {
                    log.error(RequestMetadata.build(request).parse("No se encontro un SUBJECT en el hilo de ejecucion"));
                    this.resource.setAccessAllowed(false);
                    return this.resource.isAccessAllowed();
                }

                if (!this.validateUserAuthenticated(currentUser, request)) {
                    log.error(RequestMetadata.build(request).parse(
                            "No hay credenciales JWT o encabezado de Usuario WHITELIST para realizar una autenticación", new Object[0]));
                    this.resource.setAccessAllowed(false);
                    return this.resource.isAccessAllowed();
                }
                this.resource.setAccessAllowed(this.authorizeRequest(request, currentUser, mappedValue));
            }
            return this.resource.isAccessAllowed();
        }
    }

    protected boolean validateUserAuthenticated(Subject currentUser, ServletRequest request) {
        synchronized (this.resource) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            boolean isValid;
            if (!currentUser.isAuthenticated()) {
                log.info(RequestMetadata.build(request).parse("El SUBJECT en el hilo de ejecucion no esta Autenticado", new Object[0]));
                String token = TokenOfAuthHeader.getTokenOfApp(this.getSecuredApplication(), httpRequest);
                if (null != token) {
                    JWTOnlyAuthenticationToken authToken = new JWTOnlyAuthenticationToken(token);
                    currentUser.login(authToken);
                    log.debug(RequestMetadata.build(request).parse("El SUBJECT se ha Autenticado con un JWT", new Object[0]));
                    isValid = true;
                } else if (this.isWhiteListed(request)) {
                    if (((HttpServletRequest) request).getHeader("CuentaUsuario") != null) {
                        UsernameHostToken authToken1 = new UsernameHostToken(request.getRemoteHost(),
                                ((HttpServletRequest) request).getHeader("CuentaUsuario"));
                        currentUser.login(authToken1);
                        log.debug(RequestMetadata.build(request).parse(
                                "El SUBJECT se ha Autenticado desde un WhiteListServer con el usuario [%s]",
                                new Object[] { ((HttpServletRequest) request).getHeader("CuentaUsuario") }));
                        isValid = true;
                    } else {
                        log.warn(RequestMetadata.build(request).parse("No existe encabezado USERNAME_HEADER", new Object[0]));
                        SecurityEventReporter.registrarEvento(httpRequest, TAG, "NoUserNameHeader", (String) null);
                        isValid = false;
                    }
                } else {
                    log.warn(RequestMetadata.build(request).parse("No token JWT para autenticar, ni es una peticion whitelisted",
                            new Object[0]));
                    SecurityEventReporter.registrarEvento(httpRequest, TAG, "NoJWTorWhiteList", (String) null);
                    isValid = false;
                }
            } else {
                log.debug("El SUBJECT en el hilo de ejecucion actual es [{}]", currentUser.getPrincipal());
                isValid = true;
            }
            return isValid;
        }
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws IOException {
        synchronized (this.resource) {
            if (!this.resource.isAccessAllowed()) {
                log.debug(RequestMetadata.build(request).parse("Acceso negado", new Object[0]));
                log.error(" Response to Client ({}, {})", this.resource.getCode(), this.resource.getMessage());
                HttpServletResponse httpResponse = WebUtils.toHttp(response);

                switch (ValidatorResponse.Status.values()[this.resource.getCode().ordinal()]) {
                case UNAUTHORIZED:
                    httpResponse.setStatus(401);
                    break;
                case FORBIDDEN:
                    httpResponse.setStatus(403);
                    break;
                case BAD_GATEWAY:
                    httpResponse.setStatus(502);
                    break;
                case SERVICE_UNAVAILABLE:
                    httpResponse.setStatus(503);
                    break;
                default:
                    httpResponse.setStatus(500);
                }

                response.setContentType("application/json");
                AllFilters.build().applyAllFilters(httpResponse);
                response.getWriter().write(
                        String.format("{ \"error\": %s, \"mensaje\": \"%s\" }", this.resource.getCode(), this.resource.getMessage()));
            }
            return this.resource.isAccessAllowed();
        }
    }

    protected boolean authorizeRequest(ServletRequest servletRequest, Subject subject, Object mappedValue) {
        synchronized (this.resource) {
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            String path = httpRequest.getRequestURI().replace(httpRequest.getContextPath(), "");

            if (null == subject) {
                log.error(RequestMetadata.build(servletRequest).parse("No se encontro un SUBJECT en el hilo de ejecucion", new Object[0]));
                return false;
            } else {
                Map<String, String> mapRoles = null;
                if (null != httpRequest.getHeader("roles")) {
                    mapRoles = new HashMap<String, String>();
                    mapRoles.put("roles", httpRequest.getHeader("roles"));
                } else {
                    log.warn(RequestMetadata.build(servletRequest)
                            .parse(String.format("El header http \'%s\' que contiene los roles no esta presente en la peticion",
                                    new Object[] { "roles" }), new Object[0]));
                }

                UserProperties userProperties = (UserProperties) subject.getPrincipals().oneByType(UserProperties.class);

                List<String> roles = userProperties.getRoles();
                String idEmpleado = userProperties.getIdentification();
                String canonical = userProperties.getRealm();

                Object validatorRequest;
                if (canonical.equalsIgnoreCase(LdapFacade.class.getCanonicalName())) {
                    validatorRequest = new ResourceTDSValidatorRequest(this.getSecuredApplication().getDataRepository());
                } else {
                    validatorRequest = new ResourceADValidatorRequest(this.getSecuredApplication().getDataRepository());
                }

                log.debug("Verificando acceso permitido al recurso: {}", path);
                log.debug("Roles de usuario autenticado {}", roles);
                StandardRequest requestAndRolesDTO = new StandardRequest(this.getSecuredApplication().getName(), path,
                        httpRequest.getMethod().toUpperCase(), roles);
                requestAndRolesDTO.setHeaders(mapRoles);
                requestAndRolesDTO.setBody(this.resource.getBody());

                if (this.resource.esUnRecursoCommand()) {
                    requestAndRolesDTO.setCommandSubmitted(this.resource.getCommandSubmitted());
                    requestAndRolesDTO.setCommand(this.resource.getCommandSubmitted().getComando().getNombre());
                }

                Map<String, Object> context = new HashMap<String, Object>();
                context.put("gestorId", idEmpleado);
                context.put("mappedValue", mappedValue);
                boolean isValid = ((ValidatorRequest) validatorRequest).isValidRequest(requestAndRolesDTO, context, this.resource);

                if (!isValid) {
                    SecurityEventReporter.registrarEvento(httpRequest, TAG, "AccesoNoPermitido",
                            "{ \'subject\': \'" + subject.getPrincipal().toString() + "\', " + requestAndRolesDTO.toString() + " }");
                }

                if (context.containsKey("RestrictionInRequest")) {
                    servletRequest.setAttribute("RestrictionInRequest", context.get("RestrictionInRequest"));
                }

                log.debug("Validacion para el filtro de recursos  es \'{}\'", Boolean.valueOf(isValid));
                return isValid;
            }
        }
    }

    public void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        synchronized (this.resource) {
            ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest((HttpServletRequest) request);
            this.resource.setBody((String) null);
            String[] bodyableMethods = new String[] { "POST", "PUT" };
            boolean aplicaBody = Arrays.stream(bodyableMethods).anyMatch((m) -> {
                return m.equalsIgnoreCase(wrappedRequest.getMethod());
            });

            if (aplicaBody) {
                log.debug(RequestMetadata.build(request).parse("Procesando BODY de peticion", new Object[0]));
                this.resource.setBody(IOUtils.toString(wrappedRequest.getInputStream(), Charset.defaultCharset()));
                this.resource.esUnRecursoCommand();
                wrappedRequest.resetInputStream();
                super.doFilterInternal(wrappedRequest, response, chain);
            } else {
                log.trace(RequestMetadata.build(request).parse("Metodo http usado no requiere BODY", new Object[0]));
                super.doFilterInternal(request, response, chain);
            }
        }
    }

    protected boolean isWhiteListed(ServletRequest request) {
        synchronized (this.resource) {
            return null == request.getAttribute("whitelisted") ? false : ((Boolean) request.getAttribute("whitelisted")).booleanValue();
        }
    }

    public ResourceRequest getResource() {
        return this.resource;
    }

    public SecuredApplication getSecuredApplication() {
        return this.securedApplication;
    }

    public void setSecuredApplication(SecuredApplication securedApplication) {
        this.securedApplication = securedApplication;
    }
}
