package com.pragma.sapi.shiro.services;

import javax.ws.rs.ext.Provider;

@Provider
public class HSTSFilter extends BaseContainerResponseFilter {

    public static final String HSTS_HEADER = "Strict-Transport-Security";
    public static final String HSTS_HEADER_VALUE = "max-age=31536000; includeSubDomains";
    static final String[] ALL_HEADERs = new String[] { "Strict-Transport-Security" };
    static final String[] ALL_HEADER_VALUEs = new String[] { "max-age=31536000; includeSubDomains" };

    public HSTSFilter() {
        this.setAllHeaders(ALL_HEADERs);
        this.setAllHeaderValues(ALL_HEADER_VALUEs);
    }
}