package com.pragma.sapi.shiro.services.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.UserLogin;
import com.pragma.sapi.core.model.StandardResponse.Header;
import com.pragma.sapi.shiro.services.usecase.LogoutTokenUseCase;
import com.pragma.sapi.shiro.services.usecase.LogoutUserNameUseCase;
import com.pragma.sapi.shiro.services.usecase.UseCase.UseCaseCallback;
import com.pragma.sapi.shiro.services.usecase.UseCaseHandler;

@Path("/sapi/logout")
@Produces({ "application/json" })
public class LogoutResource {

    private StandardResponse result;

    public LogoutResource() {
        super();
    }

    @GET
    public Response logoutTokenJWT(@QueryParam("closeAll") Integer closeAllFlag, @QueryParam("token") String token,
            @Context HttpServletRequest request) {
        boolean closeAllSessions = false;
        if (null != closeAllFlag && closeAllFlag.equals(Integer.valueOf(1))) {
            closeAllSessions = true;
        }

        if (null == token) {
            this.result = new StandardResponse();
            this.result.setHeader(new Header(500, "No se encuentra el token de autorizacion."));
            return Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
        } else {
            return this.logout(token, closeAllSessions);
        }
    }

    @POST
    public Response logoutUserName(UserLogin userLogin, @Context HttpServletRequest request) {
        if (null == userLogin.getUser()) {
            this.result = new StandardResponse();
            this.result.setHeader(new Header(500, "No se encuentra un usuario."));
            return Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
        } else {
            UseCaseHandler.getInstance().execute(new LogoutUserNameUseCase(), new LogoutUserNameUseCase.Request(userLogin.getUser()),
                    new UseCaseCallback<LogoutUserNameUseCase.Response>() {
                        public void onSuccess(LogoutUserNameUseCase.Response response) {
                            LogoutResource.this.result = response.standardResponse;
                        }

                        public void onError(int status, RuntimeException e) {
                            LogoutResource.this.result = new StandardResponse();
                            LogoutResource.this.result.setHeader(new Header(status, e.getMessage()));
                        }
                    });

            return this.result.getHeader().getStatus() == 200 ? Response.ok(this.result).build()
                    : Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
        }
    }

    private Response logout(String token, boolean closeAllSessions) {
        UseCaseHandler.getInstance().execute(new LogoutTokenUseCase(), new LogoutTokenUseCase.Request(token, closeAllSessions),
                new UseCaseCallback<LogoutTokenUseCase.Response>() {
                    public void onSuccess(LogoutTokenUseCase.Response response) {
                        LogoutResource.this.result = response.standardResponse;
                    }

                    public void onError(int status, RuntimeException e) {
                        LogoutResource.this.result = new StandardResponse();
                        LogoutResource.this.result.setHeader(new Header(status, e.getMessage()));
                    }
                });
        return this.result.getHeader().getStatus() == 200 ? Response.ok(this.result).build()
                : Response.serverError().status(this.result.getHeader().getStatus()).entity(this.result).build();
    }
}
