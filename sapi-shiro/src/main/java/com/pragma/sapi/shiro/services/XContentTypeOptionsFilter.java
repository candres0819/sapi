package com.pragma.sapi.shiro.services;

import javax.ws.rs.ext.Provider;

@Provider
public class XContentTypeOptionsFilter extends BaseContainerResponseFilter {

    public static final String XCTO_HEADER = "X-Content-Type-Options";
    public static final String XCTO_HEADER_VALUE = "nosniff";

    static final String[] ALL_HEADERs = new String[] { "X-Content-Type-Options" };
    static final String[] ALL_HEADER_VALUEs = new String[] { "nosniff" };

    public XContentTypeOptionsFilter() {
        this.setAllHeaders(ALL_HEADERs);
        this.setAllHeaderValues(ALL_HEADER_VALUEs);
    }
}