package com.pragma.sapi.shiro.services.usecase;

public class UseCaseHandler {

    private static UseCaseHandler INSTANCE;

    public static UseCaseHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UseCaseHandler();
        }
        return INSTANCE;
    }

    public <T extends UseCase.RequestValues, R extends UseCase.ResponseValue> void execute(UseCase<T, R> useCase, T values,
            UseCase.UseCaseCallback<R> callback) {
        useCase.setRequestValues(values);
        useCase.setUseCaseCallback(new UiCallbackWrapper<R>(callback));
        useCase.run();
    }

    private static final class UiCallbackWrapper<V extends UseCase.ResponseValue> implements UseCase.UseCaseCallback<V> {

        private final UseCase.UseCaseCallback<V> mCallback;

        public UiCallbackWrapper(UseCase.UseCaseCallback<V> callback) {
            this.mCallback = callback;
        }

        public void onSuccess(V response) {
            this.mCallback.onSuccess(response);
        }

        public void onError(int status, RuntimeException e) {
            this.mCallback.onError(status, e);
        }
    }
}