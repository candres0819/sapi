package com.pragma.sapi.shiro.services;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AllFilters {

    private static final Logger log = LoggerFactory.getLogger(AllFilters.class);

    static Class<?>[] basicFilters = new Class[] { HSTSFilter.class, PoweredByFilter.class, XContentTypeOptionsFilter.class,
            XXSSProtectionFilter.class };

    static List<String[]> headers = new ArrayList<String[]>();

    private static void getAttributesAndValues(Class<?> c) {
        try {
            Field e = c.getDeclaredField("ALL_HEADERs");
            Field fv = c.getDeclaredField("ALL_HEADER_VALUEs");

            String[] names = (String[]) ((String[]) e.get(c));
            String[] values = (String[]) ((String[]) fv.get(c));

            for (int i = 0; i < names.length; ++i) {
                headers.add(new String[] { names[i], values[i] });
            }
        } catch (IllegalAccessException | NoSuchFieldException arg5) {
            log.error("Error accediendo a los atributos de la clase filtro", arg5);
        }
    }

    public static AllFilters build() {
        if (headers.isEmpty()) {
            Arrays.stream(basicFilters).forEach(AllFilters::getAttributesAndValues);

            headers.add(new String[] { "Access-Control-Allow-Origin", "*" });
            headers.add(new String[] { "Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS" });
            headers.add(new String[] { "Access-Control-Max-Age", "180" });
            headers.add(new String[] { "Content-Type", "application/json" });
            headers.add(new String[] { "Access-Control-Allow-Headers",
                    "User-Agent,Referer,Origin,Host,Connection,Access-Control-Request-Method,Access-Control-Request-Headers,Cache-Control,Origin,X-Requested-With,Content-Type,Accept,Accept-Encoding,Accept-Language" });
        }

        return new AllFilters();
    }

    public void applyAllFilters(HttpServletResponse response) {
        if (headers != null && !headers.isEmpty()) {
            headers.stream().forEach((h) -> {
                log.trace("Header establecido {}:{}", h[0], h[1]);
                response.setHeader(h[0], h[1]);
            });
        } else {
            log.warn("Coleccion de headers esta vacia.");
        }
    }
}