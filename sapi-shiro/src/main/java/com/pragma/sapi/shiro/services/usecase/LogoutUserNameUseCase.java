package com.pragma.sapi.shiro.services.usecase;

import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.StandardResponse.Header;
import com.pragma.sapi.core.repository.impl.DataRepository;
import com.pragma.sapi.shiro.services.usecase.UseCase.RequestValues;
import com.pragma.sapi.shiro.services.usecase.UseCase.ResponseValue;
import com.pragma.sapi.shiro.services.usecase.settings.BeanableMethodUseCase;

/**
 * 
 * @author carlos.cardona
 *
 */
public class LogoutUserNameUseCase extends BeanableMethodUseCase<LogoutUserNameUseCase.Request, LogoutUserNameUseCase.Response> {

    private static final Logger log = LoggerFactory.getLogger(LogoutUserNameUseCase.class);

    public LogoutUserNameUseCase() {
        super();
    }

    protected void executeUseCase(LogoutUserNameUseCase.Request requestValues) {
        String username = requestValues.username;

        if (null != username && !"".equals(username)) {
            this.setUpBeans();

            try {
                DataRepository repo = this.getSecuredApplication().getDataRepository();
                repo.closeAllActiveSessions(this.getSecuredApplication().getName(), username);
                log.debug("Sesiones activas de {} en {} cerradas.", username, this.getSecuredApplication().getName());

                this.getUseCaseCallback().onSuccess(new LogoutUserNameUseCase.Response(this.responseSuccess()));
            } catch (Exception e) {
                log.error("Error", e);
                this.getUseCaseCallback().onError(500, new AuthenticationException("Error en servidor"));
            }
        } else {
            log.warn("No se presentaron las credenciales para cerrar sesion.");
            this.getUseCaseCallback().onError(400, new AuthenticationException("Credenciales no presentadas"));
        }
    }

    /**
     * 
     * @return responseSuccess StandardResponse
     */
    private StandardResponse responseSuccess() {
        StandardResponse result = new StandardResponse();
        result.setHeader(new Header(200, "Cerrado Sesion. Operacion exitosa"));
        return result;
    }

    public static class Response implements ResponseValue {

        public final StandardResponse standardResponse;

        public Response(StandardResponse response) {
            this.standardResponse = response;
        }
    }

    public static class Request implements RequestValues {

        private final String username;

        public Request(String username) {
            this.username = username;
        }
    }
}
