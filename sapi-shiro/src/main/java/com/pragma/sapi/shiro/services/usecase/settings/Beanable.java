package com.pragma.sapi.shiro.services.usecase.settings;

import java.util.Map;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;

public interface Beanable {

    SecuredApplication getSecuredApplication();

    Map<String, SapiIdentityRepository> getSapiIdentityRepositories();

    void setUpBeans();
}