package com.pragma.sapi.shiro.token;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.util.TokenJWTUtils;

public class TokenOfAuthHeader {

    private static final Logger log = LoggerFactory.getLogger(TokenOfAuthHeader.class);

    public static final Pattern JWT_PATTERN = Pattern.compile("^([a-zA-Z0-9_=]+)\\.([a-zA-Z0-9_=]+)\\.([a-zA-Z0-9_\\-\\+\\/=]*)");

    public static String getTokenOfApp(SecuredApplication application, HttpServletRequest httpRequest) {
        String jwt = getJWTFromHeader(httpRequest);
        if (null == jwt) {
            ObjectJWT objectJWT = getTokenParsed(jwt);
            if (objectJWT != null && !application.getName().equals(objectJWT.getIssuer())) {
                jwt = null;
            }
        }
        return jwt;
    }

    public static ObjectJWT getTokenParsed(String jwt) {
        ObjectJWT objectJWT = null;
        if (null != jwt && !"".equalsIgnoreCase(jwt.trim())) {
            objectJWT = TokenJWTUtils.parseObjectJWT(jwt);
        }
        return objectJWT;
    }

    public static ObjectJWT getTokenParsed(HttpServletRequest httpRequest) {
        String jwt = getJWTFromHeader(httpRequest);
        return getTokenParsed(jwt);
    }

    public static String getJWTFromHeader(HttpServletRequest httpRequest) {
        String jwt = null;
        String headerAuthorization = httpRequest.getHeader("Authorization");

        if (null != headerAuthorization && headerAuthorization.startsWith("Bearer ")) {
            jwt = headerAuthorization.substring(7);

            Matcher matcher = JWT_PATTERN.matcher(jwt);
            if (!matcher.matches()) {
                log.error("El Token en el encabezado [Authorization] no corresponde a un JWT. Verificar!...");
                jwt = null;
            }
        }
        return jwt;
    }
}