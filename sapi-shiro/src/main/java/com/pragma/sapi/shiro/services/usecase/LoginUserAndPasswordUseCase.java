package com.pragma.sapi.shiro.services.usecase;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.exception.FirmaNoValidaException;
import com.pragma.sapi.core.exception.SapiLdapException;
import com.pragma.sapi.core.exception.SapiMaxNumOfSessionsException;
import com.pragma.sapi.core.exception.SapiValidationException;
import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.UserLogin;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.core.services.auth.SessionManager;
import com.pragma.sapi.shiro.services.usecase.LoginUserAndPasswordUseCase.Request;
import com.pragma.sapi.shiro.services.usecase.LoginUserAndPasswordUseCase.Response;
import com.pragma.sapi.shiro.services.usecase.settings.BeanableMethodUseCase;
import com.pragma.sapi.token.firm.Firm;

public class LoginUserAndPasswordUseCase extends BeanableMethodUseCase<Request, Response> {

    private static final Logger log = LoggerFactory.getLogger(LoginUserAndPasswordUseCase.class);

    protected void executeUseCase(Request requestValues) {
        log.debug("Ejecutando el caso de autenticacion por usuario/contrasena");
        log.trace(String.format("Subject a autenticar [%s]", ((Request) getRequestValues()).userLogin));
        setUpBeans();
        if (isValidUserLogin(requestValues.userLogin)) {
            String user = requestValues.userLogin.getUser();
            String password = requestValues.userLogin.getPassword();

            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user, password);
            usernamePasswordToken.setRememberMe(false);
            try {
                Subject subject = login(usernamePasswordToken);

                UserProperties userProperties = (UserProperties) subject.getPrincipals().oneByType(UserProperties.class);
                userProperties.setIpAddress(requestValues.ipAddress);

                SapiIdentityRepository idRepository = (SapiIdentityRepository) getSapiIdentityRepositories().get(userProperties.getRealm());

                Firm<?> firm = idRepository.getKeyStoreManager().getPrivateCert(getSecuredApplication().getName(),
                        getSecuredApplication().getCertificatePassword());

                int numeroMaximoSessiones = idRepository.getNumeroMaximoSesiones();

                String jwtTokenString = new SessionManager(getSecuredApplication()).createNewSession(
                        subject.getPrincipals().getPrimaryPrincipal(),
                        (UserProperties) subject.getPrincipals().oneByType(UserProperties.class), firm, numeroMaximoSessiones);

                getUseCaseCallback().onSuccess(
                        new Response(LoginTokenUseCase.getStandardResponse(jwtTokenString, 0L, userProperties, "Autenticacion exitosa")));
            } catch (SapiMaxNumOfSessionsException ee) {
                log.error("No tiene acceso por la validacion de numero de sesiones", ee);
                getUseCaseCallback().onError(401, ee);
            } catch (SapiLdapException e) {
                log.error("Error al autenticar con LDAP", e);
                getUseCaseCallback().onError(401, e);
            } catch (AuthenticationException ae) {
                log.error("Error por autenticacion", ae);
                getUseCaseCallback().onError(401, new AuthenticationException(ae.getMessage()));
            } catch (FirmaNoValidaException fne) {
                log.error("Error por firma", fne);
                getUseCaseCallback().onError(500, new AuthenticationException("Error interno de Servidor"));
            } catch (Exception e) {
                log.error("Error interno", e);
                getUseCaseCallback().onError(500, new AuthenticationException("Error interno de servidor"));
            }
        } else {
            log.error("Usuario y/o Password no validos.");
            getUseCaseCallback().onError(400, new AuthenticationException("Usuario y/o Password no validos."));
        }
    }

    private Subject login(UsernamePasswordToken usernamePasswordToken) throws AuthenticationException, SapiValidationException {
        Subject currentUser = SecurityUtils.getSubject();
        log.debug(String.format("Autenticando usuario [%s]", currentUser));
        try {
            currentUser.login(usernamePasswordToken);
            return currentUser;
        } catch (UnknownAccountException uae) {
            log.error("Cuenta desconocida", uae);
            throw uae;
        } catch (IncorrectCredentialsException ice) {
            log.error("Credenciales no validas", ice);
            throw ice;
        } catch (LockedAccountException lae) {
            log.error("Cuenta bloqueada", lae);
            throw lae;
        } catch (ExcessiveAttemptsException eae) {
            log.error("Superado el numero de intentos", eae);
            throw eae;
        } catch (AuthenticationException ae) {
            log.error("Error de autenticacion", ae);
            throw ae;
        } catch (Exception e) {
            log.error("Error desconocido", e);
            throw new SapiValidationException("Error desconocido", e);
        }
    }

    private boolean isValidUserLogin(UserLogin userLogin) {
        return (userLogin.getPassword() != null) && (!"".equals(userLogin.getPassword())) && (userLogin.getUser() != null)
                && (!"".equals(userLogin.getUser()));
    }

    public static final class Request implements UseCase.RequestValues {
        private final UserLogin userLogin;
        private final String ipAddress;

        public Request(UserLogin userLogin, String ipAddress) {
            this.userLogin = userLogin;
            this.ipAddress = ipAddress;
        }
    }

    public static final class Response implements UseCase.ResponseValue {

        public final StandardResponse standardResponse;

        public Response(StandardResponse standardResponse) {
            this.standardResponse = standardResponse;
        }
    }
}
