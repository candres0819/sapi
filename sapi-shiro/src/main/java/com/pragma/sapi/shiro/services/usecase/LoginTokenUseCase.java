package com.pragma.sapi.shiro.services.usecase;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.exception.SapiMaxNumOfSessionsException;
import com.pragma.sapi.core.model.StandardResponse;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.core.services.auth.SessionManager;
import com.pragma.sapi.shiro.services.usecase.LoginTokenUseCase.Request;
import com.pragma.sapi.shiro.services.usecase.LoginTokenUseCase.Response;
import com.pragma.sapi.shiro.services.usecase.settings.BeanableMethodUseCase;
import com.pragma.sapi.shiro.token.JWTOnlyAuthenticationToken;
import com.pragma.sapi.shiro.token.TokenOfAuthHeader;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.firm.Firm;

public class LoginTokenUseCase extends BeanableMethodUseCase<Request, Response> {

    private static final Logger log = LoggerFactory.getLogger(LoginTokenUseCase.class);

    private SessionManager sessionManager = null;

    protected void executeUseCase(Request requestValues) {
        log.debug("Se ejecuta el caso para realiza la autenticacion por token");
        setUpBeans();

        this.sessionManager = new SessionManager(getSecuredApplication());

        String token = requestValues.token;
        if ((null == token) || ("".equals(token))) {
            getUseCaseCallback().onError(400, new AuthenticationException("Credenciales no presentadas o no validas."));
            return;
        }

        ObjectJWT jwtParsed = TokenOfAuthHeader.getTokenParsed(token);
        if (jwtParsed.getRealm() == ObjectJWT.Realm.Empleados) {
            log.warn("El token presentado es de Azure y no se admite para transferencia de identidad");
            getUseCaseCallback().onError(400, new AuthenticationException(
                    "Los tokens de Azure no estan habilidatos para transferir identidad. Los Tokens de AZURE ya garantizan un ambiente de Single-Sign-On"));
            return;
        }

        if (jwtParsed.getExpiration().before(new Date())) {
            log.warn("El token presentado ha expirado.");
            getUseCaseCallback().onError(401, new AuthenticationException("Autenticacion no exitosa. Token ya expiro."));
            return;
        }

        Subject subject;
        try {
            subject = SecurityUtils.getSubject();
            subject.login(new JWTOnlyAuthenticationToken(token));
        } catch (AuthenticationException e) {
            log.error("Error de autenticacion", e);
            getUseCaseCallback().onError(401, new AuthenticationException(e.getMessage()));
            return;
        } catch (Exception e) {
            log.error("Error", e);
            getUseCaseCallback().onError(500, new AuthenticationException("Error interno de servidor"));
            return;
        }

        if (!subject.isAuthenticated()) {
            getUseCaseCallback().onError(401, new AuthenticationException("El subject no pudo ser autenticado"));
            return;
        }

        UserProperties userProperties = (UserProperties) subject.getPrincipals().oneByType(UserProperties.class);
        userProperties.setIpAddress(requestValues.ipAddress);

        Map<String, SapiIdentityRepository> sapiIdentityRepositories = this.getSapiIdentityRepositories();
        SapiIdentityRepository idRepository = (SapiIdentityRepository) sapiIdentityRepositories.get(userProperties.getRealm());

        this.sessionManager.registerAndInactivateSession(jwtParsed);

        SecuredApplication securedApplication = getSecuredApplication();
        Firm<?> firm = idRepository.getKeyStoreManager().getPrivateCert(securedApplication.getName(),
                securedApplication.getCertificatePassword());

        validarLimiteSessiones(jwtParsed.getSubject(), idRepository.getNumeroMaximoSesiones());

        String jwtTokenString = this.sessionManager.createNewSession(subject.getPrincipals().getPrimaryPrincipal(),
                (UserProperties) subject.getPrincipals().oneByType(UserProperties.class), firm);

        getUseCaseCallback().onSuccess(new Response(getStandardResponse(jwtTokenString, 0L, userProperties, "Autenticacion Exitosa")));
    }

    /**
     * Valida el numero de sessiones permitidas desde la configuraion.
     * 
     * @param subject the subject
     */
    private void validarLimiteSessiones(String subject, int numeroMaximoSesiones) {
        try {
            log.debug("Limite de Sesiones {}", Integer.valueOf(numeroMaximoSesiones));
            this.sessionManager.checkSessionLimitPolicy(subject, numeroMaximoSesiones);
        } catch (SapiMaxNumOfSessionsException e) {
            log.error("Error por numero de sesiones del usuario", e);
            throw new SapiMaxNumOfSessionsException(e.getMessage());
        } catch (Exception e) {
            log.error("error en la validacion de las sesiones del usuario", e);
            throw new AuthenticationException(e.getMessage());
        }
    }

    public static StandardResponse getStandardResponse(String tokenSesion, long expiration, UserProperties userProperties, String mensaje) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("token", tokenSesion);
        data.put("expiration", Long.valueOf(expiration));
        data.put("userProperties", userProperties);

        StandardResponse standardResponse = new StandardResponse();
        StandardResponse.Body body = new StandardResponse.Body();
        body.setData(data);

        standardResponse.setBody(body);
        standardResponse.setHeader(new StandardResponse.Header(200, mensaje));

        return standardResponse;
    }

    public static final class Request implements UseCase.RequestValues {

        private final String token;
        private final String ipAddress;

        public Request(String token, String ipAddress) {
            this.token = token;
            this.ipAddress = ipAddress;
        }
    }

    public static final class Response implements UseCase.ResponseValue {

        public final StandardResponse standardResponse;

        public Response(StandardResponse response) {
            this.standardResponse = response;
        }
    }
}
