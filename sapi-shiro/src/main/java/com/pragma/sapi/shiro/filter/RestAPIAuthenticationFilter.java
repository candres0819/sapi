package com.pragma.sapi.shiro.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.datasource.session.SapiSession;
import com.pragma.sapi.core.services.auth.SessionManager;
import com.pragma.sapi.shiro.ClientIPResolver;
import com.pragma.sapi.shiro.RequestMetadata;
import com.pragma.sapi.shiro.services.AllFilters;
import com.pragma.sapi.shiro.token.JWTOnlyAuthenticationToken;
import com.pragma.sapi.shiro.token.TokenOfAuthHeader;
import com.pragma.sapi.shiro.token.UsernameHostToken;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.UUIDGen;

public class RestAPIAuthenticationFilter extends AuthenticatingFilter {

    private static final Logger log = LoggerFactory.getLogger(RestAPIAuthenticationFilter.class);

    public static final String SAPI_UUID_REQUEST_ATTRIBUTE = "SAPI_UUID_REQUEST";

    private SecuredApplication securedApplication;
    private boolean validacionExplicitaDeIPs = true;

    public enum TokenIssuer {

        SAPI, MICROSOFT;

        TokenIssuer() {

        }
    }

    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        this.setRequestUUID(request);
        this.setRequestIP(ClientIPResolver.getClientIpAddr(request), request);
        String token = TokenOfAuthHeader.getJWTFromHeader((HttpServletRequest) request);

        if (token != null && !"".equalsIgnoreCase(token)) {
            request.setAttribute("token", token);
            log.debug("Creando un AuthenticationToken con un JWT presente en el request");
            return new JWTOnlyAuthenticationToken(token, (String) request.getAttribute("SAPI_UUID_REQUEST"),
                    RequestMetadata.build(request).getSapiSessionId(), (String) request.getAttribute("SAPI_IP_REQUEST_ATTRIBUTE"));
        } else if (this.isWhiteListed(request)) {
            log.debug("Creando un AuthenticationToken con username y host - whitelisted");
            String cuentaUsuario = ((HttpServletRequest) request).getHeader("CuentaUsuario");
            String ipAddress = (String) request.getAttribute("SAPI_IP_REQUEST_ATTRIBUTE");
            return new UsernameHostToken(ipAddress, cuentaUsuario);
        } else {
            log.error(RequestMetadata.build(request).parse("No se pudo determinar un parametro para construir un AuthenticationToken",
                    new Object[0]));
            return null;
        }
    }

    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        this.setRequestUUID(request);
        this.setRequestIP(ClientIPResolver.getClientIpAddr(request), request);
        HttpServletRequest httpRequest = WebUtils.toHttp(request);

        return "OPTIONS".equalsIgnoreCase(httpRequest.getMethod());
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        this.setRequestUUID(request);
        this.setRequestIP(ClientIPResolver.getClientIpAddr(request), request);

        String jwtToken = TokenOfAuthHeader.getJWTFromHeader((HttpServletRequest) request);
        if (null == jwtToken) {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. No se presentaron credenciales.", new Object[0]));
            this.notAuthorized(request, response, 400, "Credenciales no presentadas o no validas.");
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "CredencialesNoPresentadas", (String) null);
            return false;
        } else if (this.isJWTLogInAttempt(request, response)) {
            log.debug("Intento de login utilizando token");
            return this.validToken(jwtToken, request, response);
        } else if (this.isWhiteListedHostAttempt(request, response)) {
            log.debug("Intento de login User/Host en lista blanca");
            if (!this.executeLogin(request, response)) {
                log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa.", new Object[0]));
                SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "AutenticacionNoExitosaDeWhiteList",
                        (String) null);
                this.notAuthorized(request, response, 401, "Acceso no permitido");
                return false;
            } else {
                return true;
            }
        } else if (this.validToken(jwtToken, request, response)) {
            log.debug(RequestMetadata.build(request).parse("Acceso permitido", new Object[0]));
            return true;
        } else {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. No se presentaron credenciales.", new Object[0]));
            this.notAuthorized(request, response, 400, "Credenciales no presentadas o no validas.");
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "CredencialesNoPresentadas", (String) null);
            return false;
        }
    }

    private boolean validToken(String jwtToken, ServletRequest request, ServletResponse response) throws IOException {
        if (null == jwtToken) {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. Token no presente en encabezado de authorizacion",
                    new Object[0]));
        }

        TokenIssuer generadorDelToken = TokenIssuer.SAPI;
        ObjectJWT parsedJwt = TokenOfAuthHeader.getTokenParsed(jwtToken);
        if (null != this.getSecuredApplication().getTenant() && parsedJwt.getIssuer().contains(this.getSecuredApplication().getTenant())) {
            generadorDelToken = TokenIssuer.MICROSOFT;
        }

        return generadorDelToken == TokenIssuer.SAPI ? this.validateSapiToken(jwtToken, request, response)
                : this.validateAzureToken(jwtToken, request, response);
    }

    private boolean validateAzureToken(String jwtToken, ServletRequest request, ServletResponse response) throws IOException {
        ObjectJWT jwtParsed = TokenOfAuthHeader.getTokenParsed(jwtToken);
        if (new Date().compareTo(jwtParsed.getExpiration()) > 0) {
            this.notAuthorized(request, response, 401, "Token ha expirado.");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateSapiToken(String jwtToken, ServletRequest request, ServletResponse response) throws IOException {
        ObjectJWT jwtParsed = TokenOfAuthHeader.getTokenParsed(jwtToken);
        if (null == jwtParsed) {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. Token no valido para la aplicacion.", new Object[0]));
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "TokenInvalidoDeAplicacion", (String) null);
            this.notAuthorized(request, response, 401, "Token no valido de aplicacion.");
            return false;
        } else if (this.isSapiTokenExpired(jwtParsed)) {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. Token ya expiro.", new Object[0]));
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "TokenExpirado", jwtToken);
            this.notAuthorized(request, response, 401, "Token ha expirado.");
            return false;
        } else if (!this.isSapiTokenActive(jwtParsed)) {
            log.error(RequestMetadata.build(request).parse("Autenticacion no exitosa. Token ya no es valido.", new Object[0]));
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "TokenInvalidoPorFinDeSesion", jwtToken);
            this.notAuthorized(request, response, 401, "Autenticacion no exitosa. Token ya no es valido.");
            return false;
        } else if (this.isValidacionExplicitaDeIPs() && !this.isSapiTokenIpSameOfRequestIp(jwtParsed, request)) {
            log.error(RequestMetadata.build(request)
                    .parse("Autenticacion no exitosa. No hay concordancia de la IP del Token vs IP del Request", new Object[0]));
            SecurityEventReporter.registrarEvento(request, "RestAPIAuthenticationFilter", "TokenMultiplesIP", jwtToken);
            this.notAuthorized(request, response, 401, "Token no valido para la direccion IP");
            return false;
        } else {
            return true;
        }
    }

    private boolean isSapiTokenActive(ObjectJWT jwtParsed) {
        boolean isActive = true;
        try {
            log.debug("Determinando si el uuid \'{}\' corresponde a un token valido", jwtParsed.getUuid());
            SapiSession ex = (new SessionManager(this.securedApplication)).findSessionByUUID(jwtParsed.getUuid());
            if (null == ex || 0 == ex.getActive()) {
                isActive = false;
            }

            log.debug("session {} es {}", jwtParsed.getUuid(), isActive ? "Activa y Valida" : "Inactiva y no valida");
        } catch (Exception e) {
            log.error("Error", e);
            isActive = false;
        }
        return isActive;
    }

    private boolean isSapiTokenExpired(ObjectJWT jwtParsed) {
        return (new Date()).compareTo(jwtParsed.getExpiration()) > 0;
    }

    private boolean isSapiTokenIpSameOfRequestIp(ObjectJWT jwtParsed, ServletRequest request) {
        String ipEnToken = (String) jwtParsed.getClaims().get("ipaddr");
        if (null == ipEnToken) {
            log.warn(RequestMetadata.build(request).parse("JWT sin Claim de IP", new Object[0]));
            return false;
        } else {
            String ipRequest = ClientIPResolver.getClientIpAddr(request);
            return ipEnToken.equalsIgnoreCase(ipRequest);
        }
    }

    private boolean isWhiteListed(ServletRequest request) {
        return null == request.getAttribute("whitelisted") ? false : ((Boolean) request.getAttribute("whitelisted")).booleanValue();
    }

    protected void notAuthorized(ServletRequest servletRequest, ServletResponse servletResponse, int status, String msg)
            throws IOException {
        log.error("Response to Client ({}, {})", Integer.valueOf(status), msg);
        HttpServletResponse response = WebUtils.toHttp(servletResponse);
        response.setStatus(status);
        response.setContentType("application/json");
        AllFilters.build().applyAllFilters(response);
        response.getWriter().write(String.format("{ \"error\": %d, \"mensaje\": \"%s\" }", Integer.valueOf(status), msg));
    }

    protected boolean isJWTLogInAttempt(ServletRequest request, ServletResponse response) {
        boolean retorno = false;
        try {
            boolean isJWTFromHeader = TokenOfAuthHeader.getJWTFromHeader((HttpServletRequest) request) != null;
            boolean isNotAuthenticated = !this.getSubject(request, response).isAuthenticated();

            retorno = isJWTFromHeader && isNotAuthenticated;
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error(RequestMetadata.build(request).parse("Token no valido", new Object[0]), e);
        } catch (Exception e) {
            log.error(RequestMetadata.build(request).parse("Error descrifrando presencia de token", new Object[0]), e);
        }
        return retorno;
    }

    protected boolean isWhiteListedHostAttempt(ServletRequest request, ServletResponse response) {
        try {
            String e = ((HttpServletRequest) request).getHeader("CuentaUsuario");
            return e != null && !"".equalsIgnoreCase(e) && this.isWhiteListed(request);
        } catch (Exception e) {
            log.error("Error", e);
            return false;
        }
    }

    private void setRequestUUID(ServletRequest request) {
        if (StringUtils.isBlank((String) request.getAttribute("SAPI_UUID_REQUEST"))) {
            request.setAttribute("SAPI_UUID_REQUEST", UUIDGen.getTimeUUID().toString());
        }
    }

    private void setRequestIP(String ip, ServletRequest request) {
        if (StringUtils.isBlank((String) request.getAttribute("SAPI_IP_REQUEST_ATTRIBUTE"))) {
            request.setAttribute("SAPI_IP_REQUEST_ATTRIBUTE", ip);
        }
    }

    public SecuredApplication getSecuredApplication() {
        return this.securedApplication;
    }

    public void setSecuredApplication(SecuredApplication securedApplication) {
        this.securedApplication = securedApplication;
    }

    public boolean isValidacionExplicitaDeIPs() {
        return this.validacionExplicitaDeIPs;
    }

    public void setValidacionExplicitaDeIPs(boolean validacionExplicitaDeIPs) {
        this.validacionExplicitaDeIPs = validacionExplicitaDeIPs;
    }
}