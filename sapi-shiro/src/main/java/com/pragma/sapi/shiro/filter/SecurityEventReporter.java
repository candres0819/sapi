package com.pragma.sapi.shiro.filter;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.log.event.AccessEvent;
import com.pragma.sapi.core.log.event.BusEventosAsincrono;
import com.pragma.sapi.shiro.ClientIPResolver;
import com.pragma.sapi.shiro.RequestMetadata;

public class SecurityEventReporter {

    private static final Logger log = LoggerFactory.getLogger(SecurityEventReporter.class);

    public static void registrarEvento(ServletRequest request, String filterName, String eventName, String eventPayload) {
        String requestUUID = "NO_UUID";
        if (null != request.getAttribute("SAPI_UUID_REQUEST")) {
            requestUUID = request.getAttribute("SAPI_UUID_REQUEST").toString();
            log.info("requestUUID: " + requestUUID);
        }

        RequestMetadata meta = RequestMetadata.build(request);
        AccessEvent evt = new AccessEvent(meta.getApplicationName(), meta.getURL(), meta.getSubjectAsString(),
                ClientIPResolver.getClientIpAddr(request), meta.getHeaders(), requestUUID, meta.getSapiSessionId(), filterName, eventName,
                eventPayload);
        BusEventosAsincrono.publicar(evt);
    }
}