package com.pragma.sapi.shiro.services.usecase;

import com.pragma.sapi.shiro.services.usecase.UseCase.RequestValues;
import com.pragma.sapi.shiro.services.usecase.UseCase.ResponseValue;

public abstract class UseCase<Q extends RequestValues, P extends ResponseValue> {

    private Q mRequestValues;
    private UseCaseCallback<P> mUseCaseCallback;

    public Q getRequestValues() {
        return this.mRequestValues;
    }

    public void setRequestValues(Q requestValues) {
        this.mRequestValues = requestValues;
    }

    public UseCaseCallback<P> getUseCaseCallback() {
        return this.mUseCaseCallback;
    }

    public void setUseCaseCallback(UseCaseCallback<P> useCaseCallback) {
        this.mUseCaseCallback = useCaseCallback;
    }

    void run() {
        executeUseCase(this.mRequestValues);
    }

    protected abstract void executeUseCase(Q paramQ);

    public static abstract interface UseCaseCallback<R> {

        public abstract void onSuccess(R paramR);

        public abstract void onError(int paramInt, RuntimeException paramRuntimeException);

    }

    public static abstract interface ResponseValue {

    }

    public static abstract interface RequestValues {

    }
}
