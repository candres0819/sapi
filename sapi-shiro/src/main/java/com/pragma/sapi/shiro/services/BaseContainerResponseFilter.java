package com.pragma.sapi.shiro.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseContainerResponseFilter implements ContainerResponseFilter {

    private static final Logger log = LoggerFactory.getLogger(BaseContainerResponseFilter.class);

    private String[] allHeaders;
    private String[] allHeaderValues;

    public String[] getAllHeaders() {
        return this.allHeaders;
    }

    public void setAllHeaders(String[] allHeaders) {
        this.allHeaders = allHeaders;
    }

    public String[] getAllHeaderValues() {
        return this.allHeaderValues;
    }

    public void setAllHeaderValues(String[] allHeaderValues) {
        this.allHeaderValues = allHeaderValues;
    }

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (this.allHeaders != null && this.allHeaderValues != null) {
            if (this.allHeaders.length != this.allHeaderValues.length) {
                log.warn("El tamaño de la coleccion de headers a aplicar no es valida.");
            } else if (responseContext == null) {
                log.warn("ContainerResponseContext es nulo o no valido. No se aplicaran headers.");
            } else {
                for (int i = 0; i < this.allHeaders.length; ++i) {
                    List<Object> value = new ArrayList<Object>();
                    value.add(this.allHeaderValues[i]);

                    log.trace("Aplicando Header {}:{}", this.allHeaders[i], value);
                    responseContext.getHeaders().put(this.allHeaders[i], value);
                }
            }
        } else {
            log.warn("La coleccion de headers a aplicar esta vacia.");
        }
    }
}