package com.pragma.sapi.shiro.token;

import org.apache.shiro.authc.AuthenticationToken;

public interface JWTAuthenticationToken extends AuthenticationToken {

    String getJWToken();

    String getUuid();

    String getIp();

    String getSessionId();

}
