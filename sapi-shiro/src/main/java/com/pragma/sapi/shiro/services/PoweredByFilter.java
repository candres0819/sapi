package com.pragma.sapi.shiro.services;

import javax.ws.rs.ext.Provider;

@Provider
public class PoweredByFilter extends BaseContainerResponseFilter {

    public static final String POWERED_BY_HEADER = "X-Powered-By";
    public static final String POWERED_BY_HEADER_VALUE = "Evil-minions";
    public static final String XRUNTIME_HEADER = "X-Runtime";
    public static final String XRUNTIME_HEADER_VALUE = "0";
    public static final String XVERSION_HEADER = "X-Version";
    public static final String XVERSION_HEADER_VALUE = "0";

    static final String[] ALL_HEADERs = new String[] { "X-Powered-By", "X-Runtime", "X-Version" };
    static final String[] ALL_HEADER_VALUEs = new String[] { "Evil-minions", "0", "0" };

    public PoweredByFilter() {
        this.setAllHeaders(ALL_HEADERs);
        this.setAllHeaderValues(ALL_HEADER_VALUEs);
    }
}