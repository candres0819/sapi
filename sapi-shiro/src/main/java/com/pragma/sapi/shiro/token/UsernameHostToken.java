package com.pragma.sapi.shiro.token;

import org.apache.shiro.authc.HostAuthenticationToken;

public class UsernameHostToken implements HostAuthenticationToken {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String host;
    private String userPrincipal;

    public UsernameHostToken(String host, String userPrincipal) {
        this.host = host;
        this.userPrincipal = userPrincipal;
    }

    public String getHost() {
        return this.host;
    }

    public Object getPrincipal() {
        return this.userPrincipal;
    }

    public Object getCredentials() {
        return null;
    }
}