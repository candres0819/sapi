package com.pragma.sapi.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.services.auth.SapiIdentityRepositoryImpl;
import com.pragma.sapi.core.services.auth.SapiAuthenticationInfo;
import com.pragma.sapi.core.services.auth.SapiAuthenticationToken;
import com.pragma.sapi.core.services.auth.SapiIdentityRepository;
import com.pragma.sapi.shiro.token.JWTAuthenticationToken;
import com.pragma.sapi.shiro.token.UsernameHostToken;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.util.TokenJWTUtils;

public class LdapAuthenticationRealm implements Realm, SapiRealm {

    private static final Logger log = LoggerFactory.getLogger(LdapAuthenticationRealm.class);

    public static final String REALM_NAME = LdapAuthenticationRealm.class.getSimpleName();

    private SapiIdentityRepositoryImpl sapiIdentityRepository;

    public String getName() {
        return REALM_NAME;
    }

    public boolean supports(AuthenticationToken token) {
        if (token != null) {
            if (JWTAuthenticationToken.class.isAssignableFrom(token.getClass())) {
                ObjectJWT parsed_payload = TokenJWTUtils.parseObjectJWT(((JWTAuthenticationToken) token).getJWToken());
                return this.sapiIdentityRepository.supportsCredentials(parsed_payload.getSubject());
            } else if (UsernamePasswordToken.class.isAssignableFrom(token.getClass())) {
                return this.sapiIdentityRepository.supportsCredentials(((UsernamePasswordToken) token).getUsername());
            } else return UsernameHostToken.class.isAssignableFrom(token.getClass());
        } else {
            log.error("El token de autenticacion no es valido o es nulo");
        }
        return false;
    }

    public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) {
        SapiAuthenticationToken sapiAuthenticationToken = new SapiAuthenticationToken();

        if (JWTAuthenticationToken.class.isAssignableFrom(token.getClass())) {
            sapiAuthenticationToken.setPrincipal(((JWTAuthenticationToken) token).getJWToken());
            return this.parseAuthInfo(this.sapiIdentityRepository.authenticate(sapiAuthenticationToken));
        } else if (UsernameHostToken.class.isAssignableFrom(token.getClass())) {
            sapiAuthenticationToken.setPrincipal(token.getPrincipal());
            sapiAuthenticationToken.setHost(((UsernameHostToken) token).getHost());
            return this.parseAuthInfo(this.sapiIdentityRepository.authenticate(sapiAuthenticationToken));
        } else if (UsernamePasswordToken.class.isAssignableFrom(token.getClass())) {
            sapiAuthenticationToken.setPrincipal(token.getPrincipal());
            sapiAuthenticationToken.setCredentials(token.getCredentials());
            return this.parseAuthInfo(this.sapiIdentityRepository.authenticate(sapiAuthenticationToken));
        } else {
            log.error("El tipo de Token de Autenticacion no es aceptado por este realm");
            throw new AuthenticationException("Autenticacion no exitosa");
        }
    }

    private SapiShiroAutenticationInfo parseAuthInfo(SapiAuthenticationInfo userInfo) {
        SimplePrincipalCollection spc = new SimplePrincipalCollection(userInfo.getPrincipal(), userInfo.getUserProperties().getRealm());
        spc.add(userInfo.getUserProperties(), userInfo.getUserProperties().getRealm());
        return new SapiShiroAutenticationInfo(spc, userInfo.getCredentials());
    }

    public SapiIdentityRepository getSapiIdentityRepository() {
        return this.sapiIdentityRepository;
    }

    public void setSapiIdentityRepository(SapiIdentityRepository sapiIdentityRepository) {
        this.sapiIdentityRepository = (SapiIdentityRepositoryImpl) sapiIdentityRepository;
    }
}