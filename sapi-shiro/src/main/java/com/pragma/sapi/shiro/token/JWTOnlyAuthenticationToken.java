package com.pragma.sapi.shiro.token;

public class JWTOnlyAuthenticationToken implements JWTAuthenticationToken {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String token;
    private String uuidRequest;
    private String uuidSession;
    private String ip;

    public JWTOnlyAuthenticationToken() {
        super();
    }

    public JWTOnlyAuthenticationToken(String token) {
        this.token = token;
    }

    public JWTOnlyAuthenticationToken(String token, String uuidRequest, String uuidSession, String ip) {
        this.token = token;
        this.uuidRequest = uuidRequest;
        this.uuidSession = uuidSession;
        this.ip = ip;
    }

    public Object getPrincipal() {
        return this.getJWToken();
    }

    public Object getCredentials() {
        return this.getJWToken();
    }

    public String getJWToken() {
        return this.token;
    }

    public void setJWToken(String token) {
        this.token = token;
    }

    public String getUuid() {
        return this.uuidRequest;
    }

    public void setUuid(String uuid) {
        this.uuidRequest = uuid;
    }

    public String getSessionId() {
        return this.uuidSession;
    }

    public void setSessionId(String sessionId) {
        this.uuidSession = sessionId;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}