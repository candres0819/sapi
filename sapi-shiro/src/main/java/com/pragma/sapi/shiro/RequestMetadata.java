package com.pragma.sapi.shiro;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.shiro.token.TokenOfAuthHeader;
import com.pragma.sapi.token.ObjectJWT;

public class RequestMetadata {

    private final Logger log = LoggerFactory.getLogger(RequestMetadata.class);

    private ServletRequest request;

    private RequestMetadata(ServletRequest request) {
        this.request = request;
    }

    public static RequestMetadata build(ServletRequest request) {
        return new RequestMetadata(request);
    }

    public String getSubjectAsString() {
        String subject;
        try {
            Subject e = this.getSubject();
            if (e != null) {
                subject = (String) e.getPrincipal();
            } else {
                subject = "SubjectNoDeterminado";
            }
        } catch (Exception arg2) {
            this.log.error("Error Obteniendo Subject del Contexto", arg2);
            subject = "SubjectNoDeterminado";
        }
        return subject;
    }

    public Subject getSubject() {
        Subject currentUser = null;
        try {
            currentUser = SecurityUtils.getSubject();
        } catch (Exception e) {
            this.log.error("{ \"uuid\": \"{}\", \"mensaje\":Error Obteniendo Subject del Contexto",
                    this.request.getAttribute("SAPI_UUID_REQUEST"), e);
        }
        return currentUser;
    }

    public String getURL() {
        HttpServletRequest httpRequest = (HttpServletRequest) this.request;
        String completeURL = null;
        StringBuilder requestURI = new StringBuilder();
        if (null != httpRequest.getRequestURI()) {
            requestURI.append(httpRequest.getRequestURI());
            if (null != httpRequest.getQueryString()) {
                requestURI.append("?").append(httpRequest.getQueryString());
            }
            completeURL = requestURI.toString();
        }
        return completeURL;
    }

    public String getHeaders() {
        HttpServletRequest httpRequest = (HttpServletRequest) this.request;
        StringBuilder buffer = new StringBuilder();
        buffer.append("[");
        if (null != httpRequest.getHeaderNames()) {
            List<String> headerNames = Collections.list(httpRequest.getHeaderNames());
            String allHeaders = (String) headerNames.stream().map((h) -> {
                String hv = httpRequest.getHeader(h);

                if ("Authorization".equalsIgnoreCase(h)) {
                    hv = "-Encabezado omitido por seguridad-";
                }

                return "{ \"" + h + "\" : \"" + hv + "\"}";
            }).collect(Collectors.joining(", "));
            buffer.append(allHeaders);
        }

        buffer.append("]");
        return buffer.toString();
    }

    public String getApplicationName() {
        return SapiEnvironment.getSecuredApplication().getName();
    }

    public String getSapiSessionId() {
        ObjectJWT o = TokenOfAuthHeader.getTokenParsed((HttpServletRequest) this.request);
        return null != o ? o.getUuid() : "NO_UUID";
    }

    public String parse(String mensaje, Object... params) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        buffer.append(String.format("\"sapisessionid\": \"%s\", ", new Object[] { this.getSapiSessionId() }));
        buffer.append(String.format("\"requestid\": \"%s\", ", new Object[] { this.request.getAttribute("SAPI_UUID_REQUEST") }));
        if (this.log.isDebugEnabled() || this.log.isTraceEnabled()) {
            buffer.append(String.format("\"subject\": \"%s\", ", new Object[] { this.getSubjectAsString() }));
            buffer.append(String.format("\"app\": \"%s\", ", new Object[] { this.getApplicationName() }));
            buffer.append(String.format("\"url\": \"%s\", ", new Object[] { this.getURL() }));
            buffer.append(String.format("\"ip\": \"%s\", ", new Object[] { ClientIPResolver.getClientIpAddr(this.request) }));
            buffer.append("\"headers\":");
            buffer.append(this.getHeaders());
            buffer.append(",");
        }

        String mensaje_formateado = String.format(mensaje, params);
        buffer.append(String.format("\"mensaje\": \"%s\"", new Object[] { mensaje_formateado }));
        buffer.append("}");
        return buffer.toString();
    }
}