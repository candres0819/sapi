package com.pragma.sapi.shiro.services.extension;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.servlet.ServletInputStream;
import javax.servlet.ReadListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

public class ResettableStreamHttpServletRequest extends HttpServletRequestWrapper {

    private byte[] rawData;
    private HttpServletRequest request;
    private ResettableServletInputStream servletStream;

    public ResettableStreamHttpServletRequest(HttpServletRequest request) {
        super(request);
        this.request = request;
        this.servletStream = new ResettableServletInputStream();
    }

    public void resetInputStream() {
        this.servletStream.stream = new ByteArrayInputStream(this.rawData);
    }

    public ServletInputStream getInputStream() throws IOException {
        if (this.rawData == null) {
            this.rawData = IOUtils.toByteArray(this.request.getInputStream());
            this.servletStream.stream = new ByteArrayInputStream(this.rawData);
        }
        return this.servletStream;
    }

    public BufferedReader getReader() throws IOException {
        if (this.rawData == null) {
            this.rawData = IOUtils.toByteArray(this.request.getReader(), Charset.defaultCharset());
            this.servletStream.stream = new ByteArrayInputStream(this.rawData);
        }
        return new BufferedReader(new InputStreamReader(this.servletStream));
    }

    private class ResettableServletInputStream extends ServletInputStream {

        private InputStream stream;

        private ResettableServletInputStream() {
            super();
        }

        public int read() throws IOException {
            return this.stream.read();
        }

        public boolean isFinished() {
            return true;
        }

        public boolean isReady() {
            return true;
        }

        public void setReadListener(ReadListener readListener) {
            throw new UnsupportedOperationException();
        }
    }
}