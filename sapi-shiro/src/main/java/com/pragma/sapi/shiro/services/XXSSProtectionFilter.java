package com.pragma.sapi.shiro.services;

import javax.ws.rs.ext.Provider;

@Provider
public class XXSSProtectionFilter extends BaseContainerResponseFilter {

    public static final String XXSS_HEADER = "X-XSS-Protection";
    public static final String XXSS_HEADER_VALUE = "1; mode=block";

    static final String[] ALL_HEADERs = new String[] { "X-XSS-Protection" };
    static final String[] ALL_HEADER_VALUEs = new String[] { "1; mode=block" };

    public XXSSProtectionFilter() {
        this.setAllHeaders(ALL_HEADERs);
        this.setAllHeaderValues(ALL_HEADER_VALUEs);
    }
}