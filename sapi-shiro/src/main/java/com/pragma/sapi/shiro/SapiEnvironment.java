package com.pragma.sapi.shiro;

import com.pragma.sapi.core.SecuredApplication;

/**
 *
 */
public class SapiEnvironment {

    private static SecuredApplication securedApplication;

    public static SecuredApplication getSecuredApplication() {
        return securedApplication;
    }

    public static void setSecuredApplication(SecuredApplication securedApplication) {
        SapiEnvironment.securedApplication = securedApplication;
    }
}