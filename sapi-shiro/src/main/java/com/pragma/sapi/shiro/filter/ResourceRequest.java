package com.pragma.sapi.shiro.filter;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pragma.sapi.core.model.CommandSubmitted;
import com.pragma.sapi.core.services.validators.ValidatorResponse;

public class ResourceRequest implements ValidatorResponse {

    private static final Logger log = LoggerFactory.getLogger(ResourceRequest.class);

    private CommandSubmitted commandSubmitted;
    private String body;
    private boolean isAccessAllowed;
    private Status code;
    private String message;

    public ResourceRequest() {
        this.code = Status.UNAUTHORIZED;
        this.message = "no autorizado";
    }

    public CommandSubmitted getCommandSubmitted() {
        return this.commandSubmitted;
    }

    public void setCommandSubmitted(CommandSubmitted commandSubmitted) {
        this.commandSubmitted = commandSubmitted;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isAccessAllowed() {
        return this.isAccessAllowed;
    }

    public void setAccessAllowed(boolean accessAllowed) {
        this.isAccessAllowed = accessAllowed;
    }

    public Status getCode() {
        return this.code;
    }

    public void setCode(Status code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void onResponse(Status code, String message) {
        this.code = code;
        this.message = message;
    }

    protected boolean esUnRecursoCommand() {
        this.setCommandSubmitted((CommandSubmitted) null);

        try {
            if (log.isTraceEnabled()) {
                log.trace(String.format("Body de la Peticion [%s]", new Object[] { this.getBody() }));
            }

            if (this.getBody() != null) {
                Optional.ofNullable((new Gson()).fromJson(this.getBody(), CommandSubmitted.class)).map((c) -> {
                    log.trace("{}", c);
                    return c;
                }).filter((c) -> {
                    return c.getComando() != null && c.getComando().getNombre() != null;
                }).ifPresent(this::setCommandSubmitted);
            }
        } catch (JsonSyntaxException arg2) {
            log.warn("No es un recurso command o esta mal formado.", arg2);
        } catch (Exception arg3) {
            log.error("Error determinando si es un recurso command", arg3);
        }

        boolean esCommand = this.getCommandSubmitted() != null;
        log.debug(String.format("Determinando si la invocacion es estilo COMMAND = %s", new Object[] { String.valueOf(esCommand) }));
        return esCommand;
    }
}