package com.pragma.sapi.shiro.realm;

import com.pragma.sapi.core.services.auth.SapiIdentityRepository;

public interface SapiRealm {

    SapiIdentityRepository getSapiIdentityRepository();

    void setSapiIdentityRepository(SapiIdentityRepository sapiIdentityRepository);

}
