package com.pragma.sapi.shiro.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class CORSFilter extends BaseContainerResponseFilter {

    private static final Logger log = LoggerFactory.getLogger(CORSFilter.class);

    public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    public static final String ACCESS_CONTROL_ALLOW_ORIGIN_VALUE = "*";
    public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    public static final String ACCESS_CONTROL_ALLOW_HEADERS_VALUE = "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, Accept, CuentaUsuario, ClienteDNI";
    public static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    public static final String ACCESS_CONTROL_ALLOW_METHODS_VALUE = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS_VALUE = "true";

    static final String[] ALL_HEADERs = new String[] { "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials",
            "Access-Control-Allow-Headers", "Access-Control-Allow-Methods" };

    static final String[] ALL_HEADER_VALUEs = new String[] { "*", "true",
            "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, Accept, CuentaUsuario, ClienteDNI",
            "GET, POST, PUT, DELETE, OPTIONS, HEAD" };

    @Context
    private ServletContext context;

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        log.debug("Ejecutando Filtro CORS");
        MultivaluedMap<String, Object> headers = responseContext.getHeaders();
        headers.put("Access-Control-Allow-Origin", new ArrayList<Object>(Arrays.asList("*")));
        headers.put("Access-Control-Allow-Methods", new ArrayList<Object>(Arrays.asList("GET,POST,DELETE,PUT,OPTIONS")));
        headers.put("Access-Control-Max-Age", new ArrayList<Object>(Arrays.asList("180")));
        headers.put("Content-Type", new ArrayList<Object>(Arrays.asList("application/json")));
        headers.put("Access-Control-Allow-Headers", new ArrayList<Object>(Arrays.asList("User-Agent,Referer,Origin,Host,Connection,Access-Control-Request-Method,Access-Control-Request-Headers,Cache-Control,Origin,X-Requested-With,Content-Type,Accept,Accept-Encoding,Accept-Language")));

    }
}