#!groovy

pipeline {
    agent any

    //tools {
        //gradle 'Gradle-4.3.1';
    //}

    environment {
        //MAVEN_SH = "${MAVEN}/bin/mvn"
        //GRADLE_SH = "${GRADLE}/bin/gradle"
        GRADLE_SH = "./gradlew"

        BUILD_MAVEN = 'pom.xml'
        BUILD_GRADLE = 'build.gradle'

        SCM_URL = "git@git.pragma.com.co:Capacidad/sapi.git"
        SCM_BRANCH = "feature/v1.1.0"
        SCM_CREDENTIALS = "devopspragma"

        PROJECT = "sapi"
        PACKAGE = "com/pragma/${PROJECT}"
        FILE_PATTERN = "**/build/libs/*.jar"
    }

    stages {
        stage('GET_CODE') {
            steps {
                echo "[EXEC] - Obtener código fuente desde repositorio Git"
                checkout([
                    $class: 'GitSCM',
                    branches: [
                        [name: "${SCM_BRANCH}"]
                    ],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [
                        [credentialsId: "${SCM_CREDENTIALS}", url: "${SCM_URL}"]
                    ]
                ])
            }
        }

        stage('BUILD_CODE') {
            steps {
                echo "[EXEC] - Compilación de código fuente."
                sh "${GRADLE_SH} build -x test -b ./${BUILD_GRADLE}"
            }
        }

        stage('UNIT_TEST') {
            steps {
                echo "[EXEC] - Ejecución de pruebas unitarias."
                sh "${GRADLE_SH} test"
            }
        }

        stage('CODE_ANA') {
            steps {
                echo "[EXEC] - Análisis estático de código"
                sh "${GRADLE_SH} sonarqube"
            }
        }

        stage('RESULTS') {
            steps {
                echo "[RESULTS] - Resultado pruebas unitarias"
                junit '**/build/test-results/test/TEST-*.xml'
                archive 'build/libs/*.jar'
            }
        }

        stage('ARTIFACT_UP') {
            steps {
                echo "[EXEC] - Almacenando artefactos en Artifactory Server"
                sh "${GRADLE_SH} artifactoryPublish --debug"
            }
        }

        stage("DEPLOY") {
            when {
                branch 'master'    //only run these steps on the master branch
            }
            steps {
                echo "[TODO] - Despliegue de aplicacion"
            }
        }
    }

    post {
        failure {
            mail to: 'carlos.cardona@pragma.com.co',
            subject: 'Failed Pipeline',
            body: "Something is wrong"
        }
    }
}