package com.pragma.sapi.core;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SapiCoreTest {

    final private static Logger log = LoggerFactory.getLogger(SapiCoreTest.class);

    public SapiCoreTest() {
        super();
        log.info("Constructor");
    }

    @Test
    public void testSimple() {
        log.info("Constructor");
    }
}