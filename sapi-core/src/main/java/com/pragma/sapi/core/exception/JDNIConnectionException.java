package com.pragma.sapi.core.exception;

public class JDNIConnectionException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public JDNIConnectionException(String message) {
        super(message);
    }

    public JDNIConnectionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}