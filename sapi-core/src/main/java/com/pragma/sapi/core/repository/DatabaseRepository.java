package com.pragma.sapi.core.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.IDBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.datasource.HikariDBConnection;
import com.pragma.sapi.core.datasource.JNDIConnection;
import com.pragma.sapi.core.datasource.resource.SapiResource;
import com.pragma.sapi.core.datasource.resource.SapiResourceDao;
import com.pragma.sapi.core.datasource.restriction.SapiRestriction;
import com.pragma.sapi.core.datasource.restriction.SapiRestrictionDao;
import com.pragma.sapi.core.datasource.session.SapiSession;
import com.pragma.sapi.core.datasource.session.SapiSessionDao;
import com.pragma.sapi.core.model.StandardRequest;
import com.pragma.sapi.core.repository.extension.RestrictionDBCache;
import com.pragma.sapi.core.repository.impl.DataRepository;
import com.pragma.sapi.core.repository.impl.Filters;

public class DatabaseRepository implements DataRepository {

    private static final Logger log = LoggerFactory.getLogger(DatabaseRepository.class);

    private final IDBI dbi;
    private final RestrictionDBCache cache;
    private Filters filtros = new Filters();

    public DatabaseRepository(JNDIConnection data) {
        this.dbi = new DBI(data.getDataSource());
        this.cache = RestrictionDBCache.getDBToCache(this.dbi);
    }

    public DatabaseRepository(HikariDBConnection dbconn) {
        this.dbi = new DBI(dbconn.getDataSource());
        this.cache = RestrictionDBCache.getDBToCache(this.dbi);
    }

    public DatabaseRepository(IDBI dbi) {
        this.dbi = dbi;
        this.cache = RestrictionDBCache.getDBToCache(dbi);
    }

    public List<SapiResource> getResourceByAppAndRoles(String app, List<String> roles) {
        log.debug("Obteniendo los recursos para los roles [{}]", roles);
        SapiResourceDao sapiResourceDao = this.dbi.open(SapiResourceDao.class);
        List<SapiResource> resources = sapiResourceDao.findAllByApp(app);
        sapiResourceDao.close();

        return (List<SapiResource>) resources.stream().filter((r) -> this.filtros.filterValidRoles(r.getRoles(), roles))
                .collect(Collectors.toList());
    }

    public Optional<SapiRestriction> getOptionalSapiRestriction(StandardRequest requestAndRolesDTO) {
        log.debug(String.format("Obteniendo restricciones para [\'%s\' : \'%s\' : \'%s\']", requestAndRolesDTO.getApp(),
                requestAndRolesDTO.getMethod(), requestAndRolesDTO.getUrl()));
        log.debug(String.format("Los roles que se estan evaluando son %s", requestAndRolesDTO.getRoles()));
        List<SapiRestriction> urlsDeAplicacion = this.cache.getSapiRestrictionsByApp(requestAndRolesDTO.getApp());

        if (null != urlsDeAplicacion && !urlsDeAplicacion.isEmpty()) {
            log.trace("{} Restricciones obtenidas", urlsDeAplicacion.size());
            return this.filtros.filterSapiRestriction(requestAndRolesDTO, urlsDeAplicacion);
        } else {
            log.warn("ATENCION! no se encontraron configuraciones de restricciones en BD");
            return Optional.empty();
        }
    }

    public List<SapiRestriction> getRestrictionByApp(String app) {
        log.debug("Obteniendo las restricciones de \'{}\'", app);
        SapiRestrictionDao sapiRestrictionDao = this.dbi.open(SapiRestrictionDao.class);
        List<SapiRestriction> list = sapiRestrictionDao.findAllByApp(app);
        sapiRestrictionDao.close();
        return list;
    }

    public List<SapiSession> getActiveSessions(String app, String subject) {
        log.debug("Obteniendo las sesiones activa de \'{}\'", subject);
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        List<SapiSession> list = sapiSessionDao.findAllActiveByAppAndSubject(app, subject);
        sapiSessionDao.close();
        return list;
    }

    public void insertNewSession(SapiSession session) {
        log.debug("Creando una nueva sesion \'{}\'", session.getSubject());
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        sapiSessionDao.insert(session);
        sapiSessionDao.close();
    }

    public SapiSession getSessionByUuid(String uuid) {
        log.debug("Consultando sesion por uuid \'{}\'", uuid);
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        List<SapiSession> list = sapiSessionDao.findSessionByUuid(uuid);
        sapiSessionDao.close();

        return list.size() == 1 ? list.get(0) : null;
    }

    public void inactivateExpiredSessionsByAppAndUser(String application, String subject) {
        log.trace("cerrando sesiones con token expirado");
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        sapiSessionDao.inactivateExpiredSessionsByAppAndUser(application, subject, new Date());
        sapiSessionDao.close();
    }

    public void purgeSessions(String application) {
        log.trace("Depurando sesiones antiguas");
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        sapiSessionDao.purgeSessions(application, new Date());
        sapiSessionDao.close();
    }

    public void closeSession(String uuid) {
        log.debug("Marcando sesion \'{}\' como inactiva", uuid);
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        sapiSessionDao.updateActive(0, uuid);
        sapiSessionDao.close();
    }

    public void closeAllActiveSessions(String app, String subject) {
        log.debug("Cerrando todas las sesiones activas de {} en la Aplicacion {}", subject, app);
        SapiSessionDao sapiSessionDao = this.dbi.open(SapiSessionDao.class);
        sapiSessionDao.inactivateAllOpenedSessions(app, subject);
        sapiSessionDao.close();
    }

    @Override
    public void notifySessionEvent(SessionEventType arg0, SapiSession arg1) {

    }
}