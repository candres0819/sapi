package com.pragma.sapi.core.repository.extension;

import java.util.List;

import org.skife.jdbi.v2.IDBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pragma.sapi.core.cache.SapiCache;
import com.pragma.sapi.core.cache.SapiCacheBuilder;
import com.pragma.sapi.core.cache.SapiCacheLoading;
import com.pragma.sapi.core.datasource.restriction.SapiRestriction;
import com.pragma.sapi.core.datasource.restriction.SapiRestrictionDao;
import com.pragma.sapi.core.exception.IllegalCacheException;
import com.pragma.sapi.core.exception.SapiConfigurationException;

public class RestrictionDBCache implements SapiCacheLoading {

    private static final Logger log = LoggerFactory.getLogger(RestrictionDBCache.class);
    private static RestrictionDBCache instance;

    private SapiCache cacheDeRestricciones;
    private IDBI dbi;

    private RestrictionDBCache(IDBI dbi) {
        if (null == this.cacheDeRestricciones) {
            log.debug("Configurando cache de restricciones");
            this.dbi = dbi;
            this.cacheDeRestricciones = SapiCacheBuilder.build("cacheDeRestricciones", this);
        }
    }

    public static RestrictionDBCache getDBToCache(IDBI dbi) {
        if (null == instance) {
            instance = new RestrictionDBCache(dbi);
        }
        return instance;
    }

    @SuppressWarnings("unchecked")
    public List<SapiRestriction> getSapiRestrictionsByApp(String app) {
        try {
            String restricciones = (String) this.cacheDeRestricciones.get(app);
            return (List<SapiRestriction>) new Gson().fromJson(restricciones, new TypeToken<List<SapiRestriction>>() {

            }.getType());
        } catch (Exception e) {
            throw new IllegalCacheException("Se genero un error al intentar recuprar las restricciones", e);
        }
    }

    public String load(String key) {
        if (null != key && !"".equalsIgnoreCase(key.trim())) {
            SapiRestrictionDao sapiRestrictionDao = (SapiRestrictionDao) this.dbi.open(SapiRestrictionDao.class);
            List<SapiRestriction> urls = sapiRestrictionDao.findAllByApp(key);
            sapiRestrictionDao.close();
            return new Gson().toJson(urls);
        }
        throw new SapiConfigurationException("El nombre de APP no es valido para obtener las restricciones");
    }
}
