package com.pragma.sapi.core.exception;

public class SapiRestException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String code;
    private String message;
    private String response;

    public SapiRestException(String code, String message, Throwable cause, String response) {
        super(cause);
        this.code = code;
        this.message = message;
        this.response = response;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getResponse() {
        return this.response;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}