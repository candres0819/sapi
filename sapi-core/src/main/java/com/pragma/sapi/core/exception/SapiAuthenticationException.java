package com.pragma.sapi.core.exception;

public class SapiAuthenticationException extends org.apache.shiro.authc.AuthenticationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    protected int code;

    public SapiAuthenticationException() {
        super();
    }

    public SapiAuthenticationException(String message) {
        super(message);
    }

    public SapiAuthenticationException(Throwable cause) {
        super(cause);
    }

    public SapiAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SapiAuthenticationException(int code, String message, Exception e) {
        super(message, e);
        this.code = code;
    }

    public SapiAuthenticationException(int code, String message, RuntimeException e) {
        super(message, e);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}