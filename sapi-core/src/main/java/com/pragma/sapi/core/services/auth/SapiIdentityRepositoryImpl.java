package com.pragma.sapi.core.services.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.KeyStoreManager;
import com.pragma.sapi.core.directory.tds.LdapFacade;
import com.pragma.sapi.core.exception.AuthenticationException;
import com.pragma.sapi.core.exception.JWTAuthenticationException;
import com.pragma.sapi.core.exception.SapiAuthenticationException;
import com.pragma.sapi.core.exception.SapiMaxNumOfSessionsException;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.ValidatorToken.Valid;
import com.pragma.sapi.token.firm.Firm;
import com.pragma.sapi.token.util.TokenJWTUtils;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

/**
 * 
 * @author carlos.cardona
 *
 */
public class SapiIdentityRepositoryImpl extends LdapFacade implements SapiIdentityRepository {

    private static final Logger log = LoggerFactory.getLogger(SapiIdentityRepositoryImpl.class);

    public static final String AUTH_NAME = SapiIdentityRepositoryImpl.class.getSimpleName();

    private KeyStoreManager keyStoreManager;
    private int numeroMaximoSesiones;

    public KeyStoreManager getKeyStoreManager() {
        return this.keyStoreManager;
    }

    public void setKeyStoreManager(KeyStoreManager keyStoreManager) {
        this.keyStoreManager = keyStoreManager;
    }

    public int getNumeroMaximoSesiones() {
        return this.numeroMaximoSesiones;
    }

    public void setNumeroMaximoSesiones(int numeroMaximoSesiones) {
        this.numeroMaximoSesiones = numeroMaximoSesiones;
    }

    public String getRepositoryName() {
        return AUTH_NAME;
    }

    public boolean supportsCredentials(SapiAuthenticationToken token) {
        if (null != token) {
            if (null != token.getPrincipal()) {
                Matcher matcher = this.jwtPattern.matcher((String) token.getPrincipal());
                boolean matchesAJwt = matcher.matches();
                if (matchesAJwt) {
                    ObjectJWT parsedPayload = TokenJWTUtils.parseObjectJWT((String) token.getPrincipal());
                    return this.supportsCredentials(parsedPayload.getSubject());
                }
                return this.supportsCredentials((String) token.getPrincipal());
            }
            log.error("El principal en el SapiAuthenticationToken es nulo.");
        } else {
            log.error("Token a validar no es valido o es nulo.");
        }
        return false;
    }

    public SapiAuthenticationInfo authenticate(SapiAuthenticationToken token) throws AuthenticationException {
        if (null != token && null != token.getPrincipal()) {
            Matcher matcher = this.jwtPattern.matcher((String) token.getPrincipal());
            boolean matchesAJwt = matcher.matches();
            if (matchesAJwt) {
                return this.getInfoUsuarioAutenticadoConJwt(token);
            } else if (null != token.getPrincipal() && null != token.getCredentials()) {
                return this.getInfoUsuarioConUsrYPwd(token);
            } else if (null != token.getPrincipal() && null != token.getHost()) {
                return this.getInfoUsuarioSinAuthDesdeHost(token);
            } else {
                throw new AuthenticationException("No es un token de autenticacion valido.");
            }
        } else {
            throw new AuthenticationException("No proporciono es un token de autenticacion valido. Token o Principal es NULL.");
        }
    }

    public UserProperties userName(String username) {
        return search(username);
    }

    private SapiAuthenticationInfo getInfoUsuarioAutenticadoConJwt(SapiAuthenticationToken token) throws AuthenticationException {
        try {
            String principal = (String) token.getPrincipal();
            if (null != principal && !"".equalsIgnoreCase(principal.trim())) {
                log.debug("AuthenticationInfo ConJwt para [Subject: {}]", principal);
                ObjectJWT parsedPayload = TokenJWTUtils.parseObjectJWT(principal);

                ObjectJWT objectJWT;
                if (this.getSecuredApplication().getName().equals(parsedPayload.getIssuer())) {
                    objectJWT = this.validateObjectJWTApp(principal, parsedPayload);
                } else {
                    objectJWT = this.validateObjectJWTOrigin(principal, parsedPayload);
                }

                List<UserProperties> usrProps = this.isSubjectValidInRealm(objectJWT.getSubject());
                if (usrProps != null && !usrProps.isEmpty()) {
                    return new SapiAuthenticationInfo(objectJWT.getSubject(), token.getCredentials(), (UserProperties) usrProps.get(0));
                } else {
                    throw new AuthenticationException("No pudo determinar el subject");
                }
            } else {
                throw new AuthenticationException("El JWT para autenticar es nulo o vacio");
            }
        } catch (SignatureException | JWTAuthenticationException | ExpiredJwtException | SapiMaxNumOfSessionsException e) {
            log.error("Error getInfoUsuarioAutenticadoConJwt", e);
            throw e;
        }
    }

    private SapiAuthenticationInfo getInfoUsuarioSinAuthDesdeHost(SapiAuthenticationToken token) throws AuthenticationException {
        log.debug("AuthenticationInfo DesdeHost para [Subject: {}]", token.getPrincipal());
        try {
            List<UserProperties> listUserProperties = this.isSubjectValidInRealm(token.getPrincipal().toString());
            if (null != listUserProperties && !listUserProperties.isEmpty()) {
                return new SapiAuthenticationInfo(token.getPrincipal(), token.getCredentials(), (UserProperties) listUserProperties.get(0));
            } else {
                throw new AuthenticationException("No pudo determinar el subject");
            }
        } catch (RuntimeException e) {
            log.error("Error");
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    private SapiAuthenticationInfo getInfoUsuarioConUsrYPwd(SapiAuthenticationToken token) {
        String principal = token.getPrincipal().toString();
        log.debug("AuthenticationInfo ConUsrYPwd para [Subject: {}]", principal);

        validarLimiteSessiones(principal);

        char[] charArray = (char[]) token.getCredentials();
        String credential = String.valueOf(charArray);

        UserProperties usrProps;
        usrProps = this.loginUser(principal, credential);
        if (null != usrProps) {
            log.debug("Autenticacion exitosa");
            return new SapiAuthenticationInfo(principal, token.getCredentials(), usrProps);
        } else {
            log.debug("Autenticacion no exitosa de {} con las credenciales presentadas", principal);
            throw new AuthenticationException("Autenticacion no exitosa");
        }
    }

    private ObjectJWT validateObjectJWTApp(String token, ObjectJWT parsedPayload) {
        if (parsedPayload.getAlgorithm().isRsa()) {
            Firm<?> privateFirm = this.getPrivateFirm();
            if (null != privateFirm && null != privateFirm.getSigner() && null != privateFirm.getSigner().getKey()) {
                return (new Valid(privateFirm, token)).getObjectJWT();
            } else {
                throw new JWTAuthenticationException(String.format("No se encontro una firma privada para el issuer [%s] en el keystore",
                        new Object[] { parsedPayload.getIssuer() }));
            }
        } else {
            throw new JWTAuthenticationException(
                    String.format("El algoritmo de firma del token no es valido: %s", new Object[] { parsedPayload.getAlgorithm() }));
        }
    }

    private ObjectJWT validateObjectJWTOrigin(String token, ObjectJWT parsedPayload) {
        if (parsedPayload.getAlgorithm().isRsa()) {
            Firm<?> firm = this.getPublicFirm(parsedPayload.getIssuer());
            if (null == firm) {
                throw new JWTAuthenticationException(String.format("No se encontro una firma publica del issuer [%s] en el keystore",
                        new Object[] { parsedPayload.getIssuer() }));
            } else {
                return (new Valid(firm, token)).getObjectJWT();
            }
        } else {
            throw new JWTAuthenticationException(
                    String.format("El algoritmo de firma del token no es valido: %s", parsedPayload.getAlgorithm()));
        }
    }

    private List<UserProperties> isSubjectValidInRealm(String subject) {
        List<UserProperties> usrProps = null;
        if (this.supportsCredentials(subject) && this.validUserName(subject)) {
            UserProperties userProperties = this.getUserProperties(subject);
            if (null != userProperties) {
                usrProps = new ArrayList<UserProperties>();
                usrProps.add(userProperties);
            }
        }
        return usrProps;
    }

    /**
     * Valida el numero de sessiones permitidas desde la configuraion.
     * 
     * @param subject
     */
    private void validarLimiteSessiones(String subject) {
        try {
            int numeroMaximoSesiones = this.getNumeroMaximoSesiones();
            log.debug("Limite de Sesiones {}", Integer.valueOf(numeroMaximoSesiones));
            new SessionManager(this.getSecuredApplication()).checkSessionLimitPolicy(subject, numeroMaximoSesiones);
        } catch (SapiMaxNumOfSessionsException e) {
            log.error("Error por numero de sesiones del usuario", e);
            throw e;
        } catch (Exception e) {
            log.error("error en la validacion de las sesiones del usuario", e);
            throw new SapiAuthenticationException(e.getMessage(), e);
        }
    }

    private Firm<?> getPrivateFirm() {
        if (null != this.keyStoreManager) {
            log.trace("Obtieniendo firma privada del keystore manager...");
            return this.keyStoreManager.getPrivateCert(this.getSecuredApplication().getName(),
                    this.getSecuredApplication().getCertificatePassword());
        } else {
            throw new AuthenticationException("no se pudo encontrar la firma privada de la aplicacion en el keystore");
        }
    }

    private Firm<?> getPublicFirm(String issuer) {
        if (null != this.keyStoreManager) {
            log.trace("Obtieniendo firma publica del keystore manager...");
            return this.keyStoreManager.getPublicCert(issuer);
        } else {
            throw new AuthenticationException("no se pudo encontrar la firma privada de la aplicacion en el keystore");
        }
    }
}