package com.pragma.sapi.core.directory.tds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.pragma.sapi.core.cache.SapiCache;
import com.pragma.sapi.core.cache.SapiCacheBuilder;
import com.pragma.sapi.core.cache.SapiCacheLoading;
import com.pragma.sapi.core.directory.LdapContextFactory;
import com.pragma.sapi.core.directory.SearchResultUtils;
import com.pragma.sapi.core.exception.IllegalCacheException;
import com.pragma.sapi.core.model.UserProperties;

public class LdapCache {

    private static final Logger log = LoggerFactory.getLogger(LdapCache.class);

    public static final String DEFAULT_USER_SEARCH = "(&(objectClass=person)(cn=%s))";
    private static LdapCache instance;

    private LdapContextFactory ldapContextFactory;
    private String searchExpression;
    private String searchBase;
    private String patternGroup;

    private SapiCache cacheUsrProps;
    private SapiCache cacheUsrValid;

    public static LdapCache build(LdapContextFactory ldapCF, Map<String, Object> searchParams) {
        if (null == instance) {
            instance = new LdapCache();
            instance.initialize(ldapCF, searchParams);
        }
        return instance;
    }

    public UserProperties getUserProperties(String key) {
        log.info("LdapCache getUserProperties");
        String data = this.cacheUsrProps.get(key);
        return (UserProperties) new Gson().fromJson(data, UserProperties.class);
    }

    public String getValidUserData(String key) {
        return this.cacheUsrValid.get(key);
    }

    private LdapCache initialize(LdapContextFactory ldapCF, Map<String, Object> searchParams) {
        if ((null == ldapCF) || (null == searchParams) || (searchParams.isEmpty())) {
            throw new IllegalArgumentException("Argumentos ldapContextFactory y/o searchParams no pueden ser nulos");
        }
        if (!searchParams.containsKey("searchBase")) {
            throw new IllegalArgumentException("Argumento searchBase no puede ser nulo o vacio");
        }
        if (!searchParams.containsKey("patternGroup")) {
            throw new IllegalArgumentException("Argumento patternGroup no puede ser nulo o vacio");
        }

        this.ldapContextFactory = ldapCF;
        this.searchBase = ((String) searchParams.get("searchBase"));
        this.patternGroup = ((String) searchParams.get("patternGroup"));
        this.searchExpression = ((String) searchParams.get("searchExpression"));

        if ((null == searchExpression) || ("".equals(searchExpression))) {
            searchExpression = "(&(objectClass=person)(cn=%s))";
        }

        this.cacheUsrProps = (SapiCache) SapiCacheBuilder.build("cacheUsrProps", this.cacheLoader4UserProps);
        this.cacheUsrValid = (SapiCache) SapiCacheBuilder.build("cacheUsrValid", this.cacheLoader4UserValid);

        return this;
    }

    private SapiCacheLoading cacheLoader4UserProps = key -> {
        if (null == key) {
            throw new IllegalCacheException("El key de busqueda no puede ser nulo.");
        }
        log.debug(String.format("Recuperando las propiedades del subject '%s'", new Object[] { key }));

        List<String> listGroups = new ArrayList<String>();
        String filter = String.format(LdapCache.this.searchExpression, new Object[] { key });
        UserProperties userProperties = null;
        try {
            List<Attributes> result = SearchResultUtils.getAttrs(LdapCache.this.ldapContextFactory, LdapCache.this.searchBase, filter);
            if ((result == null) || (result.isEmpty())) {
                LdapCache.log.warn(String.format("No se ha obtenido informacion para el usuario [%s] de TDS", new Object[] { key }));
                return null;
            }
            userProperties = new UserProperties();
            for (Attributes attrs : result) {

                if (null != attrs.get("mail")) {
                    userProperties.setEmail(attrs.get("mail").get().toString());
                }
                if (null != attrs.get("givenName")) {
                    userProperties.setFullname(attrs.get("givenName").get().toString());
                }
                if (null != attrs.get("uid")) {
                    userProperties.setIdentification(attrs.get("uid").get().toString());
                }

                // bmm attributes
                if (null != attrs.get("bmm_id_picture")) {
                    userProperties.setBmmIdPicture(attrs.get("bmm_id_picture").get().toString());
                }
                if (null != attrs.get("bmm_number_document")) {
                    userProperties.setBmmNumberDocument(attrs.get("bmm_number_document").get().toString());
                }
                if (null != attrs.get("bmm_phrase_security")) {
                    userProperties.setBmmPhraseSecurity(attrs.get("bmm_phrase_security").get().toString());
                }
                if (null != attrs.get("bmm_type_document")) {
                    userProperties.setBmmTypeDocument(attrs.get("bmm_type_document").get().toString());
                }

                try {
                    NamingEnumeration<?> elements = attrs.get("o").getAll();
                    while (elements.hasMoreElements()) {
                        String memberof = elements.nextElement().toString();
                        listGroups.add(memberof);
                    }
                    userProperties.setRoles(LdapCache.this.mapPattenMarcher(listGroups));
                } catch (Exception e) {
                    log.info("attributes o no found");
                }

                userProperties.setRoles(listGroups);
            }

            log.info("LdapFacade.class.getCanonicalName(): " + LdapFacade.class.getCanonicalName());
            String realm = "LdapAuthenticationRealm";
            userProperties.setRealm(realm);
        } catch (NamingException e) {
            log.error("Error obteniendo informacion del usuario", e);
            return null;
        }

        return new Gson().toJson(userProperties);
    };

    private SapiCacheLoading cacheLoader4UserValid = new SapiCacheLoading() {

        public String load(String key) {
            if (null == key) {
                throw new IllegalCacheException("El key de busqueda no puede ser nulo.");
            }
            String filter = String.format(LdapCache.this.searchExpression, new Object[] { key });

            return SearchResultUtils.getAttrForSearchResult(LdapCache.this.ldapContextFactory, LdapCache.this.searchBase, filter);
        }

    };

    private List<String> mapPattenMarcher(List<String> listGroups) {
        List<String> listRoles = new ArrayList<String>();
        String pattern = this.patternGroup;
        pattern = pattern.replace("*", "[a-zA-Z_0-9]+").toLowerCase();
        for (String group : listGroups) {
            if (Pattern.compile(pattern).matcher(group.toLowerCase()).find()) {
                listRoles.add(group);
            }
        }
        return listRoles;
    }
}
