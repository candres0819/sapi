package com.pragma.sapi.core.repository;

import java.util.List;

import com.pragma.sapi.core.datasource.resource.SapiResource;

public interface SapiResourceRepo {

    List<SapiResource> getResourceByAppAndRoles(String arg0, List<String> arg1);

}