package com.pragma.sapi.core.schedule;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;

public class SapiSchedulingService {

    private static final Logger log = LoggerFactory.getLogger(SapiSchedulingService.class);

    private static SapiSchedulingService instance = null;
    private static SecuredApplication securedApplication;
    private static final ScheduledExecutorService scheduler = SharedExecutorService.getScheduledExecutor();
    private static Map<String, ScheduledFuture<?>> commandHandles = new HashMap<String, ScheduledFuture<?>>();

    public static void startService(SecuredApplication securedApplication) {
        if (null == instance) {
            SapiSchedulingService.securedApplication = securedApplication;
            instance = new SapiSchedulingService();
            instance.scheduleDepurador(new SessionInactivationCommand(SapiSchedulingService.securedApplication.getDataRepository(),
                    SapiSchedulingService.securedApplication.getName()));
            log.info("Sapi Scheduler Iniciado.");
        }
    }

    public static void stopService() {
        if (instance != null) {
            instance.stop();
        }
    }

    public void stop() {
        log.debug("Deteniendo...");
        Iterator<ScheduledFuture<?>> commandHandlesScheduled = commandHandles.values().iterator();

        while (commandHandlesScheduled.hasNext()) {
            ScheduledFuture<?> handle = commandHandlesScheduled.next();

            if (handle.cancel(false)) {
                log.debug("Tarea {} cancelada.", handle.toString());
            } else {
                log.warn("Tarea {} no fue cancelada, ya habia terminado?", handle.toString());
            }
        }

        scheduler.shutdown();
        log.debug("Detenido.");
    }

    public static SecuredApplication getSecuredApplication() {
        return securedApplication;
    }

    public static ScheduledExecutorService getScheduler() {
        return scheduler;
    }

    public static Map<String, ScheduledFuture<?>> getHandlers() {
        return commandHandles;
    }

    public void scheduleDepurador(Runnable r) {
        LocalDateTime ahora = LocalDateTime.now();
        LocalDateTime mediaNoche = LocalDateTime.now().plusDays(1L).with(ChronoField.MINUTE_OF_DAY, (long) (new SecureRandom()).nextInt(59))
                .with(ChronoField.SECOND_OF_MINUTE, (long) (new SecureRandom()).nextInt(59)).with(ChronoField.MILLI_OF_SECOND, 0L);
        Long tiempoPostMediaNocheEnSegundos = Long.valueOf(ChronoUnit.SECONDS.between(ahora, mediaNoche));

        commandHandles.put("DEPURADOR",
                scheduler.scheduleAtFixedRate(r, tiempoPostMediaNocheEnSegundos.longValue(), 86400L, TimeUnit.SECONDS));
    }
}