package com.pragma.sapi.core.directory.tds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.directory.DirectoryFacade;
import com.pragma.sapi.core.directory.JndiLdapContextFactory;
import com.pragma.sapi.core.directory.LdapContextFactory;
import com.pragma.sapi.core.exception.SapiLdapException;
import com.pragma.sapi.core.model.GroupInfo;
import com.pragma.sapi.core.model.UserProperties;

public abstract class LdapFacade extends DirectoryFacade {

    private static final Logger log = LoggerFactory.getLogger(LdapFacade.class);

    protected Pattern usernamePatternDefault = Pattern.compile("^(?!.*[a-zA-Z]{2})[a-zA-Z]{1}[\\d]+$");

    private SecuredApplication securedApplication;
    private LdapContextFactory ldapContextFactory;

    private String searchBase;
    private String patternGroup;
    private String principalSuffix;
    private String searchExpression;
    private String usernamePattern;

    public LdapFacade() {
        log.trace("Facade configured with default config and credentials for TDS Pruebas");
        this.searchExpression = "(&(objectClass=person)(uid=%s))";
        this.searchBase = "dc=appbmm,dc=com,O=APPBMM";
        this.principalSuffix = "uid=%s,ou=users,dc=appbmm,dc=com,O=APPBMM";
        this.patternGroup = "*";
        this.ldapContextFactory = new JndiLdapContextFactory();

        ((JndiLdapContextFactory) this.ldapContextFactory).setUrl("ldap://10.31.3.20:389");
        ((JndiLdapContextFactory) this.ldapContextFactory).setSystemPassword("cn=root");
        ((JndiLdapContextFactory) this.ldapContextFactory).setSystemPassword((String) null);
        ((JndiLdapContextFactory) this.ldapContextFactory).setPoolingEnabled(true);
    }

    public LdapFacade(String searchExpression, String searchBase, String patternGroup, String principalSuffix,
            LdapContextFactory ldapContextFactory) {
        this.searchExpression = searchExpression;
        this.searchBase = searchBase;
        this.patternGroup = patternGroup;
        this.principalSuffix = principalSuffix;
        this.ldapContextFactory = ldapContextFactory;
    }

    public boolean supportsCredentials(String credentials) {
        String pattern = this.getUsernamePattern();
        Pattern usernamePatternCustom;
        if (null != pattern && !"".equals(pattern)) {
            usernamePatternCustom = Pattern.compile(pattern);
        } else {
            usernamePatternCustom = this.usernamePatternDefault;
        }

        Matcher matcher = usernamePatternCustom.matcher(credentials);
        boolean match = matcher.matches();
        log.debug(this.getClass().getCanonicalName() + " soporta el tipo de credenciales presentadas? {} => {}", credentials,
                Boolean.valueOf(match));
        return match;
    }

    public UserProperties search(String key) {
        log.debug(String.format("Validando subject \'%s\' ", new Object[] { key }));
        Map<String, Object> searchParams = new HashMap<String, Object>();
        searchParams.put("searchBase", this.getSearchBase());
        searchParams.put("patternGroup", this.getPatternGroup());
        searchParams.put("searchExpression", this.getSearchExpression());

        return LdapCache.build(this.ldapContextFactory, searchParams).getUserProperties(key);
    }

    public boolean validUserName(String username) {
        log.debug(String.format("Validando subject \'%s\' ", new Object[] { username }));
        Map<String, Object> searchParams = new HashMap<String, Object>();
        searchParams.put("searchBase", this.getSearchBase());
        searchParams.put("patternGroup", this.getPatternGroup());
        searchParams.put("searchExpression", this.getSearchExpression());

        String result = LdapCache.build(this.ldapContextFactory, searchParams).getValidUserData(username);

        return result != null;
    }

    public UserProperties loginUser(Object username, Object credentials) throws SapiLdapException {
        String logInFmtUsername = this.toFormatUserName(username.toString());
        log.debug(String.format("Intento de login [Subject:%s]", logInFmtUsername));

        LdapContext ctx = null;
        UserProperties usrProps = null;

        try {
            ctx = this.ldapContextFactory.getLdapContext(logInFmtUsername, credentials);
            if (null != ctx) {
                log.debug(String.format("Usuario [%s] autenticado satisfactoriamente en TDS", username));
                usrProps = this.getUserProperties((String) username);

                if (null != usrProps) {
                    log.trace("UserProperties: " + usrProps.toString());
                } else {
                    log.error("No se ha conseguido informacion del usuario necesaria para continuar!!!");
                }
                return usrProps;
            }

            return usrProps;
        } catch (OperationNotSupportedException e) {
            log.error("OperationNotSupportedException Error en proceso de autenticacion", e);
            throw new SapiLdapException(extractLdapCode(e.getMessage()), e.getMessage(), e);
        } catch (NamingException e) {
            log.error("NamingException Error en proceso de autenticacion", e);
            throw new SapiLdapException(extractLdapCode(e.getMessage()), e.getMessage(), e);
        } finally {
            try {
                if (null != ctx) {
                    ctx.close();
                }
            } catch (Exception e) {
                log.error("Error cerrando contexto LDAP. ", e);
            }
        }
    }

    private String toFormatUserName(String username) {
        String newUserName;
        if (null != this.getPrincipalSuffix() && !"".equals(this.getPrincipalSuffix().trim())) {
            newUserName = String.format(this.getPrincipalSuffix(), new Object[] { username });
        } else {
            newUserName = username;
        }
        return newUserName;
    }

    public boolean validUserNameAtGroup(String username, String group) {
        log.warn("Metodo \'validUserNameAtGroup\' en LdapFacade siempre retorna False");
        return false;
    }

    public UserProperties getUserProperties(String subject) {
        UserProperties userProperties = null;
        try {
            Map<String, Object> searchParams = new HashMap<String, Object>();
            searchParams.put("searchBase", this.getSearchBase());
            searchParams.put("patternGroup", this.getPatternGroup());
            searchParams.put("searchExpression", this.getSearchExpression());

            userProperties = LdapCache.build(this.ldapContextFactory, searchParams).getUserProperties(subject);
        } catch (Exception e) {
            log.error("Error", e);
        }

        return userProperties;
    }

    public List<GroupInfo> getUserRoles(String subject) {
        return (List<GroupInfo>) this.getUserProperties(subject).getRoles().stream().map((r) -> new GroupInfo((String) null, r))
                .collect(Collectors.toList());
    }

    public String getSearchExpression() {
        return null != this.searchExpression && !"".equals(this.searchExpression) ? this.searchExpression
                : "(&(objectClass=person)(cn=%s))";
    }

    public void setSearchExpression(String searchExpression) {
        this.searchExpression = searchExpression;
    }

    public String getPatternGroup() {
        return this.patternGroup;
    }

    public void setPatternGroup(String patternGroup) {
        this.patternGroup = patternGroup;
    }

    public LdapContextFactory getLdapContextFactory() {
        return this.ldapContextFactory;
    }

    public void setLdapContextFactory(LdapContextFactory ldapContextFactory) {
        this.ldapContextFactory = ldapContextFactory;
    }

    public String getSearchBase() {
        return this.searchBase;
    }

    public void setSearchBase(String searchBase) {
        this.searchBase = searchBase;
    }

    public String getPrincipalSuffix() {
        return this.principalSuffix;
    }

    public void setPrincipalSuffix(String principalSuffix) {
        this.principalSuffix = principalSuffix;
    }

    public SecuredApplication getSecuredApplication() {
        return this.securedApplication;
    }

    public void setSecuredApplication(SecuredApplication securedApplication) {
        this.securedApplication = securedApplication;
    }

    public String getUsernamePattern() {
        return usernamePattern;
    }

    public void setUsernamePattern(String usernamePattern) {
        this.usernamePattern = usernamePattern;
    }

    private int extractLdapCode(final String exceptionMsg) {
        String pattern = "-?\\d+";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(exceptionMsg);
        if (m.find()) {
            return Integer.valueOf(m.group(0));
        }
        return -1;
    }
}