package com.pragma.sapi.core.datasource.resource;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper({ SapiResourceMapperJdbi.class })
public interface SapiResourceDao {

    @SqlQuery("SELECT * FROM TSAPI_RECURSOS WHERE RECAPLICACION = :app ORDER BY RECORDEN")
    List<SapiResource> findAllByApp(@Bind("app") String arg0);

    void close();
}