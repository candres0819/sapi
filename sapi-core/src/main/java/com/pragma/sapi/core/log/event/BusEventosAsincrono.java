package com.pragma.sapi.core.log.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.AsyncEventBus;
import com.pragma.sapi.core.schedule.SharedExecutorService;

public class BusEventosAsincrono {

    private static final Logger log = LoggerFactory.getLogger(BusEventosAsincrono.class);

    private static AsyncEventBus eventBus = new AsyncEventBus("SapiBusEventosAsincrono", SharedExecutorService.getExecutor());

    public static void subscribir(Object subscriptor) {
        if (null != subscriptor) {
            log.debug("Registrando subscriptor [{}] al bus asincrono.", subscriptor.getClass().getName());
            eventBus.register(subscriptor);
        }
    }

    public static void publicar(Object evento) {
        if (evento instanceof EventoAsincrono) {
            eventBus.post(evento);
        }
    }
}