package com.pragma.sapi.core.log.command;

import java.util.Objects;

import com.pragma.sapi.core.model.AplicacionEmisora;
import com.pragma.sapi.core.model.Usuario;

/**
 * <p>
 * Un <b>comando</b> es una acción que realiza un usuario en la aplicación. Por ejemplo: RadicarSolicitud, AprobarSolicitud,
 * ConsultarSolicitud, AnularSolicitud.
 * </p>
 * Los comandos se generan al iniciar una transacción y pueden o no generar <b>eventos</b>. Cuando finalizan generan uno o varios eventos
 * según lo que indique la naturaleza de la funcionalidad. Cuando no generan evento es porque se generó un error o el comando aún está en
 * ejecución. Para el caso de los errores se puede llevar una trazabilidad del comando si en el log de errores de la aplicación adicional a
 * la fecha, stacktrace, etc. se registra el id del comando para todas las excepciones. Esto permite que se pueda asociar el error generado
 * con el comando y se pueda identificar fácilmente desde el comando a qué afiliado afecta o a qué solicitud, etc.
 * 
 * 
 * <p><b>Definción de comando</b></p>
 * 
 * <pre>
 * <code>
 * {
 *   "comando": {
 *     "idComando": "id unico de la transaccion en el sistema que genera el comando",
 *     "nombre": "nombre del comando",
 *     "idTrazabilidad": "id con el que se desea correlacionar varios comandos, puede ser la cédula del afiliado, el número de trámite, número de solicitud, etc.",
 *     "version": "versión del comando",
 *     "aplicacionEmisora": {
 *       "idAplicacionEmisora": "Código de la aplicación Emisora/origen del comando",
 *       "nombreAplicacionEmisora": "Nombre aplicación Emisora/origen del comando",
 *       "idTransaccionEmisora": "id único en la aplicacion emisora/origen que invoca al comando",
 *       "fechaTransaccion": "Fecha de transacción en la aplicación Emisora"
 *     },
 *     "usuario": {
 *       "identificacion": "cedula del usuario",
 *       "ip": "ip desde donde ingresa el usuario",
 *       "nombre": "nombre del usuario",
 *       "canal": "canal desde donde ingresa el usuario",
 *       "telefono": "telefono desde donde realiza la tx el usuario"
 *     },
 *     "dni": {
 *       "tipoIdentificacion": "Tipo de Identificacion del afiliado asociado al comando ejecutado" ,
 *       "identificacion":"identificacion del afiliado asociado al comando ejecutado"
 *     },
 *     "payloadComando": {
 *       "nombreParametro": "nombre del parametro",
 *       "valorParametro": "valor del parametro"
 *     }
 *   }
 * }
 * </code>
 * </pre>
 * 
 * @author carlos.cardona
 *
 */
public class Command<T> {

    private String idTrazabilidad;
    private String id;
    private String nombre;
    private String version;
    private AplicacionEmisora aplicacionEmisora;
    private Usuario usuario;
    private T payload;

    public Command() {
        super();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String idTrazabilidad() {
        return this.idTrazabilidad;
    }

    public void setIdTrazabilidad(String idTrazabilidad) {
        this.idTrazabilidad = idTrazabilidad;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AplicacionEmisora getAplicacionEmisora() {
        return this.aplicacionEmisora;
    }

    public void setAplicacionEmisora(AplicacionEmisora aplicacionEmisora) {
        this.aplicacionEmisora = aplicacionEmisora;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public T getPayload() {
        return this.payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public int hashCode() {
        return Objects.hash(new Object[] { this.id, this.idTrazabilidad, this.nombre, this.version, this.aplicacionEmisora, this.usuario,
                this.payload });
    }

    public boolean equals(Object command) {
        if (null == command) {
            return false;
        } else if (!(command instanceof Command)) {
            return false;
        } else {
            Command<?> ref = (Command<?>) command;
            return Objects.equals(this.id, ref.id) && Objects.equals(this.idTrazabilidad, ref.idTrazabilidad);
        }
    }

    public String toString() {
        String lineSep = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(lineSep).append(String.format("id: %s,", this.id)).append(lineSep)
                .append(String.format("idTrazabilidad: %s,", this.idTrazabilidad)).append(lineSep)
                .append(String.format("nombre: %s,", this.nombre)).append(lineSep).append(String.format("version: %s", this.version))
                .append(lineSep).append(String.format("aplicacionEmisora: %s", this.aplicacionEmisora)).append(lineSep)
                .append(String.format("usuario: %s", this.usuario)).append(lineSep).append(String.format("payload: %s", this.payload))
                .append(lineSep).append("}");
        return sb.toString();
    }
}