package com.pragma.sapi.core.services;

import com.pragma.sapi.core.cache.SapiCache;
import com.pragma.sapi.core.cache.SapiCacheBuilder;
import com.pragma.sapi.core.cache.SapiCacheLoading;

public class JWTCache implements SapiCache {

    private static JWTCache instance;
    private SapiCache jwtCache;

    private JWTCache() {
        SapiCacheLoading loadingCache = (SapiCacheLoading) jwtCache;
        this.jwtCache = (SapiCache) SapiCacheBuilder.build("cacheDeJwts", loadingCache);
    }

    public static JWTCache getInstance() {
        if (instance == null) {
            instance = new JWTCache();
        }
        return instance;
    }

    public boolean jwtUsado(String jwt) {
        boolean existe = this.jwtCache.contains(jwt);
        this.jwtCache.get(jwt);
        return existe;
    }

    public String load(String key) {
        return "OK";
    }

    @Override
    public String get(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void invalidate(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getCacheName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCacheName(String arg0) {
        // TODO Auto-generated method stub

    }

}
