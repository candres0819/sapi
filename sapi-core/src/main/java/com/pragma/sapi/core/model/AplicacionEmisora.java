package com.pragma.sapi.core.model;

import java.util.Date;
import java.util.Objects;

public class AplicacionEmisora {

    private String idAplicacionEmisora;
    private String nombreAplicacionEmisora;
    private String idTransaccionEmisora;
    private Date fechaTransaccion;

    public String getIdAplicacionEmisora() {
        return this.idAplicacionEmisora;
    }

    public void setIdAplicacionEmisora(String idAplicacionEmisora) {
        this.idAplicacionEmisora = idAplicacionEmisora;
    }

    public String getNombreAplicacionEmisora() {
        return this.nombreAplicacionEmisora;
    }

    public void setNombreAplicacionEmisora(String nombreAplicacionEmisora) {
        this.nombreAplicacionEmisora = nombreAplicacionEmisora;
    }

    public String getIdTransaccionEmisora() {
        return this.idTransaccionEmisora;
    }

    public void setIdTransaccionEmisora(String idTransaccionEmisora) {
        this.idTransaccionEmisora = idTransaccionEmisora;
    }

    public Date getFechaTransaccion() {
        return this.fechaTransaccion;
    }

    public void setFechaTransaccion(Date fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    public int hashCode() {
        return Objects.hash(
                new Object[] { this.idAplicacionEmisora, this.idTransaccionEmisora, this.fechaTransaccion, this.nombreAplicacionEmisora });
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof AplicacionEmisora)) {
            return false;
        } else {
            AplicacionEmisora ref = (AplicacionEmisora) o;
            return Objects.equals(this.idAplicacionEmisora, ref.idAplicacionEmisora)
                    && Objects.equals(this.idTransaccionEmisora, ref.idTransaccionEmisora);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(String.format("idAplicacionEmisora: %s, ", this.idAplicacionEmisora))
                .append(String.format("nombreAplicacionEmisora: %s, ", this.nombreAplicacionEmisora))
                .append(String.format("idTransaccionEmisora: %s, ", this.idTransaccionEmisora))
                .append(String.format("fechaTransaccion: %s", this.fechaTransaccion)).append("}");
        return sb.toString();
    }
}
