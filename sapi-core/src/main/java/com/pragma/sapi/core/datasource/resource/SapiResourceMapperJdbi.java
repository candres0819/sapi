package com.pragma.sapi.core.datasource.resource;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class SapiResourceMapperJdbi implements ResultSetMapper<SapiResource> {

    public static final String TABLE_NAME = "TSAPI_RECURSOS";
    public static final String COLUMN_APP = "RECAPLICACION";
    public static final String COLUMN_ROLES = "RECROLES";
    public static final String COLUMN_RESOURCE = "RECRECURSO";
    public static final String COLUMN_ORDER = "RECORDEN";

    public SapiResource map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        SapiResource sapiResource = new SapiResource();
        sapiResource.setApp(r.getString("RECAPLICACION"));
        sapiResource.setRoles(r.getString("RECROLES"));
        sapiResource.setResource(r.getString("RECRECURSO"));
        return sapiResource;
    }
}