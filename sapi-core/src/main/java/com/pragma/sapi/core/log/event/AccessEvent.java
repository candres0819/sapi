package com.pragma.sapi.core.log.event;

import java.util.Date;

public class AccessEvent extends EventoAsincrono {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String appName;
    private String resource;
    private String subject;
    private String ip;
    private String headers;
    private String filterName;
    private String eventName;
    private Date dateTime;
    private String requestUUID;
    private String sessionUUID;

    public AccessEvent(String appName, String resource, String subject, String ip, String headers, String requestUUID, String sessionUUID,
            String filterName, String eventName, String eventPayload) {
        super(eventPayload);
        this.appName = appName;
        this.resource = resource;
        this.subject = subject;
        this.ip = ip;
        this.headers = headers;
        this.filterName = filterName;
        this.eventName = eventName;
        this.dateTime = new Date();
        this.requestUUID = requestUUID;
        this.sessionUUID = sessionUUID;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getResource() {
        return this.resource;
    }

    public String getIp() {
        return this.ip;
    }

    public String getHeaders() {
        return this.headers;
    }

    public String getRequestUUID() {
        return this.requestUUID;
    }

    public String getSessionUUID() {
        return this.sessionUUID;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getFilterName() {
        return this.filterName;
    }

    public String getEventName() {
        return this.eventName;
    }

    public Date getDateTime() {
        return this.dateTime;
    }

    public String toString() {
        return "{ " + String.format("\"sessionid\": \"%s\", ", new Object[] { this.getSessionUUID() })
                + String.format("\"requestid\": \"%s\", ", new Object[] { this.getRequestUUID() })
                + String.format("\"subject\": \"%s\",", new Object[] { this.getSubject() })
                + String.format("\"app\": \"%s\",", new Object[] { this.getAppName() })
                + String.format("\"url\": \"%s\",", new Object[] { this.getResource() })
                + String.format("\"ip\": \"%s\",", new Object[] { this.getIp() })
                + String.format("\"headers\": \"%s\",", new Object[] { this.getHeaders() })
                + String.format("\"event\": \"%s\",", new Object[] { this.getEventName() })
                + String.format("\"filter\": \"%s\",", new Object[] { this.getFilterName() })
                + String.format("\"timestamp\": %d,", Long.valueOf(this.getDateTime().getTime()))
                + String.format("\"payload\": %s ", new Object[] { this.getEventPayload() }) + "}";
    }
}