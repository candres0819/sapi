package com.pragma.sapi.core.exception;

public class FirmaNoValidaException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public FirmaNoValidaException(String message) {
        super(message);
    }
}