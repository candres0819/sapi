package com.pragma.sapi.core.cache;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class SapiCacheGoogleImpl implements SapiCache {

    private static final Logger log = LoggerFactory.getLogger(SapiCacheGoogleImpl.class);

    private LoadingCache<String, String> googleLoadingCache;
    private String cacheName = "";

    public String getCacheName() {
        return this.cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public SapiCacheGoogleImpl(SapiCacheLoading loadingFn, String name, int timeout) {
        try {
            int timeout1 = 300;
            this.googleLoadingCache = CacheBuilder.newBuilder().maximumSize(1000L).expireAfterWrite((long) timeout1, TimeUnit.SECONDS)
                    .build(new CacheLoader<String, String>() {
                        public String load(String key) throws Exception {
                            return loadingFn.load(key);
                        }
                    });
        } catch (Exception e) {
            log.error("Error en SapiCacheGoogleImpl", e);
        }
    }

    public String get(String key) {
        String value = null;

        try {
            value = this.googleLoadingCache.get(key);
        } catch (Exception var4) {
            log.error("Error accediendo a valor del cache", var4);
        }

        return value;
    }

    public boolean contains(String key) {
        return this.googleLoadingCache.asMap().keySet().stream().anyMatch((k) -> {
            return k.equalsIgnoreCase(key);
        });
    }

    public void invalidate(String key) {
        this.googleLoadingCache.invalidate(key);
    }
}