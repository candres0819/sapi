package com.pragma.sapi.core.datasource;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.exception.SapiConfigurationException;
import com.pragma.sapi.token.UUIDGen;

public class FlatFileConnection implements GenericDataSource {

    private static final Logger log = LoggerFactory.getLogger(FlatFileConnection.class);

    private String filePath;

    public File getDataSource() {
        return new File(this.getFilePath());
    }

    private String checkForTmpDir(String appName) {
        String tmpdir = System.getProperty("java.io.tmpdir");
        if (null != tmpdir) {
            String pathSeparator = new String(new char[] { File.separatorChar });
            log.warn("\n\n*****ATENCION*****: La carpeta TEMP del sistema sera usado como repositorio de Archivos JSON de sapi\n\n");
            File tmpFile = new File(tmpdir + pathSeparator + appName);
            tmpFile.mkdir();
            return tmpFile.getPath();
        } else {
            throw new SapiConfigurationException("No pudo obtener la Carpeta temporal del sistema");
        }
    }

    public boolean checkPermissions() {
        if (StringUtils.isBlank(this.getFilePath())) {
            return false;
        } else {
            File jsonRepoDirectory = new File(this.getFilePath());
            if (!jsonRepoDirectory.isDirectory()) {
                log.error("El repositorio de archivos indicado \'{}\' no es un directorio", jsonRepoDirectory.toString());
                return false;
            } else if (jsonRepoDirectory.canRead() && jsonRepoDirectory.canWrite()) {
                return true;
            } else {
                log.error("El repositorio de archivos indicado \'{}\' no permite acceso de lectura/escritura",
                        jsonRepoDirectory.toString());
                return false;
            }
        }
    }

    public void initializeAlternate(String appName) {
        String tmpAppName = appName;
        if (StringUtils.isBlank(appName)) {
            tmpAppName = UUIDGen.getTimeUUID().toString();
        }

        log.warn("El directorio para el repositorio de archivos JSON no es valido y se va a cambiar");
        this.setFilePath(this.checkForTmpDir(tmpAppName));
    }

    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        if (StringUtils.isNotBlank(filePath)) {
            log.debug("Estableciendo ruta de Repositorio JSON en {}", filePath);
            this.filePath = filePath;
        }
    }
}