package com.pragma.sapi.core.exception;

public class SapiLdapException extends SapiAuthenticationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiLdapException(String message) {
        super(message);
    }

    public SapiLdapException(String message, Exception e) {
        super(message, e);
    }

    public SapiLdapException(String message, RuntimeException e) {
        super(message, e);
    }

    public SapiLdapException(int code, String message, Exception e) {
        super(message, e);
        this.code = code;
    }

    public SapiLdapException(int code, String message, RuntimeException e) {
        super(message, e);
        this.code = code;
    }
}