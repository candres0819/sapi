package com.pragma.sapi.core.model;

public class UserLogin {

    private String user;
    private String password;
    private String ipAddress;
    private String tokenJWT;

    public UserLogin() {
        super();
    }

    public UserLogin(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public UserLogin(String tokenJWT) {
        this.tokenJWT = tokenJWT;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getTokenJWT() {
        return this.tokenJWT;
    }

    public void setTokenJWT(String tokenJWT) {
        this.tokenJWT = tokenJWT;
    }

    public String toString() {
        return String.format("UserLogin [user:%s]", new Object[] { this.user });
    }
}