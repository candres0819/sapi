package com.pragma.sapi.core.cache;

public abstract interface SapiCacheLoading {

    abstract String load(String paramString);

}