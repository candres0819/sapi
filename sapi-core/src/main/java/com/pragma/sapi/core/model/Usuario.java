package com.pragma.sapi.core.model;

import java.util.Objects;

public class Usuario {

    private String identificacion;
    private String ip;
    private String nombre;
    private String canal;
    private String telefono;
    private String idSession;

    public String getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCanal() {
        return this.canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdSession() {
        return this.idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public int hashCode() {
        return Objects.hash(new Object[] { this.identificacion, this.ip, this.nombre, this.canal, this.telefono });
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof Usuario)) {
            return false;
        } else {
            Usuario ref = (Usuario) o;
            return Objects.equals(this.identificacion, ref.identificacion) && Objects.equals(this.ip, ref.ip)
                    && Objects.equals(this.telefono, ref.telefono);
        }
    }

    public String toString() {
        return "{" + String.format("identificacion: %s, ", this.identificacion) + String.format("ip: %s, ", this.ip) +
                String.format("nombre: %s, ", this.nombre) + String.format("canal: %s, ", this.canal) +
                String.format("telefono: %s, ", this.telefono) + String.format("idsesion: %s", this.idSession) + "}";
    }
}