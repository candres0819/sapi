package com.pragma.sapi.core.services.auth;

public class SapiAuthenticationToken {

    private Object principal;
    private Object credentials;
    private String sessionId;
    private String uuid;
    private String host;
    private String ip;

    public SapiAuthenticationToken() {
        super();
    }

    public SapiAuthenticationToken(Object principal) {
        this.principal = principal;
        this.credentials = null;
        this.host = null;
    }

    public SapiAuthenticationToken(Object principal, Object credentials, String host) {
        this.principal = principal;
        this.credentials = credentials;
        this.host = host;
    }

    public SapiAuthenticationToken(Object principal, Object credentials, String host, String uuid, String sessionId, String ip) {
        this.principal = principal;
        this.credentials = credentials;
        this.host = host;
        this.uuid = uuid;
        this.sessionId = sessionId;
        this.ip = ip;
    }

    public Object getPrincipal() {
        return this.principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public Object getCredentials() {
        return this.credentials;
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}