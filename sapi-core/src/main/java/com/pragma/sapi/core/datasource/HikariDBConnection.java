package com.pragma.sapi.core.datasource;

import java.util.Optional;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zaxxer.hikari.HikariDataSource;

public class HikariDBConnection implements DBConnection, GenericDataSource {

    private static final Logger log = LoggerFactory.getLogger(HikariDBConnection.class);

    private HikariDataSource ds = new HikariDataSource();
    private String url;
    private String driveClass;
    private String user;
    private String password;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDriveClass(String driveClass) {
        this.driveClass = driveClass;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public DataSource getDataSource() {
        log.debug("getDataSource {}", this.url);
        this.ds.setJdbcUrl(this.url);
        Optional.ofNullable(this.driveClass).ifPresent((c) -> this.ds.setDriverClassName(c));
        this.ds.setUsername(this.user);
        this.ds.setPassword(this.password);
        this.ds.setIdleTimeout(15000L);
        this.ds.setMaximumPoolSize(4);
        this.ds.setMinimumIdle(1);
        return this.ds;
    }
}