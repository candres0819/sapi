package com.pragma.sapi.core.datasource.restriction;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper({ SapiRestrictionMapperJdbi.class })
public interface SapiRestrictionDao {

    @SqlQuery("SELECT * FROM TSAPI_RESTRICCIONES WHERE RESAPLICACION = :app ORDER BY RESORDEN")
    List<SapiRestriction> findAllByApp(@Bind("app") String arg0);

    void close();

}