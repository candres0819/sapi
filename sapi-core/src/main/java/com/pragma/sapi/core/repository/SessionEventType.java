package com.pragma.sapi.core.repository;

public enum SessionEventType {

    SESSION_CREATED, SESSION_CLOSED

}
