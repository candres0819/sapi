package com.pragma.sapi.core.services.auth;

import com.pragma.sapi.core.KeyStoreManager;
import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.exception.AuthenticationException;
import com.pragma.sapi.core.model.UserProperties;

public abstract interface SapiIdentityRepository {

    abstract boolean supportsCredentials(SapiAuthenticationToken sapiAuthenticationToken);

    abstract SapiAuthenticationInfo authenticate(SapiAuthenticationToken sapiAuthenticationToken) throws AuthenticationException;

    abstract SecuredApplication getSecuredApplication();

    abstract KeyStoreManager getKeyStoreManager();

    abstract int getNumeroMaximoSesiones();

    abstract UserProperties userName(String username);

}