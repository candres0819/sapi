package com.pragma.sapi.core.services.validators;

import java.util.Map;
import java.util.Optional;

import com.pragma.sapi.core.model.StandardRequest;
import com.pragma.sapi.core.repository.SapiRestrictionRepo;
import com.pragma.sapi.core.services.validators.ValidatorResponse.Status;

public class ResourceADValidatorRequest implements ValidatorRequest {

    private final SapiRestrictionRepo repo;

    public ResourceADValidatorRequest(SapiRestrictionRepo repo) {
        this.repo = repo;
    }

    public boolean isValidRequest(StandardRequest requestDTO, Map<String, Object> validatorContext, ValidatorResponse response) {
        Optional<?> optRestriccion = this.repo.getOptionalSapiRestriction(requestDTO);
        if (optRestriccion.isPresent()) {
            validatorContext.put("RestrictionInRequest", optRestriccion.get());
            return true;
        } else {
            response.onResponse(Status.FORBIDDEN, MsgErrors.PERMISSION_PATH.getValue());
            return false;
        }
    }
}