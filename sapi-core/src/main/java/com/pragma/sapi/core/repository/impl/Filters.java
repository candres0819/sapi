package com.pragma.sapi.core.repository.impl;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.datasource.restriction.SapiRestriction;
import com.pragma.sapi.core.model.StandardRequest;

/**
 * 
 * @author carlos.cardona
 *
 */
public class Filters {

    private static final Logger log = LoggerFactory.getLogger(Filters.class);

    public Optional<SapiRestriction> filterSapiRestriction(StandardRequest requestAndRolesDTO, List<SapiRestriction> urlsDeAplicacion) {
        List<SapiRestriction> primerFiltro = (List<SapiRestriction>) urlsDeAplicacion.stream()
                .sorted(Comparator.comparingInt(SapiRestriction::getOrder))
                .filter(u -> this.filterPatten(u.getUrl(), requestAndRolesDTO.getUrl()))
                .filter(m -> this.filterPatten(m.getMethod(), requestAndRolesDTO.getMethod())).filter(e -> {
                    if (null != requestAndRolesDTO.getCommandSubmitted()) {
                        boolean retorno = null != e.getCommand();
                        log.trace(String.format(" --- Verificar filtro comando [ %s != null ] = %s",
                                e.getCommand() != null ? e.getCommand() : "null", String.valueOf(retorno)));
                        return retorno;
                    } else {
                        log.trace(" --- Verificar filtro comando [ No es comando ] = true");
                        return true;
                    }
                }).collect(Collectors.toList());

        log.trace("Restricciones pre-seleccionadas en primer filtro [" + (primerFiltro != null ? primerFiltro.size() : 0) + "]");
        if (null == primerFiltro) {
            return Optional.empty();
        } else {
            List<SapiRestriction> segundoFiltro = (List<SapiRestriction>) primerFiltro.stream()
                    .filter(e -> null == requestAndRolesDTO.getCommandSubmitted()
                            || this.filterValidEvent(e.getCommand(), requestAndRolesDTO.getCommand()))
                    .filter(r -> this.filterValidRoles(r.getRoles(), requestAndRolesDTO.getRoles())).collect(Collectors.toList());

            log.trace("Restricciones pre-seleccionadas en segundo filtro [" + (segundoFiltro != null ? segundoFiltro.size() : 0) + "]");

            if (null == segundoFiltro) {
                return Optional.empty();
            } else {
                SapiRestriction urlMatch = (SapiRestriction) segundoFiltro.stream().findFirst().orElse((SapiRestriction) null);
                log.debug(String.format(" --- La restriccion seleccionada es:\n\n%s\n\n", urlMatch));
                return Optional.ofNullable(urlMatch);
            }
        }
    }

    public boolean filterPatten(String op1, String op2) {
        String tmpOp1 = op1.trim().replace(" ", "").replace("*", "[^$]*");
        tmpOp1 = tmpOp1.concat("+$");
        boolean result = Arrays.stream(tmpOp1.split(",")).filter((o) -> "*".equals(o) || Pattern.compile(o).matcher(op2).find()).findAny()
                .isPresent();
        log.trace(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro regexpr", tmpOp1, op2, Boolean.valueOf(result)));
        return result;
    }

    public boolean filterValidRoles(String roles1, List<String> roles2) {
        String tmpRoles = roles1.trim().replaceAll("^\\s*", "").replaceAll("\\s+$", "");
        if (null == roles2) {
            log.error("No se paso como argumento la lista de roles del usuario");
            return false;
        } else {
            boolean result = "*".equals(tmpRoles) || Arrays.stream(tmpRoles.split(",")).filter((r1) -> {
                Stream<String> rolesStream = roles2.stream();
                String r1Temp = r1.trim();
                r1Temp.getClass();

                return rolesStream.filter(r1Temp::equals).findAny().isPresent();
            }).findAny().isPresent();
            log.trace(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro de roles", tmpRoles, roles2,
                    Boolean.valueOf(result)));
            return result;
        }
    }

    public boolean filterValidEvent(String op1, String op2) {
        boolean result = false;
        if (null != op2) {
            String tmpOp1;
            if (null == op1) {
                tmpOp1 = "*";
            } else {
                tmpOp1 = op1.trim().replace(" ", "").replace("*", "[^$]*");
            }

            result = Arrays.stream(tmpOp1.split(",")).filter((o) -> "*".equals(o) || Pattern.compile(o).matcher(op2).find()).findAny()
                    .isPresent();
        }

        log.trace(String.format("%s [%s es igual %s ] = %s", " --- Verificar filtro de evento", op1, op2, Boolean.valueOf(result)));
        return result;
    }
}