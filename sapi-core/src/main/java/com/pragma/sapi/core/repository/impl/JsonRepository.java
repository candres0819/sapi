package com.pragma.sapi.core.repository.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.datasource.FlatFileConnection;
import com.pragma.sapi.core.datasource.resource.SapiResource;
import com.pragma.sapi.core.datasource.restriction.SapiRestriction;
import com.pragma.sapi.core.datasource.session.SapiSession;
import com.pragma.sapi.core.exception.SapiConfigurationException;
import com.pragma.sapi.core.exception.SapiIOException;
import com.pragma.sapi.core.model.StandardRequest;
import com.pragma.sapi.core.repository.SessionEventType;
import com.pragma.sapi.core.schedule.SapiSchedulingService;

public class JsonRepository implements DataRepository {

    private static final Logger log = LoggerFactory.getLogger(JsonRepository.class);
    private static final String RESOURCES_FILE_NAME = "sapiResources.json";
    private static final String RESTRICTIONS_FILE_NAME = "sapiRestrictions.json";
    private static final String SESSIONS_FILE_NAME = "sapiSessions_%s.json";

    private FlatFileConnection flatFileConnection;
    private ObjectMapper mapper = new ObjectMapper();
    private String pathSeparator;
    private Filters filtros;
    private SecuredApplication securedApplication;
    private static List<SapiSession> sesiones = new ArrayList<SapiSession>();
    private static boolean repoInitialized = false;
    private static boolean usingAlternatePath = false;

    public JsonRepository(FlatFileConnection flatFileConnection, SecuredApplication app) throws SapiConfigurationException {
        this.pathSeparator = new String(new char[] { File.separatorChar });
        this.filtros = new Filters();
        this.securedApplication = app;
        if (null == flatFileConnection) {
            log.warn("No especifico una ruta de configuración de archivos JSON de Sapi.");
            this.flatFileConnection = new FlatFileConnection();
        } else {
            this.flatFileConnection = flatFileConnection;
        }

        if (!this.flatFileConnection.checkPermissions()) {
            this.flatFileConnection.initializeAlternate(this.securedApplication.getName());
            usingAlternatePath = true;
        }
        this.initializeFiles();
    }

    public List<SapiResource> getResourceByAppAndRoles(String app, List<String> roles) {
        List<SapiResource> resources = null;
        try {
            List<SapiResource> allResources = this.mapper.readValue(
                    new File(this.flatFileConnection.getDataSource().getPath() + this.pathSeparator + RESOURCES_FILE_NAME),
                    new TypeReference<List<SapiResource>>() {

                    });
            resources = (List<SapiResource>) allResources.stream().filter((d) -> {
                return this.filtros.filterValidRoles(d.getRoles(), roles);
            }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error obteniendo recursos", e);
        }
        return resources;
    }

    public Optional<SapiRestriction> getOptionalSapiRestriction(StandardRequest requestAndRolesDTO) {
        Optional<SapiRestriction> restriction = Optional.empty();
        try {
            List<SapiRestriction> appRestrictions = this.getRestrictionByApp(requestAndRolesDTO.getApp());
            restriction = this.filtros.filterSapiRestriction(requestAndRolesDTO, appRestrictions);
        } catch (Exception e) {
            log.error("Error obteniendo restriccion", e);
        }
        return restriction;
    }

    public List<SapiRestriction> getRestrictionByApp(String app) {
        List<SapiRestriction> restrictions = null;
        try {
            List<SapiRestriction> allRestrictions = this.mapper.readValue(
                    new File(this.flatFileConnection.getDataSource().getPath() + this.pathSeparator + RESTRICTIONS_FILE_NAME),
                    new TypeReference<List<SapiRestriction>>() {

                    });
            restrictions = (List<SapiRestriction>) allRestrictions.stream().filter((r) -> {
                return r.getApp().equalsIgnoreCase(app);
            }).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Error obteniendo restricciones", e);
        }
        return restrictions;
    }

    public List<SapiSession> getActiveSessions(String app, String subject) {
        synchronized (sesiones) {
            Date fechaActual = new Date();
            List<SapiSession> tmpSesiones = (List<SapiSession>) sesiones.stream().filter((s) -> {
                return s.getApp().equalsIgnoreCase(app) && s.getSubject().equalsIgnoreCase(subject);
            }).filter((s) -> {
                return s.getExpiration().compareTo(fechaActual) > 0;
            }).filter((s) -> {
                return s.getActive() == 1;
            }).collect(Collectors.toList());
            log.debug(String.format("Numero de sesiones activas de '%s' en '%s': %d", subject, app, tmpSesiones.size()));
            return tmpSesiones;
        }
    }

    public void insertNewSession(SapiSession session) {
        synchronized (sesiones) {
            Optional<SapiSession> laSesion = sesiones.stream().filter((s) -> {
                return s.getUuid().equalsIgnoreCase(session.getUuid());
            }).findFirst();
            if (!laSesion.isPresent()) {
                sesiones.add(session);
                log.debug("Sesion con ID {} registrada", session.getUuid());
            } else {
                log.warn("Sesion con ID {} ya existe, no se vuelve a registrar.", session.getUuid());
            }
        }
    }

    public SapiSession getSessionByUuid(String uuid) {
        synchronized (sesiones) {
            Optional<SapiSession> laSesion = sesiones.stream().filter((s) -> {
                return s.getUuid().equalsIgnoreCase(uuid);
            }).findFirst();
            if (laSesion.isPresent()) {
                log.debug("Sesion con ID {} encontrada", uuid);
                return (SapiSession) laSesion.get();
            } else {
                log.debug("Sesion con ID {} NO encontrada", uuid);
                return null;
            }
        }
    }

    public void inactivateExpiredSessionsByAppAndUser(String application, String subject) {
        this.purgeSessions((String) null);
    }

    public void closeSession(String uuid) {
        synchronized (sesiones) {
            Optional<SapiSession> laSesion = sesiones.stream().filter((s) -> {
                return s.getUuid().equalsIgnoreCase(uuid);
            }).findFirst();
            if (laSesion.isPresent()) {
                log.debug("Sesion con ID {} cerrada", uuid);
                ((SapiSession) laSesion.get()).setActive(0);
            } else {
                log.debug("Sesion con ID {} no se puede cerrar, no existe.", uuid);
            }
        }
    }

    public void closeAllActiveSessions(String app, String subject) {
        synchronized (sesiones) {
            sesiones.stream().filter((s) -> {
                return s.getSubject().equalsIgnoreCase(subject) && s.getApp().equalsIgnoreCase(app) && s.getActive() == 1;
            }).forEach(SapiSession::close);
            log.debug("Cerrando todas las sesiones del usuario '{}' en la app '{}'", subject, app);
        }
    }

    private void initializeFiles() throws SapiConfigurationException {
        synchronized (sesiones) {
            if (!repoInitialized) {
                this.mirrorFilesInJar();
                this.touchFiles();
                this.loadSessionsFile();
                SapiSchedulingService.getHandlers().put("JSON_FILE_SYNC", SapiSchedulingService.getScheduler().scheduleAtFixedRate(() -> {
                    this.purgeSessions((String) null);
                    this.saveSessions();
                }, 5L, 5L, TimeUnit.MINUTES));
                log.debug("Scheduler para volcado de archivo JSON iniciado.");
                repoInitialized = true;
            }
        }
    }

    private void mirrorFilesInJar() {
        if (usingAlternatePath) {
            Arrays.stream(this.getFilesArray()).forEach((fileName) -> {
                String fileContent = this.readFileInJar(fileName);
                if (null != fileContent) {
                    this.writeToFile(new File(this.flatFileConnection.getDataSource().getPath() + this.pathSeparator + fileName),
                            fileContent);
                }
            });
        }
    }

    private String readFileInJar(String filename) {
        try {
            InputStream in = this.getClass().getResourceAsStream("/" + filename);
            Throwable throwable = null;

            try {
                if (null == in) {
                    log.debug("Archivo {} no presente en el jar de aplicacion", filename);
                    return null;
                } else {
                    log.debug("Leeyendo archivo {} presente en Jar de la Aplicacion", filename);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder contenido = new StringBuilder();
                    String linea = "";

                    while (null != (linea = reader.readLine())) {
                        contenido.append(linea);
                        contenido.append("\n");
                    }
                    return contenido.toString();
                }
            } catch (Throwable e) {
                throwable = e;
                throw e;
            } finally {
                if (null != in) {
                    if (null != throwable) {
                        try {
                            in.close();
                        } catch (Throwable e) {
                            throwable.addSuppressed(e);
                        }
                    } else {
                        in.close();
                    }
                }
            }
        } catch (IOException e) {
            log.error("Error leyendo archivo contenido en la aplicacion", e);
            return null;
        }
    }

    private void touchFiles() {
        try {
            String basePath = this.flatFileConnection.getDataSource().getPath();
            Arrays.stream(this.getFilesArray()).forEach((fileName) -> {
                File archivo = new File(basePath + this.pathSeparator + fileName);
                if (!archivo.exists()) {
                    this.writeToFile(archivo, "[]");
                } else {
                    log.debug("Archivo {} ya existe", archivo.getAbsolutePath());
                }
            });
        } catch (Exception var2) {
            log.error("Error creando archivos", var2);
        }
    }

    private String[] getFilesArray() {
        return new String[] { RESOURCES_FILE_NAME, RESTRICTIONS_FILE_NAME, this.parseFileName(SESSIONS_FILE_NAME) };
    }

    private void writeToFile(File archivo, String data) throws SapiIOException {
        try {
            RandomAccessFile raf = new RandomAccessFile(archivo, "rw");
            Throwable var4 = null;

            try {
                FileChannel fileChannel = raf.getChannel();
                Throwable var6 = null;

                try {
                    FileLock fileLock = fileChannel.lock();
                    Throwable var8 = null;

                    try {
                        ByteBuffer buf = ByteBuffer.allocate(data.getBytes().length);
                        log.trace("Escribiendo {} bytes de datos en {}", data.getBytes().length, archivo.getPath());
                        buf.clear();
                        buf.put(data.getBytes());
                        buf.flip();

                        while (buf.hasRemaining()) {
                            fileChannel.write(buf);
                        }

                        fileChannel.force(true);
                    } catch (Throwable e) {
                        var8 = e;
                        throw e;
                    } finally {
                        if (null != fileLock) {
                            if (null != var8) {
                                try {
                                    fileLock.close();
                                } catch (Throwable var54) {
                                    var8.addSuppressed(var54);
                                }
                            } else {
                                fileLock.close();
                            }
                        }
                    }
                } catch (Throwable e) {
                    var6 = e;
                    throw e;
                } finally {
                    if (null != fileChannel) {
                        if (null != var6) {
                            try {
                                fileChannel.close();
                            } catch (Throwable e) {
                                var6.addSuppressed(e);
                            }
                        } else {
                            fileChannel.close();
                        }
                    }
                }
            } catch (Throwable e) {
                var4 = e;
                throw e;
            } finally {
                if (null != raf) {
                    if (null != var4) {
                        try {
                            raf.close();
                        } catch (Throwable e) {
                            var4.addSuppressed(e);
                        }
                    } else {
                        raf.close();
                    }
                }
            }
        } catch (Exception e) {
            log.error("Problema accediendo a archivo", e);
            throw new SapiIOException(e.getMessage());
        }
    }

    private void loadSessionsFile() {
        try {
            log.debug("Buscando archivo de sesiones para cargar...");
            File archivo = new File(this.flatFileConnection.getDataSource().getPath() + this.pathSeparator
                    + String.format(SESSIONS_FILE_NAME, this.securedApplication.getName()));
            if (!archivo.exists()) {
                log.debug("Archivo de sesiones no existe.");
                this.touchFiles();
            }

            setSesiones(this.mapper.readValue(archivo, new TypeReference<List<SapiSession>>() {

            }));
            log.debug("Archivo de sesiones cargado.");
        } catch (JsonMappingException e) {
            log.error("Error cargando informacion de archivo de sesiones", e);
        } catch (Exception e) {
            log.error("Error obteniendo sesiones de archivo", e);
        }

    }

    private void saveSessions() {
        synchronized (sesiones) {
            log.trace("Volcando sesiones en archivo JSON...");
            File archivo = new File(this.flatFileConnection.getDataSource().getPath() + this.pathSeparator
                    + String.format(SESSIONS_FILE_NAME, this.securedApplication.getName()));

            try {
                this.writeToFile(archivo, this.mapper.writeValueAsString(sesiones));
            } catch (JsonProcessingException e) {
                log.error("Error convirtiendo a texto informaciónd se sesiones.", e);
            } catch (SapiIOException e) {
                log.error("No se pudo escribir archivo de sesiones", e);
            }

        }
    }

    private String parseFileName(String baseFileName) {
        return String.format(baseFileName, this.securedApplication.getName());
    }

    public void purgeSessions(String app) {
        Date fechaActual = new Date();
        int sesionesActuales = sesiones.size();
        setSesiones(sesiones.stream().filter((s) -> {
            return s.getExpiration().compareTo(fechaActual) >= 0;
        }).collect(Collectors.toList()));
        log.trace("Sesiones Depuradas. {} removidas, {} permanecen.", sesionesActuales - sesiones.size(), sesiones.size());
    }

    public void notifySessionEvent(SessionEventType evt, SapiSession session) {
        synchronized (sesiones) {
            if (null != session && null != session.getApp()) {
                if (this.securedApplication.getName().equalsIgnoreCase(session.getApp())) {
                    if (evt == SessionEventType.SESSION_CREATED) {
                        this.insertNewSession(session);
                    } else if (evt == SessionEventType.SESSION_CLOSED) {
                        this.closeSession(session.getUuid());
                    }
                }
            }
        }
    }

    public static void setSesiones(List<SapiSession> sesiones) {
        JsonRepository.sesiones = sesiones;
    }
}
