package com.pragma.sapi.core.model;

import java.io.Serializable;

public class GroupInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String objectId;
    private String displayName;

    public GroupInfo(String objectId, String displayName) {
        this.objectId = objectId;
        this.displayName = displayName;
    }

    public String getObjectId() {
        return this.objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}