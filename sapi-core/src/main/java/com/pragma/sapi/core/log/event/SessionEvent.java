package com.pragma.sapi.core.log.event;

import com.pragma.sapi.core.datasource.session.SapiSession;
import com.pragma.sapi.core.repository.SessionEventType;

public class SessionEvent extends EventoAsincrono {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private SapiSession session;
    private SessionEventType eventType;

    public SessionEvent(SapiSession session, SessionEventType eventType) {
        super((String) null);
        this.session = session;
        this.eventType = eventType;
    }

    public SapiSession getSession() {
        return this.session;
    }

    public SessionEventType getEventType() {
        return this.eventType;
    }

    public String toString() {
        return String.format("{ \'session\': %s, \'eventType\': \'%s\' }", this.session.toString(),
                this.eventType.equals(SessionEventType.SESSION_CREATED) ? "SESSION_CREATED" : "SESSION_CLOSED");
    }
}