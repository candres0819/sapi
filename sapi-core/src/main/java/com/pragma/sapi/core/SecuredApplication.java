package com.pragma.sapi.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.datasource.FlatFileConnection;
import com.pragma.sapi.core.datasource.GenericDataSource;
import com.pragma.sapi.core.datasource.HikariDBConnection;
import com.pragma.sapi.core.datasource.JNDIConnection;
import com.pragma.sapi.core.exception.SapiConfigurationException;
import com.pragma.sapi.core.repository.DatabaseRepository;
import com.pragma.sapi.core.repository.impl.DataRepository;
import com.pragma.sapi.core.repository.impl.JsonRepository;

public class SecuredApplication {

    private static final Logger log = LoggerFactory.getLogger(SecuredApplication.class);

    private String name;
    private GenericDataSource genericDataSource;
    private String tenant;
    private String clientId;
    private String clientSecret;
    private String stsUrl = "https://login.windows.net/%s/oauth2/token?api-version=1.0";
    private String certificatePassword;
    private String allowOrigin;

    public SecuredApplication() {
        super();
    }

    public GenericDataSource getGenericDataSource() {
        return genericDataSource;
    }

    public void setGenericDataSource(GenericDataSource genericDataSource) {
        if (genericDataSource != null) {
            log.debug("Estableciendo datasource del tipo {}", genericDataSource.getClass().getName());
            this.genericDataSource = genericDataSource;
        } else {
            log.error("El datasource no puede ser nulo");
        }
    }

    public DataRepository getDataRepository() {
        if (null == getGenericDataSource()) {
            log.error("El datasource no fue establecido");
            throw new SapiConfigurationException("El datasource no fue establecido");
        }

        log.trace("Fuente de datos es {}", getGenericDataSource().getClass().getCanonicalName());
        if (genericDataSource instanceof JNDIConnection) {
            return new DatabaseRepository((JNDIConnection) genericDataSource);
        }
        if (genericDataSource instanceof FlatFileConnection) {
            return new JsonRepository((FlatFileConnection) genericDataSource, this);
        }
        return new DatabaseRepository((HikariDBConnection) genericDataSource);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertificatePassword() {
        return certificatePassword;
    }

    public void setCertificatePassword(String certificatePassword) {
        this.certificatePassword = certificatePassword;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getAllowOrigin() {
        return allowOrigin;
    }

    public void setAllowOrigin(String allowOrigin) {
        this.allowOrigin = allowOrigin;
    }

    public String getStsUrl() {
        return stsUrl;
    }

    public void setStsUrl(String stsUrl) {
        this.stsUrl = stsUrl;
    }
}
