package com.pragma.sapi.core.services.validators;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.model.StandardRequest;
import com.pragma.sapi.core.repository.impl.DataRepository;
import com.pragma.sapi.core.services.validators.ValidatorResponse.Status;

public class ResourceTDSValidatorRequest implements ValidatorRequest {

    private static final Logger log = LoggerFactory.getLogger(ResourceTDSValidatorRequest.class);

    private final DataRepository repo;

    public ResourceTDSValidatorRequest(DataRepository repo) {
        this.repo = repo;
    }

    public boolean isValidRequest(StandardRequest requestDTO, Map<String, Object> validatorContext, ValidatorResponse validatorResponse) {
        List<String> roles = requestDTO.getRoles();
        if (null != roles && !roles.isEmpty()) {
            requestDTO.setRoles(roles);

            Optional<?> optRestriccion = this.repo.getOptionalSapiRestriction(requestDTO);
            if (!optRestriccion.isPresent()) {
                log.error("No se encontro una restriccion configurada. Negando acceso");
                validatorResponse.onResponse(Status.FORBIDDEN, MsgErrors.EMPRESA_NO_PERMITIDA.getValue());
                return false;
            } else {
                validatorContext.put("RestrictionInRequest", optRestriccion.get());
                return true;
            }
        } else {
            validatorResponse.onResponse(Status.FORBIDDEN, MsgErrors.EMPRESA_NO_PERMITIDA.getValue());
            return false;
        }
    }
}