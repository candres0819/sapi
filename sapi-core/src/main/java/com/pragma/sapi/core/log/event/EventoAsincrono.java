package com.pragma.sapi.core.log.event;

import java.io.Serializable;

public abstract class EventoAsincrono implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String eventPayload;

    public EventoAsincrono() {
        super();
    }

    public EventoAsincrono(String eventPayload) {
        this.eventPayload = eventPayload;
    }

    public String getEventPayload() {
        return this.eventPayload;
    }
}