package com.pragma.sapi.core.datasource.session;

import java.util.Date;

public class SapiSession {

    private String uuid;
    private int active;
    private String app;
    private String subject;
    private Date expiration;
    private String ip;

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getActive() {
        return this.active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void close() {
        this.setActive(0);
    }

    public String getApp() {
        return this.app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getExpiration() {
        return this.expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String toString() {
        return String.format(
                "{ \"subject\": \"%s\", \"uuid\": \"%s\", \"isActive\": %s, \"app\": \"%s\", \"expiration\": %d, \"ip\": \"%s\" }",
                this.getSubject(), this.getUuid(), Integer.valueOf(this.getActive()), this.getApp(),
                Long.valueOf(this.getExpiration().getTime()), this.getIp());
    }
}