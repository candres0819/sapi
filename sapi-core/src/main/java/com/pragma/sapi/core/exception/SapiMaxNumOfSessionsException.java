package com.pragma.sapi.core.exception;

import com.pragma.sapi.core.exception.util.SapiCodeException;

public class SapiMaxNumOfSessionsException extends SapiAuthenticationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiMaxNumOfSessionsException(String message) {
        super(message);
        this.code = SapiCodeException.SAPI_MAX_NUM_OF_SESSIONS_EXCEPTION.getCode();
    }

    public SapiMaxNumOfSessionsException(String message, RuntimeException e) {
        super(message, e);
        this.code = SapiCodeException.SAPI_MAX_NUM_OF_SESSIONS_EXCEPTION.getCode();
    }

    public SapiMaxNumOfSessionsException(int code, String message, RuntimeException e) {
        super(message, e);
        this.code = SapiCodeException.SAPI_MAX_NUM_OF_SESSIONS_EXCEPTION.getCode();
    }
}