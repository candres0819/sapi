package com.pragma.sapi.core.services;

public enum JwtCacheName {

    JWT_TRANSFER, JWT_SESSION;

    JwtCacheName() {

    }
}