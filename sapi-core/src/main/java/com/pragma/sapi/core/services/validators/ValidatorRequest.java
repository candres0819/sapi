package com.pragma.sapi.core.services.validators;

import java.util.Map;

import com.pragma.sapi.core.model.StandardRequest;

public interface ValidatorRequest {

    String RESTRICTION_KEY_MAP = "RestrictionInRequest";

    boolean isValidRequest(StandardRequest standardRequest, Map<String, Object> validatorContext, ValidatorResponse validatorResponse);

    enum MsgErrors {

        REQUEST_BODY("El Request no es valido, revise su cuerpo y su archivo ini"),
        REQUEST_COMMAND("El Request no es valido, revise su estructura de comando"),
        PERMISSION_PATH("No tiene permisos para la ruta"),
        REQUEST_HEADER("El Request no es valido, revise su cabecera"),
        REQUEST_SIN_ID_CLIENTE("El Request no es valido, no se pudo determinar la identificacion del cliente en header o body. Revise la peticion."),
        REQUEST_NOT_CONFIGURED("El Request no tiene configuracion en tabla de Restricciones"),
        EMPRESA_NO_PERMITIDA("No tiene privilegios sobre la empresa");

        String value;

        MsgErrors(String s) {
            this.value = s;
        }

        public String toString() {
            return this.value;
        }

        public String getValue() {
            return this.value;
        }
    }
}