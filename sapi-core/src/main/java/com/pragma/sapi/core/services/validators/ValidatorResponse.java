package com.pragma.sapi.core.services.validators;

public abstract interface ValidatorResponse {

    public abstract void onResponse(Status paramStatus, String paramString);

    public enum Status {

        BAD_GATEWAY, BAD_REQUEST, UNAUTHORIZED, FORBIDDEN, SERVICE_UNAVAILABLE, OK;

        Status() {

        }
    }
}