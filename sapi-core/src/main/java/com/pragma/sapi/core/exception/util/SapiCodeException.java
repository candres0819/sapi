package com.pragma.sapi.core.exception.util;

public enum SapiCodeException {

    SAPI_MAX_NUM_OF_SESSIONS_EXCEPTION(700, "Error por numero de sesiones del usuario");

    private final int code;
    private final String description;

    SapiCodeException(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
