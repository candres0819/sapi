package com.pragma.sapi.core.exception;

public class SapiValidationException extends org.apache.shiro.authc.AuthenticationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiValidationException(String message) {
        super(message);
    }

    public SapiValidationException(String message, RuntimeException e) {
        super(message, e);
    }

    public SapiValidationException(String message, Exception e) {
        super(message, e);
    }
}