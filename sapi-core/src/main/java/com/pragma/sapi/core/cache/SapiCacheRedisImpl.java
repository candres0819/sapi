package com.pragma.sapi.core.cache;

import redis.clients.jedis.Jedis;

public class SapiCacheRedisImpl implements SapiCache {

    private Jedis jedis = new Jedis();
    private SapiCacheLoading loadingFn;
    private String cacheName;
    private int timeout;

    public SapiCacheRedisImpl(SapiCacheLoading loadingFn, String name, int timeout) {
        this.loadingFn = loadingFn;
        this.cacheName = name;
        this.timeout = timeout;
    }

    public String getCacheName() {
        return this.cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String get(String key) {
        String value = this.jedis.get(this.formatKey(key));
        String NIL_VALUE = "CACHE_NIL";
        if (value != null) {
            return NIL_VALUE.equalsIgnoreCase(value) ? null : value;
        } else {
            value = this.loadingFn.load(key);
            String valueToSave;
            if (value == null) {
                valueToSave = NIL_VALUE;
            } else {
                valueToSave = value;
            }

            this.jedis.set(this.formatKey(key), valueToSave);
            this.jedis.expire(this.formatKey(key), this.timeout);
            return value;
        }
    }

    private String formatKey(String key) {
        return this.getCacheName() + "/" + key;
    }

    public boolean contains(String key) {
        return this.jedis.exists(this.formatKey(key)).booleanValue();
    }

    public void invalidate(String key) {
        this.jedis.del(this.formatKey(key));
    }
}
