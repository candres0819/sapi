package com.pragma.sapi.core.directory;

import java.util.List;
import java.util.regex.Pattern;

import javax.naming.NamingException;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.model.GroupInfo;
import com.pragma.sapi.core.model.UserProperties;

public abstract class DirectoryFacade {

    protected static final String JWT_PATTERN = "^[a-zA-Z0-9\\-_]+?\\.[a-zA-Z0-9\\-_]+?\\.([a-zA-Z0-9\\-_]+)?$";

    protected Pattern jwtPattern = Pattern.compile("^[a-zA-Z0-9\\-_]+?\\.[a-zA-Z0-9\\-_]+?\\.([a-zA-Z0-9\\-_]+)?$");

    protected abstract UserProperties loginUser(Object arg0, Object arg1) throws NamingException;

    protected abstract boolean supportsCredentials(String arg0);

    public abstract boolean validUserName(String arg0);

    public abstract boolean validUserNameAtGroup(String arg0, String arg1);

    public abstract String getRepositoryName();

    public abstract void setSearchBase(String arg0);

    public abstract void setPatternGroup(String arg0);

    public abstract void setPrincipalSuffix(String arg0);

    public abstract void setSearchExpression(String arg0);

    public abstract void setSecuredApplication(SecuredApplication arg0);

    public abstract List<GroupInfo> getUserRoles(String arg0);

    public abstract UserProperties getUserProperties(String arg0);

    public abstract SecuredApplication getSecuredApplication();
}