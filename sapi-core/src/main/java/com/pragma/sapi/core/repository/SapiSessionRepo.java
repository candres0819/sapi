package com.pragma.sapi.core.repository;

import java.util.List;

import com.pragma.sapi.core.datasource.session.SapiSession;

public interface SapiSessionRepo {

    List<SapiSession> getActiveSessions(String arg0, String arg1);

    void insertNewSession(SapiSession arg0);

    SapiSession getSessionByUuid(String arg0);

    void inactivateExpiredSessionsByAppAndUser(String arg0, String arg1);

    void purgeSessions(String arg0);

    void closeSession(String arg0);

    void closeAllActiveSessions(String arg0, String arg1);

    void notifySessionEvent(SessionEventType arg0, SapiSession arg1);

}