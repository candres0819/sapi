package com.pragma.sapi.core.datasource;

import com.pragma.sapi.core.exception.SapiConfigurationException;

public abstract interface GenericDataSource {

    public abstract Object getDataSource() throws SapiConfigurationException;

}
