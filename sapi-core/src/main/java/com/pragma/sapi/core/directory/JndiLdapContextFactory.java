package com.pragma.sapi.core.directory;

import java.util.*;
import java.util.function.Predicate;
import javax.naming.AuthenticationException;
import javax.naming.NamingException;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JndiLdapContextFactory implements LdapContextFactory {

    protected static final String SUN_CONNECTION_POOLING_PROPERTY = "com.sun.jndi.ldap.connect.pool";
    protected static final String DEFAULT_CONTEXT_FACTORY_CLASS_NAME = "com.sun.jndi.ldap.LdapCtxFactory";
    protected static final String SIMPLE_AUTHENTICATION_MECHANISM_NAME = "simple";
    protected static final String DEFAULT_REFERRAL = "follow";
    protected static final String ERROR_MSG_CREDENTIALS_REQUIRED = "LDAP Simple authentication requires both a principal and credentials.";
    private static final Logger log = LoggerFactory.getLogger(JndiLdapContextFactory.class);
    private Map<String, Object> environment = new HashMap<String, Object>();
    private boolean poolingEnabled;
    private String systemPassword;
    private String systemUsername;

    public JndiLdapContextFactory() {
        this.setContextFactoryClassName("com.sun.jndi.ldap.LdapCtxFactory");
        this.setReferral("follow");
        this.poolingEnabled = true;
    }

    public void setAuthenticationMechanism(String authenticationMechanism) {
        this.setEnvironmentProperty("java.naming.security.authentication", authenticationMechanism);
    }

    public String getAuthenticationMechanism() {
        return (String) this.getEnvironmentProperty("java.naming.security.authentication");
    }

    public void setContextFactoryClassName(String contextFactoryClassName) {
        this.setEnvironmentProperty("java.naming.factory.initial", contextFactoryClassName);
    }

    public String getContextFactoryClassName() {
        return (String) this.getEnvironmentProperty("java.naming.factory.initial");
    }

    public Map<String, Object> getEnvironment() {
        return this.environment;
    }

    public void setEnvironment(Map<String, Object> env) {
        this.environment = env;
    }

    private Object getEnvironmentProperty(String name) {
        return this.environment.get(name);
    }

    private void setEnvironmentProperty(String name, String value) {
        if (StringUtils.isNotBlank(value)) {
            this.environment.put(name, value);
        } else {
            this.environment.remove(name);
        }
    }

    public boolean isPoolingEnabled() {
        return this.poolingEnabled;
    }

    public void setPoolingEnabled(boolean poolingEnabled) {
        this.poolingEnabled = poolingEnabled;
    }

    public void setReferral(String referral) {
        this.setEnvironmentProperty("java.naming.referral", referral);
    }

    public String getReferral() {
        return (String) this.getEnvironmentProperty("java.naming.referral");
    }

    public void setUrl(String url) {
        this.setEnvironmentProperty("java.naming.provider.url", url);
    }

    public String getUrl() {
        return (String) this.getEnvironmentProperty("java.naming.provider.url");
    }

    public void setSystemPassword(String systemPassword) {
        this.systemPassword = systemPassword;
    }

    public String getSystemPassword() {
        return this.systemPassword;
    }

    public void setSystemUsername(String systemUsername) {
        this.systemUsername = systemUsername;
    }

    public String getSystemUsername() {
        return this.systemUsername;
    }

    public LdapContext getSystemLdapContext() throws NamingException {
        return this.getLdapContext((Object) this.getSystemUsername(), (Object) this.getSystemPassword());
    }

    /** @deprecated */
    @Deprecated
    public LdapContext getLdapContext(String username, String password) throws NamingException {
        return this.getLdapContext((Object) username, (Object) password);
    }

    protected boolean isPoolingConnections(Object principal) {
        return this.isPoolingEnabled() && principal != null && principal.equals(this.getSystemUsername());
    }

    public LdapContext getLdapContext(Object principal, Object credentials) throws NamingException, IllegalStateException {
        String url = this.getUrl();
        if (url == null) {
            throw new IllegalStateException("An LDAP URL must be specified of the form ldap://<hostname>:<port>");
        } else {
            Hashtable<String, Object> env = new Hashtable<String, Object>(this.environment);
            Object authcMech = this.getAuthenticationMechanism();
            if (null == authcMech && (null != principal || null != credentials)) {
                env.put("java.naming.security.authentication", "simple");
            }

            if (null != principal) {
                env.put("java.naming.security.principal", principal);
            }

            if (null != credentials) {
                env.put("java.naming.security.credentials", credentials);
            }

            boolean pooling = this.isPoolingConnections(principal);
            if (pooling) {
                env.put("com.sun.jndi.ldap.connect.pool", "true");
            }

            if (log.isDebugEnabled()) {
                log.debug("Initializing LDAP context using URL [{}] and principal [{}] with pooling {}",
                        new Object[] { url, principal, pooling ? "enabled" : "disabled" });
            }

            this.validateAuthenticationInfo(env);
            return this.createLdapContext(env);
        }
    }

    protected LdapContext createLdapContext(Hashtable<String, Object> env) throws NamingException {
        return new InitialLdapContext(env, (Control[]) null);
    }

    protected void validateAuthenticationInfo(Hashtable<String, Object> environment) throws AuthenticationException {
        String mechName = (String) environment.get("java.naming.security.authentication");
        String secPrincipal = (String) environment.get("java.naming.security.principal");
        Object credentials = environment.get("java.naming.security.credentials");

        if ("simple".equals(mechName) && environment.get("java.naming.security.principal") != null
                && StringUtils.isNotBlank(secPrincipal)) {
            Predicate<Object> credentialsChecker = new Predicate<Object>() {
                public boolean test(Object t) {
                    if (t == null) {
                        return false;
                    } else if (t instanceof byte[]) {
                        return ((byte[]) ((byte[]) t)).length > 0;
                    } else if (t instanceof char[]) {
                        return ((char[]) ((char[]) t)).length > 0;
                    } else {
                        return t instanceof String ? StringUtils.isNotBlank((String) t) : false;
                    }
                }
            };
            if ((new HashSet<Object>(Collections.singletonList(credentials))).stream().noneMatch(credentialsChecker)) {
                throw new AuthenticationException("LDAP Simple authentication requires both a principal and credentials.");
            }

            if (log.isTraceEnabled()) {
                log.trace("Ldap Credentials not empty");
            }
        }
    }
}
