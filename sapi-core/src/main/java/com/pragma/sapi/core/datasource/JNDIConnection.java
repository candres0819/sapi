package com.pragma.sapi.core.datasource;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.exception.JDNIConnectionException;

public class JNDIConnection implements GenericDataSource {

    private static final Logger log = LoggerFactory.getLogger(JNDIConnection.class);

    private String resRefName;

    public JNDIConnection() {
        super();
    }

    public DataSource getDataSource() {
        log.debug(String.format("%s [%s]", new Object[] { "getDataSource", resRefName }));
        try {
            return (DataSource) InitialContext.doLookup(getResRefName());
        } catch (NamingException e) {
            log.error("Problemas en obtener el datasource reference", e);
            throw new JDNIConnectionException("Problemas en obtener el datasource referencia", e);
        }
    }

    public String getResRefName() {
        return resRefName;
    }

    public void setResRefName(String resRefName) {
        this.resRefName = resRefName;
    }
}