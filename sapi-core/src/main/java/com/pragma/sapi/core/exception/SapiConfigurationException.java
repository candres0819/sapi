package com.pragma.sapi.core.exception;

public class SapiConfigurationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SapiConfigurationException(String message) {
        super(message);
    }
}
