package com.pragma.sapi.core.datasource.restriction;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class SapiRestrictionMapperJdbi implements ResultSetMapper<SapiRestriction> {

    public static final String TABLE_NAME = "TSAPI_RESTRICCIONES";
    public static final String COLUMN_APP = "RESAPLICACION";
    public static final String COLUMN_ROLES = "RESROLES";
    public static final String COLUMN_URL = "RESURL";
    public static final String COLUMN_COMMAND = "RESCOMANDO";
    public static final String COLUMN_METHOD = "RESMETODO";
    public static final String COLUMN_ORDER = "RESORDEN";
    public static final String COLUMN_STRATEGY = "RESESTRATEGIA";

    public SapiRestriction map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        SapiRestriction restriction = new SapiRestriction();
        restriction.setUrl(r.getString("RESURL"));
        restriction.setApp(r.getString(COLUMN_APP));
        restriction.setMethod(r.getString("RESMETODO"));
        restriction.setCommand(r.getString(COLUMN_COMMAND));
        restriction.setRoles(r.getString("RESROLES"));
        restriction.setOrder(r.getInt("RESORDEN"));
        restriction.setStrategy(r.getString("RESESTRATEGIA"));
        return restriction;
    }
}