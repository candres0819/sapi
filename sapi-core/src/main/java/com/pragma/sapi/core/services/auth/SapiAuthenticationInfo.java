package com.pragma.sapi.core.services.auth;

import com.pragma.sapi.core.model.UserProperties;

public class SapiAuthenticationInfo {

    private Object principal;
    private Object credentials;
    private UserProperties userProperties;

    public SapiAuthenticationInfo(Object principal, Object credentials, UserProperties userProperties) {
        this.principal = principal;
        this.credentials = credentials;
        this.userProperties = userProperties;
    }

    public Object getPrincipal() {
        return this.principal;
    }

    public Object getCredentials() {
        return this.credentials;
    }

    public UserProperties getUserProperties() {
        return this.userProperties;
    }
}
