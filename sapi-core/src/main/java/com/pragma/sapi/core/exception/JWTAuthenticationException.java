package com.pragma.sapi.core.exception;

public class JWTAuthenticationException extends AuthenticationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public JWTAuthenticationException(String message) {
        super(message);
    }

    public JWTAuthenticationException(String message, RuntimeException e) {
        super(message, e);
    }

    public JWTAuthenticationException(String message, Exception e) {
        super(message, e);
    }
}