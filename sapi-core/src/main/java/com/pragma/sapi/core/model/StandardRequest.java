package com.pragma.sapi.core.model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StandardRequest {

    private final String app;
    private final String url;
    private final String method;
    private List<String> roles;
    private String command;
    private String body;
    private Map<String, String> headers;
    private CommandSubmitted commandSubmitted;

    public StandardRequest(String app, String url, String method, String command, List<String> roles) {
        this.app = app;
        this.url = url;
        this.method = method;
        this.roles = roles;
        this.command = command;
    }

    public StandardRequest(String app, String url, String method, List<String> roles) {
        this(app, url, method, (String) null, roles);
    }

    public CommandSubmitted getCommandSubmitted() {
        return this.commandSubmitted;
    }

    public void setCommandSubmitted(CommandSubmitted commandSubmitted) {
        this.commandSubmitted = commandSubmitted;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String event) {
        this.command = event;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getApp() {
        return this.app;
    }

    public String getUrl() {
        return this.url;
    }

    public String getMethod() {
        return this.method;
    }

    public List<String> getRoles() {
        return this.roles;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(String.format(
                "\'requestAndRolesDTO\': { \'app\': \'%s\', \'url\':\'%s\', \'method\':\'%s\', \'command\':\'%s\', \'roles\': [", this.app,
                this.url, this.method, this.command));

        if (null != this.roles && !this.roles.isEmpty()) {
            buffer.append((String) this.roles.stream().collect(Collectors.joining(",")));
        }

        buffer.append("] }");
        return buffer.toString();
    }
}