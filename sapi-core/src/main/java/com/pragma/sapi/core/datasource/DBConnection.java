package com.pragma.sapi.core.datasource;

public interface DBConnection {

    void setPassword(String psw);

    void setUser(String user);

    void setUrl(String url);

    void setDriveClass(String driverClass);

}