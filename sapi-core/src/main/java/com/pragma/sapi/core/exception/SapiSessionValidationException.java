package com.pragma.sapi.core.exception;

public class SapiSessionValidationException extends SapiValidationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiSessionValidationException(String message) {
        super(message);
    }

    public SapiSessionValidationException(String message, RuntimeException e) {
        super(message, e);
    }
}