package com.pragma.sapi.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.token.firm.Firm;
import com.pragma.sapi.token.firm.RSAPrivateKeyStoreFirm;
import com.pragma.sapi.token.firm.RSAPublicKeyStoreFirm;

public class KeyStoreManager {

    private static final Logger log = LoggerFactory.getLogger(KeyStoreManager.class);

    private String keyStoreFile;
    private String keyStorePassword;

    public String getKeyStoreFile() {
        return this.keyStoreFile;
    }

    public void setKeyStoreFile(String keyStoreFile) {
        this.keyStoreFile = keyStoreFile;
    }

    public String getKeyStorePassword() {
        return this.keyStorePassword;
    }

    public void setKeyStorePassword(String keyStorePassword) {
        this.keyStorePassword = keyStorePassword;
    }

    public Firm<?> getPublicCert(String alias) {
        log.debug("Buscando certificado publico con alias [" + alias + "]");
        RSAPublicKeyStoreFirm rsaPublic = new RSAPublicKeyStoreFirm(this.getKeyStoreFile());
        rsaPublic.setCertAlias(alias);
        rsaPublic.setPasswd(this.getKeyStorePassword());
        return rsaPublic;
    }

    public Firm<?> getPrivateCert(String alias, String securedApplicationPassword) {
        log.debug("Buscando certificado privado con alias [" + alias + "]");
        RSAPrivateKeyStoreFirm rsaPrivate = new RSAPrivateKeyStoreFirm(this.getKeyStoreFile());
        rsaPrivate.setCertPasswd(securedApplicationPassword);
        rsaPrivate.setCertAlias(alias);
        rsaPrivate.setPasswd(this.getKeyStorePassword());
        return rsaPrivate;
    }
}