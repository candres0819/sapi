package com.pragma.sapi.core.model;

import java.util.Map;

public class StandardResponse {

    private Header header;
    private Body body;

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public static class Header {

        private int status;
        private String message;

        public Header() {
            super();
        }

        public Header(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return this.status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class Body {

        private Map<String, Object> data;

        public Map<String, Object> getData() {
            return this.data;
        }

        public void setData(Map<String, Object> data) {
            this.data = data;
        }
    }
}
