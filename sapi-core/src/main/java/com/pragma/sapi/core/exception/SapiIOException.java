package com.pragma.sapi.core.exception;

public class SapiIOException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiIOException(String message) {
        super(message);
    }
}
