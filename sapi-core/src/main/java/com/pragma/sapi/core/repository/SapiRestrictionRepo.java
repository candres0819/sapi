package com.pragma.sapi.core.repository;

import java.util.List;
import java.util.Optional;

import com.pragma.sapi.core.datasource.restriction.SapiRestriction;
import com.pragma.sapi.core.model.StandardRequest;

public interface SapiRestrictionRepo {

    Optional<SapiRestriction> getOptionalSapiRestriction(StandardRequest arg0);

    List<SapiRestriction> getRestrictionByApp(String arg0);

}
