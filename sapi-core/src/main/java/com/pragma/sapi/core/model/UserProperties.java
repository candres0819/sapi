package com.pragma.sapi.core.model;

import java.util.ArrayList;
import java.util.List;

public class UserProperties {

    private String fullname;
    private String email;
    private String identification;
    private String tipoIdentification;
    private List<String> roles = new ArrayList<String>();
    private String realm;
    private String ipAddress;
    private String ultimoIngreso;

    private String bmmIdPicture;
    private String bmmNumberDocument;
    private String bmmPhraseSecurity;
    private String bmmTypeDocument;

    private String uuidRequest;
    private String uuidSession;

    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return this.identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getTipoIdentification() {
        return this.tipoIdentification;
    }

    public void setTipoIdentification(String tipoIdentification) {
        this.tipoIdentification = tipoIdentification;
    }

    public List<String> getRoles() {
        return this.roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getRealm() {
        return this.realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUltimoIngreso() {
        return ultimoIngreso;
    }

    public void setUltimoIngreso(String ultimoIngreso) {
        this.ultimoIngreso = ultimoIngreso;
    }

    public String getBmmIdPicture() {
        return bmmIdPicture;
    }

    public void setBmmIdPicture(String bmmIdPicture) {
        this.bmmIdPicture = bmmIdPicture;
    }

    public String getBmmNumberDocument() {
        return bmmNumberDocument;
    }

    public void setBmmNumberDocument(String bmmNumberDocument) {
        this.bmmNumberDocument = bmmNumberDocument;
    }

    public String getBmmPhraseSecurity() {
        return bmmPhraseSecurity;
    }

    public void setBmmPhraseSecurity(String bmmPhraseSecurity) {
        this.bmmPhraseSecurity = bmmPhraseSecurity;
    }

    public String getBmmTypeDocument() {
        return bmmTypeDocument;
    }

    public void setBmmTypeDocument(String bmmTypeDocument) {
        this.bmmTypeDocument = bmmTypeDocument;
    }

    public String getUuidRequest() {
        return this.uuidRequest;
    }

    public void setUuidRequest(String uuidRequest) {
        this.uuidRequest = uuidRequest;
    }

    public String getUuidSession() {
        return this.uuidSession;
    }

    public void setUuidSession(String uuidSession) {
        this.uuidSession = uuidSession;
    }

    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append(String.format("{ \'fullname\': \'%s\', ", new Object[] { this.fullname }));
        buff.append(String.format("\'email\': \'%s\', ", new Object[] { this.email }));
        buff.append(String.format("\'identification\': \'%s-%s\', ", this.tipoIdentification, this.identification));
        buff.append(String.format("\'realm\': \'%s\', ", new Object[] { this.realm }));
        if (null != this.roles) {
            buff.append(String.format("\'roles\': %d, ", Integer.valueOf(this.roles.size())));
        } else {
            buff.append("\'roles\': 0, ");
        }

        buff.append(String.format("\'uuidRequest\': \'%s\', ",
                new Object[] { this.getUuidRequest() != null ? this.uuidRequest : "InfoNoDisponible" }));
        buff.append(String.format("\'uuidSession\': \'%s\', ",
                new Object[] { this.getUuidRequest() != null ? this.uuidSession : "InfoNoDisponible" }));
        buff.append(String.format("\'ipAddress\': \'%s\', ",
                new Object[] { this.getIpAddress() != null ? this.ipAddress : "InfoNoDisponible" }));
        buff.append(" }");
        return buff.toString();
    }
}