package com.pragma.sapi.core.exception;

public class IllegalCacheException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public IllegalCacheException(String message) {
        super(message);
    }

    public IllegalCacheException(String message, Exception e) {
        super(message, e);
    }
}