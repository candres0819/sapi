package com.pragma.sapi.core.datasource.session;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class SapiSessionMapperJdbi implements ResultSetMapper<SapiSession> {

    public static final String TABLE_NAME = "TSAPI_SESIONES";
    public static final String COLUMN_ID = "SESSESION";
    public static final String COLUMN_APP = "SESAPLICACION";
    public static final String COLUMN_UUID = "SESUUID";
    public static final String COLUMN_IP = "SESIP";
    public static final String COLUMN_EXPIRATION = "SESFEVENCIMIENTO";
    public static final String COLUMN_SUBJECT = "SESSUJETO";
    public static final String COLUMN_IS_ACTIVE = "SESACTIVA";

    public SapiSession map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        SapiSession sapiSession = new SapiSession();
        sapiSession.setActive(r.getInt("SESACTIVA"));
        sapiSession.setApp(r.getString("SESAPLICACION"));
        sapiSession.setExpiration(r.getTimestamp("SESFEVENCIMIENTO"));
        sapiSession.setUuid(r.getString("SESUUID"));
        sapiSession.setIp(r.getString("SESIP"));
        sapiSession.setSubject(r.getString("SESSUJETO"));
        return sapiSession;
    }
}