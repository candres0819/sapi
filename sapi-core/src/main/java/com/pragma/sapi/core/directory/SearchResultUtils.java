package com.pragma.sapi.core.directory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntConsumer;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.exception.LdapSearchException;

public class SearchResultUtils {

    private static final Logger log = LoggerFactory.getLogger(SearchResultUtils.class);

    private SearchResultUtils() {
        super();
    }

    public static String getAttrForSearchResult(LdapContextFactory ldapContextFactory, String pathLdap, String filter)
            throws LdapSearchException {
        NamingEnumeration<SearchResult> results = null;
        LdapContext systemLdapContext = null;

        String attrs;
        try {
            log.trace(String.format("[getAttrForSearchResult] Buscando con filtro [%s] / path [%s]", filter, pathLdap));
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(2);

            systemLdapContext = ldapContextFactory.getSystemLdapContext();

            log.info("before escape " + pathLdap);
            log.info("before scapeSearch " + filter);

            results = systemLdapContext.search(pathLdap, filter, searchControls);
            if (null == results) {
                log.info("before result " + results);

                String escape = escapeDN(pathLdap);
                String scapeSearch = escapeLDAPSearchFilter(filter);

                log.info("escape " + escape);
                log.info("scapeSearch " + scapeSearch);

                results = systemLdapContext.search(escape, scapeSearch, searchControls);
                log.info("result " + results);

                if (null == results) {
                    throw new LdapSearchException("No se obtuvieron resultados de la busqueda ldap");
                }
            }
            attrs = getAtts(results);
        } catch (Exception e) {
            log.error("error en [getAttrForSearchResult]", e);
            throw new LdapSearchException(e.getMessage());
        } finally {
            finalizeConnections(systemLdapContext, results);
        }

        return attrs;
    }

    private static void finalizeConnections(LdapContext systemLdapContext, NamingEnumeration<SearchResult> results) {
        try {
            if (null != systemLdapContext) {
                systemLdapContext.close();
            }
        } catch (Exception var4) {
            log.error("Error cerrando contexto LDAP. ", var4);
        }

        try {
            if (null != results) {
                results.close();
            }
        } catch (Exception e) {
            log.error("Error al cerrar enumeracion. ", e);
        }
    }

    private static String getAtts(NamingEnumeration<SearchResult> results) {
        String attrs = null;
        while (results.hasMoreElements()) {
            SearchResult searchResult = (SearchResult) results.nextElement();
            if (null != searchResult) {
                attrs = searchResult.getName();
            } else {
                attrs = null;
            }
        }
        return attrs;
    }

    public static List<Attributes> getAttrs(LdapContextFactory ldapContextFactory, String pathLdap, String filter)
            throws LdapSearchException {
        NamingEnumeration<SearchResult> results = null;
        LdapContext systemLdapContext = null;

        try {
            log.trace(String.format("[getAttrs] Buscando con filtro [%s] / path [%s] (expresiones sin limpiar aun)", filter, pathLdap));
            SearchControls searchControls = getDefaultSearchControls();
            systemLdapContext = ldapContextFactory.getSystemLdapContext();

            log.info("before escape " + pathLdap);
            log.info("before scapeSearch " + filter);

            results = systemLdapContext.search(pathLdap, filter, searchControls);
            if (null == results) {
                log.info("before result " + results);
                String escape = escapeDN(pathLdap);
                String scapeSearch = escapeLDAPSearchFilter(filter);

                log.info("escape " + escape);
                log.info("scapeSearch " + scapeSearch);
                results = systemLdapContext.search(escape, scapeSearch, searchControls);
                log.info("result" + results);

                if (null == results) {
                    throw new LdapSearchException("No se obtuvieron resultados de la busqueda ldap");
                }
            }

            List<Attributes> attrs = new ArrayList<Attributes>();
            if (null != results && results.hasMoreElements()) {
                while (results.hasMoreElements()) {
                    SearchResult searchResult = (SearchResult) results.next();
                    attrs.add(searchResult.getAttributes());
                }
                return attrs;
            } else {
                throw new LdapSearchException("No se obtuvieron resultados de la busqueda ldap");
            }
        } catch (Exception e) {
            log.error("error en [getAttrs]", e);
            throw new LdapSearchException(e.getMessage());
        } finally {
            finalizeConnections(systemLdapContext, results);
        }
    }

    /**
     * 
     * @return
     */
    private static SearchControls getDefaultSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(2);
        searchControls.setReturningAttributes(new String[] { "memberOf", "o", "uid", "mail", "displayName", "givenName", "bmm_id_picture",
                "bmm_number_document", "bmm_phrase_security", "bmm_type_document" });
        return searchControls;
    }

    /**
     * 
     * @param name
     * @return
     */
    private static String escapeDN(String name) {
        final StringBuilder sb = new StringBuilder();
        if (StringUtils.isEmpty(name)) {
            return sb.toString();
        } else {
            if (name.charAt(0) == ' ' || name.charAt(0) == '#') {
                sb.append('\\');
            }

            IntConsumer consumer = value -> {
                switch ((char) value) {
                case '"':
                    sb.append("\\\"");
                    break;
                case '+':
                    sb.append("\\+");
                    break;
                case ',':
                    sb.append("\\,");
                    break;
                case ';':
                    sb.append("\\;");
                    break;
                case '<':
                    sb.append("\\<");
                    break;
                case '>':
                    sb.append("\\>");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                default:
                    sb.append((char) value);
                }
            };
            name.chars().forEach(consumer);
            if (name.length() > 1 && name.charAt(name.length() - 1) == ' ') {
                sb.insert(sb.length() - 1, '\\');
            }
            return sb.toString();
        }
    }

    /**
     * 
     * @param filter
     * @return
     */
    private static String escapeLDAPSearchFilter(String filter) {
        final StringBuilder sb = new StringBuilder();

        IntConsumer consumer = value -> {
            switch ((char) value) {
            case '\u0000':
                sb.append("\\");
                break;
            case '(':
                sb.append("\\(");
                break;
            case ')':
                sb.append("\\)");
                break;
            case '*':
                sb.append("\\*");
                break;
            case '\\':
                sb.append("\\\\");
                break;
            default:
                sb.append((char) value);
            }
        };
        filter.chars().forEach(consumer);
        return sb.toString();
    }
}
