package com.pragma.sapi.core.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.repository.impl.DataRepository;

public class SessionInactivationCommand implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(SessionInactivationCommand.class);

    private final DataRepository repo;
    private final String applicationName;

    public SessionInactivationCommand(DataRepository repo, String appName) {
        if (repo != null) {
            log.trace("Repositorio es {}", repo.getClass().getCanonicalName());
            this.repo = repo;
            this.applicationName = appName;
        } else {
            log.error("Repositorio no puede ser nulo");
            this.repo = null;
            this.applicationName = null;
        }
    }

    public void run() {
        log.trace("Ejecutando Cleanup de sesiones...");
        try {
            this.repo.purgeSessions(this.applicationName);
        } catch (Exception arg1) {
            log.error("Error en el proceso de Cleanup de Sesiones", arg1);
        }
    }

}