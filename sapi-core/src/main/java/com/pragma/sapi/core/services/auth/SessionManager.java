package com.pragma.sapi.core.services.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.core.SecuredApplication;
import com.pragma.sapi.core.datasource.session.SapiSession;
import com.pragma.sapi.core.exception.FirmaNoValidaException;
import com.pragma.sapi.core.exception.SapiConfigurationException;
import com.pragma.sapi.core.exception.SapiMaxNumOfSessionsException;
import com.pragma.sapi.core.exception.SapiSessionException;
import com.pragma.sapi.core.exception.SapiSessionValidationException;
import com.pragma.sapi.core.model.UserProperties;
import com.pragma.sapi.token.GeneratorToken.Builder;
import com.pragma.sapi.token.ObjectJWT;
import com.pragma.sapi.token.SapiSignatureAlgorithm;
import com.pragma.sapi.token.firm.Firm;
import com.pragma.sapi.token.util.TokenJWTUtils;

/**
 * 
 * @author carlos.cardona
 *
 */
public class SessionManager {

    private static final Logger log = LoggerFactory.getLogger(SessionManager.class);

    private SecuredApplication securedApplication;

    public SessionManager(SecuredApplication securedApplication) {
        this.securedApplication = securedApplication;
    }

    public SapiSession findSessionByUUID(String uuid) {
        if (null != this.securedApplication && null != this.securedApplication.getDataRepository()) {
            return this.securedApplication.getDataRepository().getSessionByUuid(uuid);
        } else {
            throw new SapiSessionException(
                    "El objeto \'SecuredApplication\' es nulo o el dataRepository es nulo.Verifique la configuracion.");
        }
    }

    private void registerSapiSession(SapiSession sapiSession) {
        if (null == sapiSession) {
            throw new SapiSessionException("El objeto \'SapiSession\' a registrar es nulo.");
        } else if (null != this.securedApplication && null != this.securedApplication.getDataRepository()) {
            this.securedApplication.getDataRepository().insertNewSession(sapiSession);
            log.debug("Sesion {} registrada.", sapiSession.getUuid());
        } else {
            throw new SapiSessionException(
                    "El objeto \'SecuredApplication\' es nulo o el dataRepository es nulo. Verifique la configuracion.");
        }
    }

    public void registerAndInactivateSession(ObjectJWT objectJWT) throws SapiSessionValidationException {
        if (null == objectJWT) {
            throw new SapiSessionValidationException(
                    "Se requieren un objeto JWT con la informacion del subject para marcar como inactiva la sesion.");
        } else {
            try {
                SapiSession e = this.findSessionByUUID(objectJWT.getUuid());
                if (e != null && e.getActive() == 0) {
                    log.warn("La sesion {} ya fue registrada y deshabilitada", objectJWT.getUuid());
                } else {
                    SapiSession sapiSession = new SapiSession();
                    sapiSession.setSubject(objectJWT.getSubject());
                    sapiSession.setUuid(objectJWT.getUuid());
                    sapiSession.setActive(0);
                    sapiSession.setApp(this.securedApplication.getName());
                    sapiSession.setExpiration(objectJWT.getExpiration());
                    sapiSession.setIp((String) objectJWT.getClaims().get("ipaddr"));

                    this.registerSapiSession(sapiSession);
                    log.debug("Sesion {} deshabilitada.", objectJWT.getUuid());
                }
            } catch (SapiSessionValidationException e) {
                log.error("error registrando sesion", e);
                throw e;
            } catch (Exception e) {
                log.error("error registrando sesion", e);
                throw new SapiSessionValidationException(e.getMessage());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public String refreshSession(ObjectJWT objectJWT, Firm<?> firm) throws SapiSessionValidationException, FirmaNoValidaException {
        if (null == objectJWT) {
            throw new SapiSessionValidationException("Se requieren un objeto JWT con la informacion del subject para refrescar la sesion.");
        } else if (null == objectJWT.getSubject()) {
            throw new SapiSessionValidationException("Se requiere un Subject para refrescar la sesion.");
        } else if (null != objectJWT.getClaims() && !objectJWT.getClaims().isEmpty()) {
            if (null == objectJWT.getClaims().get("identification")) {
                throw new SapiSessionValidationException(
                        "Se requiere la identificacion (cedula o nit) del usuario autenticado para refrescar la sesion");
            } else if (null == objectJWT.getClaims().get("canonicalNameAuth")) {
                throw new SapiSessionValidationException("Se requiere el nombre del realm para refrescar la sesion.");
            } else if (null == objectJWT.getClaims().get("roles")) {
                throw new SapiSessionValidationException("Se requiere la lista de roles para firmar en el token.");
            } else if (null == objectJWT.getClaims().get("ipaddr")) {
                throw new SapiSessionValidationException("Se requiere la IP relacionada para refrescar la sesion.");
            } else if (null != firm && null != firm.getSigner() && null != firm.getSigner().getKey()) {
                return this.createNewSession(objectJWT.getSubject(), (String) objectJWT.getClaims().get("canonicalNameAuth"),
                        (List<String>) objectJWT.getClaims().get("roles"), (String) objectJWT.getClaims().get("identification"),
                        (String) objectJWT.getClaims().get("ipaddr"), firm, null);
            } else {
                throw new FirmaNoValidaException("La firma para generar JWTs no puede ser nula.");
            }
        } else {
            throw new SapiSessionValidationException("Se requiere una coleccion de CLAIMS para refrescar la sesion.");
        }
    }

    public String createNewSession(Object principal, UserProperties userProperties, Firm<?> firm)
            throws SapiSessionValidationException, FirmaNoValidaException {
        if (null == userProperties) {
            throw new SapiSessionValidationException("Se requieren las propiedades del usuario para registrar la sesion.");
        } else {
            return this.createNewSession(principal, userProperties.getRealm(), userProperties.getRoles(),
                    userProperties.getIdentification(), userProperties.getIpAddress(), firm, null);
        }
    }

    public String createNewSession(Object principal, UserProperties userProperties, Firm<?> firm, int numeroMaximoSessiones)
            throws SapiSessionValidationException, FirmaNoValidaException {
        if (null == userProperties) {
            throw new SapiSessionValidationException("Se requieren las propiedades del usuario para registrar la sesion.");
        } else {
            return this.createNewSession(principal, userProperties.getRealm(), userProperties.getRoles(),
                    userProperties.getIdentification(), userProperties.getIpAddress(), firm, numeroMaximoSessiones);
        }
    }

    private String createNewSession(Object principal, String realm, List<String> roles, String identification, String ipAddress,
            Firm<?> firm, Integer numMaxSesiones) throws SapiSessionValidationException, FirmaNoValidaException {
        if (null == principal) {
            throw new SapiSessionValidationException("Se requiere un Principal para registrar la sesion.");
        } else if (null != realm && !"".equalsIgnoreCase(realm)) {
            if (null == identification) {
                throw new SapiSessionValidationException("Se requiere la identificacion (cedula o nit) del usuario autenticado.");
            } else if (null != roles && !roles.isEmpty()) {
                if (null != firm && null != firm.getSigner() && null != firm.getSigner().getKey()) {
                    ObjectJWT jwtObject = new ObjectJWT((String) principal, this.securedApplication.getName());
                    jwtObject.setAudience(this.securedApplication.getName());
                    jwtObject.setAlgorithm(SapiSignatureAlgorithm.RS256);

                    Map<String, Object> claims = new HashMap<String, Object>();
                    claims.put("ipaddr", ipAddress);
                    claims.put("canonicalNameAuth", realm);
                    claims.put("roles", roles);
                    claims.put("identification", identification);

                    jwtObject.setClaims(claims);
                    String brandNewToken = (new Builder(jwtObject, firm)).buildJWT();
                    jwtObject = TokenJWTUtils.parseObjectJWT(brandNewToken);
                    log.debug("Nuevo token con id {} generado.", jwtObject.getUuid());

                    SapiSession sapiSession = new SapiSession();
                    sapiSession.setSubject(jwtObject.getSubject());
                    sapiSession.setUuid(jwtObject.getUuid());
                    sapiSession.setActive(1);
                    sapiSession.setApp(this.securedApplication.getName());
                    sapiSession.setExpiration(jwtObject.getExpiration());
                    sapiSession.setIp((String) jwtObject.getClaims().get("ipaddr"));

                    if (null != numMaxSesiones) {
                        log.debug("Se valida antes de guardar la sesion cumpla el limite {}.", numMaxSesiones);
                        checkSessionLimitPolicy(jwtObject.getSubject(), numMaxSesiones);
                    }

                    this.registerSapiSession(sapiSession);
                    return brandNewToken;
                } else {
                    throw new FirmaNoValidaException("La firma para generar JWTs no puede ser nula.");
                }
            } else {
                throw new SapiSessionValidationException("Se requiere la lista de roles para firmar en el token.");
            }
        } else {
            throw new SapiSessionValidationException("El realm no puede ser nulo.");
        }
    }

    public boolean checkSessionLimitPolicy(ObjectJWT objectJWT, int numMaxSesiones) {
        return this.checkSessionLimitPolicy(objectJWT.getSubject(), numMaxSesiones);
    }

    public boolean checkSessionLimitPolicy(String subject, int numMaxSesiones) {
        if (null != this.securedApplication && null != this.securedApplication.getDataRepository()) {
            if (null == subject) {
                throw new SapiSessionException("El argumento \'subject\' no puede ser nulo");
            } else {
                log.debug("Maximo numero de sesiones configurado: {}", Integer.valueOf(numMaxSesiones));
                this.inactivarSesionesYaVencidas(subject);
                this.validationNumSessions(subject, numMaxSesiones);
                return true;
            }
        } else {
            throw new SapiConfigurationException(
                    "El objeto \'SecuredApplication\' es nulo o el dataRepository es nulo.Verifique la configuracion.");
        }
    }

    private void inactivarSesionesYaVencidas(String subject) {
        log.trace("Inactivando sesiones vencidas de {} en {}", subject, this.securedApplication.getName());
        this.securedApplication.getDataRepository().inactivateExpiredSessionsByAppAndUser(this.securedApplication.getName(), subject);
    }

    private void validationNumSessions(String subject, int maxNumSesiones) {
        List<?> sessions = this.securedApplication.getDataRepository().getActiveSessions(this.securedApplication.getName(), subject);
        if (maxNumSesiones > 0) {
            log.trace("Usuario tiene {} sesiones activas en {}", Integer.valueOf(sessions.size()), this.securedApplication.getName());
            if (sessions.size() >= maxNumSesiones) {
                throw new SapiMaxNumOfSessionsException(
                        String.format("No puede tener mas de %d sesiones activas", Integer.valueOf(maxNumSesiones)));
            }
        } else {
            log.trace("No existe limite de sesiones para el realm que valida el usuario {}", subject);
        }
    }
}