package com.pragma.sapi.core.exception;

public class SapiSessionException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SapiSessionException(String message) {
        super(message);
    }

    public SapiSessionException(String message, Throwable cause) {
        super(message, cause);
    }
}