package com.pragma.sapi.core.repository.impl;

import com.pragma.sapi.core.repository.SapiResourceRepo;
import com.pragma.sapi.core.repository.SapiRestrictionRepo;
import com.pragma.sapi.core.repository.SapiSessionRepo;

public interface DataRepository extends SapiRestrictionRepo, SapiResourceRepo, SapiSessionRepo {

}