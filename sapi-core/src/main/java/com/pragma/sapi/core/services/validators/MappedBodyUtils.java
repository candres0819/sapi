package com.pragma.sapi.core.services.validators;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class MappedBodyUtils {

    private static final Logger log = LoggerFactory.getLogger(MappedBodyUtils.class);

    public static String mapOfExpression(String expression, String body) throws ClassCastException, JsonSyntaxException {
        JsonElement data = (JsonElement) (new Gson()).fromJson(body, JsonElement.class);
        String[] level = expression.trim().replace(".", ",").split(",");
        String id = null;
        JsonObject[] base = new JsonObject[] { data.getAsJsonObject() };

        for (int i = 0; i < level.length; ++i) {
            if (base[0].isJsonObject() && i != level.length - 1) {
                Optional.ofNullable(base[0].getAsJsonObject(level[i])).ifPresent((b) -> base[0] = b);
            }
            id = level[i];
        }

        if (base[0].get(id) == null) {
            throw new ClassCastException("No se pudo establecer el formato de la expresion ");
        } else {
            return base[0].get(id).getAsString();
        }
    }

    public static String getClientIdOfMappedValue(Object mappedValue, String body) {
        if (mappedValue != null && String[].class.isAssignableFrom(mappedValue.getClass())) {
            String[] mapArray = (String[]) ((String[]) mappedValue);
            String[] exp = mapArray[0].trim().replace("+", ",").split(",");

            String type;
            String clientId;
            try {
                type = mapOfExpression(exp[0], body);
                clientId = mapOfExpression(exp[1], body);
            } catch (JsonSyntaxException | ClassCastException e) {
                log.error("Error en el mapeo de los datos para obtener el client", e);
                return null;
            }

            return toFormatClientId(type, clientId);
        } else {
            return null;
        }
    }

    public static String toFormatClientId(String type, String id) {
        return type.toUpperCase().concat("-").concat(id);
    }
}