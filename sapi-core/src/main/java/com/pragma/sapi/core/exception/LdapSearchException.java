package com.pragma.sapi.core.exception;

public class LdapSearchException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public LdapSearchException(String message) {
        super(message);
    }

}