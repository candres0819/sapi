package com.pragma.sapi.core.datasource.restriction;

public class SapiRestriction {

    private String app;
    private String url;
    private String method;
    private String roles;
    private String command;
    private String strategy;
    private int order;

    public SapiRestriction() {
        super();
    }

    public SapiRestriction(String app, String url, String method, String roles) {
        this(app, url, method, roles, 0);
    }

    public SapiRestriction(String app, String url, String method, String roles, int order) {
        this.app = app;
        this.url = url;
        this.method = method;
        this.roles = roles;
        this.order = order;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRoles() {
        return this.roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getApp() {
        return this.app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getStrategy() {
        return this.strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String toString() {
        return String.format("[url=%s, method=%s, order=%s, roles=%s, command=%s, estrategia=%s]", this.getUrl(), this.getMethod(),
                Integer.valueOf(this.getOrder()), this.getRoles(), this.getCommand(), this.getStrategy());
    }
}