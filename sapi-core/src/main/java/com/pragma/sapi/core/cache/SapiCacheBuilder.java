package com.pragma.sapi.core.cache;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

public class SapiCacheBuilder {

    private static final Logger log = LoggerFactory.getLogger(SapiCacheBuilder.class);

    private static Map<String, SapiCache> caches = new HashMap<String, SapiCache>();

    public enum CacheImpl {
        REDIS, GOOGLE;

        CacheImpl() {

        }
    }

    public static SapiCache build(String cacheName, SapiCacheLoading loadingFn) {
        log.info("SapiCacheBuilder build");
        // CacheImpl cImpl = CacheImpl.REDIS;
        // if (!checkRedis()) {
        // cImpl = CacheImpl.GOOGLE;
        // }
        return build(CacheImpl.GOOGLE, cacheName, 300, loadingFn);
    }

    public static SapiCache build(CacheImpl cacheImpl, String cacheName, int timeout, SapiCacheLoading loadingFn) {
        SapiCache c = get(cacheName);
        if (c == null) {
            c = new SapiCacheGoogleImpl(loadingFn, cacheName, timeout);
            caches.put(cacheName, c);
        }
        return c;
    }

    public static SapiCache get(String cacheName) {
        if (caches.containsKey(cacheName)) {
            return caches.get(cacheName);
        }
        return null;
    }

    @SuppressWarnings("unused")
    private static boolean checkRedis() {
        Jedis jedis = null;
        try {
            log.info("Validando presencia de REDIS...");
            jedis = new Jedis();
            String pong = jedis.ping();
            log.info("REDIS presente. {}", pong);
            return true;
        } catch (Exception e) {
            log.warn("REDIS no presente o no activo: {}", e.getMessage());
            log.info("Cache sera soportado con LoadingCache de Google.");
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        return false;
    }
}
