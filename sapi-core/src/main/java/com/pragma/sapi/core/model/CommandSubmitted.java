package com.pragma.sapi.core.model;

import java.util.Objects;

import com.pragma.sapi.core.log.command.Command;

public class CommandSubmitted {

    private Command<?> comando;

    public CommandSubmitted() {
        super();
    }

    public CommandSubmitted(Command<?> comando) {
        this.comando = comando;
    }

    public Command<?> getComando() {
        return this.comando;
    }

    public void setComando(Command<?> comando) {
        this.comando = comando;
    }

    public int hashCode() {
        return Objects.hash(new Object[] { this.comando });
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof CommandSubmitted)) {
            return false;
        } else {
            CommandSubmitted ref = (CommandSubmitted) o;
            return Objects.equals(this.comando, ref.comando);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(String.format("comando: %s", new Object[] { this.comando })).append("}");
        return sb.toString();
    }
}