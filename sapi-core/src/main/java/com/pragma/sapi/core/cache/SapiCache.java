package com.pragma.sapi.core.cache;

public interface SapiCache {

    String get(String arg0);

    boolean contains(String arg0);

    void invalidate(String arg0);

    String getCacheName();

    void setCacheName(String arg0);

}