package com.pragma.sapi.token;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.pragma.sapi.token.firm.Firm;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

public final class GeneratorToken {

    public final String subject;
    public final Date issuedAt;
    public final Date expiration;
    public final String issuer;
    public final SapiSignatureAlgorithm algorithm;
    public final Map<String, Object> claims;

    private GeneratorToken(Builder builder) {
        this.subject = builder.subject;
        this.claims = builder.claims;
        this.issuedAt = builder.issuedAt;
        this.issuer = builder.issuer;
        this.algorithm = builder.algorithm;
        this.expiration = builder.expiration;
    }

    public static class Builder {

        private final Firm<?> firm;
        private final String subject;
        private final Date issuedAt;
        private final Date expiration;
        private final String issuer;
        private final String audience;
        private final String uuid;
        private final SapiSignatureAlgorithm algorithm;
        private final Map<String, Object> claims;

        public Builder(ObjectJWT objectJWT, Firm<?> firm) {
            this.firm = firm;
            this.subject = objectJWT.getSubject();

            this.issuer = objectJWT.getIssuer();
            this.audience = objectJWT.getAudience();
            this.expiration = objectJWT.getExpiration();
            this.uuid = objectJWT.getUuid();

            this.issuedAt = objectJWT.getIssuedAt();
            this.algorithm = objectJWT.getAlgorithm();
            this.claims = objectJWT.getClaims();

            this.claims.put("sessionUUID", this.uuid);

            if (this.issuedAt.after(this.expiration)) {
                throw new GeneratorToken.InvalidityDateExpiration("Fecha expiration menor que fecha issuedAt");
            }
        }

        public String buildJWT() {
            GeneratorToken gen = new GeneratorToken(this);
            JwtBuilder jwts = sign(Jwts.builder(), this.firm.getSigner().getClass());

            Map<String, Object> data = new HashMap<String, Object>();
            data.put("data", this.claims);

            return jwts.setClaims(data).setSubject(TextCodec.BASE64.encode(gen.subject)).setExpiration(gen.expiration)
                    .setIssuedAt(gen.issuedAt).setAudience(this.audience).setIssuer(gen.issuer).compact();
        }

        private JwtBuilder sign(JwtBuilder builder, Class<?> firmClass) {
            SignatureAlgorithm _algorithm = this.algorithm.parse();
            if (Firm.SecretSigner.class.isAssignableFrom(firmClass)) {
                byte[] keyBytes = ((String[]) (String[]) this.firm.getSigner().getKey())[0].getBytes();
                builder.signWith(_algorithm, keyBytes);
            }
            if (Firm.Base64EncodedSecretKeySigner.class.isAssignableFrom(firmClass)) {
                String key = ((String[]) (String[]) this.firm.getSigner().getKey())[0];
                builder.signWith(_algorithm, key);
            }
            if (Firm.KeySigner.class.isAssignableFrom(firmClass)) {
                Key[] keys = (Key[]) this.firm.getSigner().getKey();
                builder.signWith(_algorithm, keys[0]);
            }
            return builder;
        }
    }

    public static class InvalidityDateExpiration extends RuntimeException {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public InvalidityDateExpiration(String message) {
            super();
        }
    }
}
