package com.pragma.sapi.token.util;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pragma.sapi.token.ObjectJWT;

import com.pragma.sapi.token.SapiSignatureAlgorithm;
import com.pragma.sapi.token.ObjectJWT.Realm;
import io.jsonwebtoken.impl.TextCodec;

public abstract class TokenJWTUtils {

    public static final String SAPI_KEY_DATA = "data";
    public static final String SAPI_KEY_DATA_SESSION_UUID = "sessionUUID";
    public static final String AZURE_KEY_DATA_SESSION_UUID = "nonce";
    public static final String SAPI_KEY_CANONICAL_NAME_AUTH = "canonicalNameAuth";
    public static final String SAPI_KEY_ROLES = "roles";
    public static final String SAPI_KEY_IDENTIFICATION = "identification";
    public static final String SAPI_KEY_EMPL_SECURITY_LEVEL = "nvlseg";
    public static final String SAPI_KEY_IPADDRESS = "ipaddr";

    private TokenJWTUtils() {
        super();
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> stringToMap(String json) {
        Gson gson = new Gson();
        Type type = (new TypeToken<Map<String, Object>>() {
        }).getType();
        return (Map<String, Object>) gson.fromJson(json, type);
    }

    public static String getBase64Encodedkey(String secret) {
        String[] base = new String[] { null };
        Optional.ofNullable(secret).ifPresent((s) -> {
            byte[] key = secret.getBytes();
            base[0] = TextCodec.BASE64URL.encode(key);
        });
        return base[0];
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> parseBody(String token) {
        String json = getBase64Decodedkey(token.split("\\.")[1]);
        Map<String, Object> dataInfo = stringToMap(json);
        return (Map<String, Object>) dataInfo.getOrDefault("data", new HashMap<String, Object>());
    }

    public static ObjectJWT parseObjectJWT(String token) {
        String json_header = getBase64Decodedkey(token.split("\\.")[0]);
        String json_body = getBase64Decodedkey(token.split("\\.")[1]);
        Map<String, Object> dataInfo = stringToMap(json_body);
        Realm realm = Realm.Empleados;
        Map<String, Object> body = null;
        String sub;
        String uuid;
        if (dataInfo.get("nonce") != null && dataInfo.get("upn") != null) {
            sub = (String) dataInfo.get("upn");
            uuid = (String) dataInfo.get("nonce");
        } else {
            sub = getBase64Decodedkey((String) dataInfo.get("sub"));
            body = parseBody(token);
            uuid = (String) body.get("sessionUUID");
            realm = Realm.Sapi;
        }

        Double exp = (Double) dataInfo.get("exp");
        Double iat = (Double) dataInfo.getOrDefault("iat", 0);
        long unixSecondsExp = exp.longValue();
        Date dateExp = new Date(unixSecondsExp * 1000L);
        ObjectJWT objectJWT = new ObjectJWT(sub, (String) dataInfo.getOrDefault("iss", ""), dateExp, uuid);
        if (iat.doubleValue() > 0.0D) {
            long unixSecondsIat = exp.longValue();
            Date dateIat = new Date(unixSecondsIat * 1000L);
            objectJWT.setIssuedAt(dateIat);
        }

        objectJWT.setRealm(realm);
        objectJWT.setAudience(dataInfo.getOrDefault("aud", "").toString());
        objectJWT.setClaims(body);
        Map<String, Object> headInfo = stringToMap(json_header);
        String alg = (String) headInfo.get("alg");
        objectJWT.setAlgorithm(parseAlgorithm(alg));
        return objectJWT;
    }

    public static String getBase64Decodedkey(String secret) {
        String[] base = new String[1];
        Optional.ofNullable(secret).ifPresent((s) -> {
            base[0] = TextCodec.BASE64URL.decodeToString(secret);
        });
        return base[0];
    }

    public static SapiSignatureAlgorithm parseAlgorithm(String algName) {
        SapiSignatureAlgorithm returnValue = null;
        if ("RS256".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.RS256;
        }

        if ("RS384".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.RS384;
        }

        if ("RS512".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.RS512;
        }

        if ("HS256".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.HS256;
        }

        if ("HS384".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.HS384;
        }

        if ("HS512".equalsIgnoreCase(algName)) {
            returnValue = SapiSignatureAlgorithm.HS512;
        }

        return returnValue;
    }
}
