package com.pragma.sapi.token.firm;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.token.exception.FirmaNulaException;
import com.pragma.sapi.token.firm.Firm.Signer;
import com.pragma.sapi.token.util.RSAUtils;
import com.pragma.sapi.token.util.TokenJWTUtils;

public abstract class Firm<T extends Signer> {

    public abstract T getSigner();

    public interface Signer {
        Object[] getKey();
    }

    public static final class KeySigner implements Firm.Signer {

        private Logger log = LoggerFactory.getLogger(KeySigner.class);

        private final String path;
        private final Type type;
        private String passwd;
        private String certAlias;
        private String certPasswd;

        public KeySigner(String path, Type type) {
            this.path = path;
            this.type = type;
        }

        public KeySigner(String path, Type type, String passwd, String certAlias, String certPasswd) {
            this(path, type);
            this.passwd = passwd;
            this.certAlias = certAlias;
            this.certPasswd = certPasswd;
        }

        public KeySigner(String path, Type type, String passwd, String certAlias) {
            this(path, type);
            this.passwd = passwd;
            this.certAlias = certAlias;
        }

        public Key[] getKey() {
            List<Key> keys = new ArrayList<Key>();
            try {
                switch (com.pragma.sapi.token.firm.Firm.KeySigner.Type.values()[this.type.ordinal()]) {
                case PRIVATE_JKS:
                    keys.add(RSAUtils.getPrivateKeyFromKS(this.path, this.passwd, this.certAlias, this.certPasswd));
                    break;
                case PUBLIC_JKS:
                    keys.add(RSAUtils.getPublicKeyFromKS(this.path, this.passwd, this.certAlias));
                    break;
                case PRIVATE:
                    keys.add(RSAUtils.getPrivateKey(this.path));
                    break;
                default:
                    keys.add(RSAUtils.getPublicKey(this.path));
                }

                if ((null == keys ) || (keys.isEmpty())) {
                    throw new FirmaNulaException("No se encontro la firma adecuada");
                }
            } catch (FirmaNulaException fne) {
                this.log.error("Error", fne);
            }
            Key[] retorno = new Key[keys.size()];
            int i = 0;
            for (Key k : keys) {
                retorno[(i++)] = k;
            }
            return retorno;
        }

        public enum Type {

            PUBLIC, PRIVATE, PUBLIC_JKS, PRIVATE_JKS;

            Type() {

            }
        }
    }

    public final class SecretSigner implements Firm.Signer {

        private final String secret;

        public SecretSigner(String secret) {
            this.secret = secret;
        }

        public String[] getKey() {
            return new String[] { this.secret };
        }
    }

    public final class Base64EncodedSecretKeySigner implements Firm.Signer {

        private final String secret;

        public Base64EncodedSecretKeySigner(String secret) {
            this.secret = TokenJWTUtils.getBase64Encodedkey(secret);
        }

        public String[] getKey() {
            return new String[] { this.secret };
        }
    }
}
