package com.pragma.sapi.token;

import io.jsonwebtoken.SignatureAlgorithm;

public enum SapiSignatureAlgorithm {

    NONE("none", false),
    HS256("HS256", false),
    HS384("HS384", false),
    HS512("HS512", false),
    RS256("RS256", true),
    RS384("RS384", true),
    RS512("RS512", true);

    private final String value;
    private final boolean isRsa;

    SapiSignatureAlgorithm(String value, boolean isRsa) {
        this.value = value;
        this.isRsa = isRsa;
    }

    public boolean isRsa() {
        return this.isRsa;
    }

    public SignatureAlgorithm parse() {
        String arg1 = this.value;
        byte arg2 = -1;
        switch (arg1.hashCode()) {
            case 3387192:
                if (arg1.equals("none")) {
                    arg2 = 0;
                }
                break;
            case 69015912:
                if (arg1.equals("HS256")) {
                    arg2 = 1;
                }
                break;
            case 69016964:
                if (arg1.equals("HS384")) {
                    arg2 = 2;
                }
                break;
            case 69018667:
                if (arg1.equals("HS512")) {
                    arg2 = 3;
                }
                break;
            case 78251122:
                if (arg1.equals("RS256")) {
                    arg2 = 4;
                }
                break;
            case 78252174:
                if (arg1.equals("RS384")) {
                    arg2 = 5;
                }
                break;
            case 78253877:
                if (arg1.equals("RS512")) {
                    arg2 = 6;
                }
        }

        SignatureAlgorithm signatureAlgorithm;
        switch (arg2) {
            case 0:
                signatureAlgorithm = SignatureAlgorithm.NONE;
                break;
            case 1:
                signatureAlgorithm = SignatureAlgorithm.HS256;
                break;
            case 2:
                signatureAlgorithm = SignatureAlgorithm.HS384;
                break;
            case 3:
                signatureAlgorithm = SignatureAlgorithm.HS512;
                break;
            case 4:
                signatureAlgorithm = SignatureAlgorithm.RS256;
                break;
            case 5:
                signatureAlgorithm = SignatureAlgorithm.RS384;
                break;
            case 6:
                signatureAlgorithm = SignatureAlgorithm.RS512;
                break;
            default:
                signatureAlgorithm = SignatureAlgorithm.RS256;
        }

        return signatureAlgorithm;
    }
}