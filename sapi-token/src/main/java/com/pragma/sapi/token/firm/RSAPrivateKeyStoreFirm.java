package com.pragma.sapi.token.firm;

public class RSAPrivateKeyStoreFirm extends Firm<Firm.KeySigner> {

    private String path;
    private String passwd;
    private String certAlias;
    private String certPasswd;

    public RSAPrivateKeyStoreFirm() {
        super();
    }

    public RSAPrivateKeyStoreFirm(String path) {
        this.path = path;
    }

    public Firm.KeySigner getSigner() {
        return new Firm.KeySigner(this.path, Firm.KeySigner.Type.PRIVATE_JKS, this.passwd, this.certAlias, this.certPasswd);
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPasswd() {
        return this.passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getCertAlias() {
        return this.certAlias;
    }

    public void setCertAlias(String certAlias) {
        this.certAlias = certAlias;
    }

    public String getCertPasswd() {
        return this.certPasswd;
    }

    public void setCertPasswd(String certPasswd) {
        this.certPasswd = certPasswd;
    }
}
