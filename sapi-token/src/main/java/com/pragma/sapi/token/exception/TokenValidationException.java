package com.pragma.sapi.token.exception;

public class TokenValidationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TokenValidationException(String message) {
        super(message);
    }

    public TokenValidationException(String message, Exception e) {
        super(message, e);
    }
}