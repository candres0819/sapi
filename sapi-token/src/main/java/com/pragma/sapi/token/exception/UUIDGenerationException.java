package com.pragma.sapi.token.exception;

public class UUIDGenerationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UUIDGenerationException(String message) {
        super(message);
    }

    public UUIDGenerationException(String message, Exception e) {
        super(message, e);
    }
}