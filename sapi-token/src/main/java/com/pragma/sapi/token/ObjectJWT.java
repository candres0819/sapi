package com.pragma.sapi.token;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;

import com.pragma.sapi.token.util.CryptoUtil;

public final class ObjectJWT {

    private final String subject;
    private final String issuer;
    private final String uuid;
    private Date expiration;
    private Realm realm;
    private Date issuedAt;
    private String audience;
    private SapiSignatureAlgorithm algorithm;
    private Map<String, Object> claims;

    public ObjectJWT(String subject, String issuer, Date expiration) {
        this(subject, issuer, expiration, UUIDGen.getTimeUUID().toString());
    }

    public ObjectJWT(String subject, String issuer) {
        this(subject, issuer, new Date(System.currentTimeMillis() + 3600000L), UUIDGen.getTimeUUID().toString());
    }

    public ObjectJWT(String subject, String issuer, Date issuedAt, Date expiration, String uuid) {
        this.subject = subject;
        this.expiration = expiration;
        this.algorithm = SapiSignatureAlgorithm.RS512;
        this.issuedAt = issuedAt;
        this.issuer = issuer;
        this.uuid = StringUtils.isBlank(uuid) ? UUIDGen.getTimeUUID().toString() : uuid;
        this.claims = new HashMap<String, Object>();
    }

    public ObjectJWT(String subject, String issuer, Date expiration, String uuid) {
        this.subject = subject;
        this.expiration = expiration;
        this.algorithm = SapiSignatureAlgorithm.HS256;
        this.issuedAt = new Date();
        this.issuer = issuer;
        this.uuid = StringUtils.isBlank(uuid) ? UUIDGen.getTimeUUID().toString() : uuid;
        this.claims = new HashMap<String, Object>();
    }

    public ObjectJWT(String subject, Date expiration) {
        this(subject, "anonymous", expiration);
    }

    public String getSubject() {
        return this.subject;
    }

    public Date getExpiration() {
        return this.expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Date getIssuedAt() {
        return this.issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    public String getIssuer() {
        return this.issuer;
    }

    public SapiSignatureAlgorithm getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(SapiSignatureAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Map<String, Object> getClaims() {
        return this.claims;
    }

    public void setClaims(Map<String, Object> claims) {
        this.claims = claims;
    }

    public void addEncryptedClaim(String claimName, String claimValue) {
        this.claims.put(claimName, CryptoUtil.encryptData(claimValue));
    }

    public String getDecryptedClaim(String claimName) {
        return CryptoUtil.decryptData((String) this.claims.get(claimName));
    }

    public String getAudience() {
        return this.audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getUuid() {
        return this.uuid;
    }

    public Realm getRealm() {
        return this.realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    private long getEpochTime(Date date) {
        long epoch = 0L;
        if (null != date) {
            epoch = date.getTime();
            epoch = 1000L * (epoch / 1000L);
        }

        return epoch;
    }

    public int hashCode() {
        byte result = 1;
        int result1 = 31 * result + (this.uuid == null ? 1 : this.uuid.hashCode());
        result1 = 31 * result1 + (this.subject == null ? 2 : this.subject.hashCode());
        result1 = 31 * result1 + (this.issuer == null ? 3 : this.issuer.hashCode());
        return result1;
    }

    public boolean equals(Object obj) {
        if ((null == obj) || (!(obj instanceof ObjectJWT))) {
            return false;
        } else {
            ObjectJWT reference = (ObjectJWT) obj;
            return (new EqualsBuilder()).append(this.getAudience(), reference.getAudience()).append(this.getIssuer(), reference.getIssuer())
                    .append(this.getSubject(), reference.getSubject())
                    .append(this.getEpochTime(this.getIssuedAt()), this.getEpochTime(reference.getIssuedAt()))
                    .append(this.getEpochTime(this.getExpiration()), this.getEpochTime(reference.getExpiration())).isEquals();
        }
    }

    public enum Realm {

        Empleados, Clientes, Sapi;

        Realm() {

        }
    }
}