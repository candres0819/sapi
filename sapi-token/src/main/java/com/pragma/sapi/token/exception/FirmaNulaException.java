package com.pragma.sapi.token.exception;

public class FirmaNulaException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public FirmaNulaException(String mensaje) {
        super(mensaje);
    }

    public FirmaNulaException(String message, Exception e) {
        super(message, e);
    }
}
