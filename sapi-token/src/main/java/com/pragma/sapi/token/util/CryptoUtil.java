package com.pragma.sapi.token.util;

import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptoUtil {

    private static final Logger log = LoggerFactory.getLogger(CryptoUtil.class);

    private static AESBouncyCastle abc;

    private static byte[] enc(String plainText) {
        byte[] encryptedText;
        try {
            byte[] e = plainText.getBytes();
            encryptedText = abc.encrypt(e);
        } catch (Exception e) {
            log.error("Error encriptando", e);
            encryptedText = null;
        }
        return encryptedText;
    }

    public static String encryptData(String plainText) {
        return Base64.getEncoder().encodeToString(enc(plainText));
    }

    private static byte[] dec(byte[] encryptedText) {
        byte[] plainText;
        try {
            plainText = abc.decrypt(encryptedText);
        } catch (Exception e) {
            log.error("Error encriptando", e);
            plainText = encryptedText;
        }

        return plainText;
    }

    public static String decryptData(String encryptedText) {
        return (new String(dec(Base64.getDecoder().decode(encryptedText)))).replace(" ", "");
    }

    static {
        try {
            KeyGenerator e = KeyGenerator.getInstance("AES");
            e.init(256);
            SecretKey sk = e.generateKey();
            abc = new AESBouncyCastle();
            abc.setPadding(new PKCS7Padding());
            abc.setKey(sk.getEncoded());
        } catch (Exception e) {
            log.error("Error inicializando la infraestructura para encripcion de claims", e);
        }
    }
}