package com.pragma.sapi.token;

import java.security.Key;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.token.exception.TokenValidationException;
import com.pragma.sapi.token.firm.Firm;
import com.pragma.sapi.token.util.TokenJWTUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;

public final class ValidatorToken {

    private static Logger log = LoggerFactory.getLogger(ValidatorToken.class);

    private final Claims body;
    private final String token;

    private ValidatorToken(Claims body, String token) {
        this.body = body;
        this.token = token;
    }

    private ObjectJWT getSapiObjectJWT() {
        String subject = TextCodec.BASE64.decodeToString(this.body.getSubject());
        Map<String, Object> data = TokenJWTUtils.parseBody(this.token);
        String uuid = data.get("sessionUUID").toString();
        ObjectJWT objectJWT = new ObjectJWT(subject, this.body.getIssuer(), this.body.getIssuedAt(), this.body.getExpiration(), uuid);
        objectJWT.setAudience(this.body.getAudience());
        objectJWT.setClaims(data);
        String json_header = TokenJWTUtils.getBase64Decodedkey(this.token.split("\\.")[0]);
        Map<?, ?> headInfo = TokenJWTUtils.stringToMap(json_header);
        String alg = (String) headInfo.get("alg");
        objectJWT.setAlgorithm(TokenJWTUtils.parseAlgorithm(alg));
        return objectJWT;
    }

    private ObjectJWT getAzureObjectJWT() {
        if (this.body.get("oid") == null) {
            return null;
        } else {
            String subject = (String) this.body.get("upn");
            String uuid = (String) this.body.get("oid");
            ObjectJWT objectJWT = new ObjectJWT(subject, this.body.getIssuer(), this.body.getIssuedAt(), this.body.getExpiration(), uuid);
            objectJWT.setAudience(this.body.getAudience());
            if (this.body.get("groups") != null) {
                List<?> grupos = (List<?>) this.body.get("groups");
                objectJWT.getClaims().put("groups", grupos);
            } else {
                log.warn("No se encontro Claim de Grupos en el Token de Azure");
            }

            objectJWT.getClaims().put("name", (String) this.body.get("name"));
            objectJWT.getClaims().put("email", (String) this.body.get("upn"));
            objectJWT.getClaims().put("ipaddr", (String) this.body.get("ipaddr"));
            return objectJWT;
        }
    }

    public boolean isEqualTo(ObjectJWT reference) {
        ObjectJWT obj = this.getAzureObjectJWT();
        return obj != null ? obj.equals(reference) : this.getSapiObjectJWT().equals(reference);
    }

    public static class Valid {

        private final String token;
        private Firm<?> firm;
        private Jws<Claims> body;
        private ValidatorToken validator;

        public Valid(Firm<?> firm, String token) {
            this.firm = firm;
            this.token = token;
            validateSapiToken();
        }

        private void validateSapiToken() {
            JwtParser parser = Jwts.parser();
            try {
                this.body = signingKey(parser, this.firm.getSigner().getClass()).parseClaimsJws(this.token);
                build();
            } catch (Exception e) {
                ValidatorToken.log.error("Error durante la validacion", e);
                throw new TokenValidationException("Error durante la validacion", e);
            }
        }

        private void build() {
            this.validator = new ValidatorToken(this.body.getBody(), this.token);
        }

        public ObjectJWT getObjectJWT() {
            return this.validator.getSapiObjectJWT();
        }

        public ValidatorToken getValidator() {
            return this.validator;
        }

        private JwtParser signingKey(JwtParser parser, Class<?> firmClass) throws TokenValidationException {
            if ((null == this.firm) || (null == this.firm.getSigner()) || (null == this.firm.getSigner().getKey())) {
                ValidatorToken.log.warn("La firma no es valida o no existe");
                throw new TokenValidationException("La firma no es valida o no existe");
            }
            try {
                if (Firm.SecretSigner.class.isAssignableFrom(firmClass)) {
                    byte[] keyBytes = ((String[]) (String[]) this.firm.getSigner().getKey())[0].getBytes();
                    parser.setSigningKey(keyBytes);
                }
                if (Firm.Base64EncodedSecretKeySigner.class.isAssignableFrom(firmClass)) {
                    String key = ((String[]) (String[]) this.firm.getSigner().getKey())[0];
                    parser.setSigningKey(key);
                }
                if (Firm.KeySigner.class.isAssignableFrom(firmClass)) {
                    Firm.Signer signer = this.firm.getSigner();
                    Key[] keys = (Key[]) signer.getKey();
                    parser.setSigningKey(keys[0]);
                }
            } catch (IllegalArgumentException iae) {
                ValidatorToken.log.error("No se especifico un key para validar Tokens", iae);
                throw new TokenValidationException("Se requiere un KEY para validar tokens JWT", iae);
            }
            return parser;
        }
    }
}