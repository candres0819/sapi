package com.pragma.sapi.token;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;

import com.pragma.sapi.token.exception.UUIDGenerationException;

public class UUIDGen {

    private static final long clockSeqAndNode = makeClockSeqAndNode();
    private static final UUIDGen instance = new UUIDGen();
    private long lastNanos;

    private UUIDGen() {
        if (clockSeqAndNode == 0L) {
            throw new UUIDGenerationException("singleton instantiation is misplaced.");
        }
    }

    public static UUID getTimeUUID() {
        return new UUID(instance.createTimeSafe(), clockSeqAndNode);
    }

    public static UUID getTimeUUID(long when) {
        return new UUID(createTime(fromUnixTimestamp(when)), clockSeqAndNode);
    }

    public static UUID getTimeUUID(long when, long clockSeqAndNode) {
        return new UUID(createTime(fromUnixTimestamp(when)), clockSeqAndNode);
    }

    public static UUID getUUID(ByteBuffer raw) {
        return new UUID(raw.getLong(raw.position()), raw.getLong(raw.position() + 8));
    }

    public static byte[] decompose(UUID uuid) {
        long most = uuid.getMostSignificantBits();
        long least = uuid.getLeastSignificantBits();
        byte[] b = new byte[16];

        for (int i = 0; i < 8; ++i) {
            b[i] = (byte) ((int) (most >>> (7 - i) * 8));
            b[8 + i] = (byte) ((int) (least >>> (7 - i) * 8));
        }

        return b;
    }

    public static byte[] getTimeUUIDBytes() {
        return createTimeUUIDBytes(instance.createTimeSafe());
    }

    public static UUID minTimeUUID(long timestamp) {
        return new UUID(createTime(fromUnixTimestamp(timestamp)), -9187201950435737472L);
    }

    public static UUID maxTimeUUID(long timestamp) {
        long uuidTstamp = fromUnixTimestamp(timestamp + 1L) - 1L;
        return new UUID(createTime(uuidTstamp), 9187201950435737471L);
    }

    public static long unixTimestamp(UUID uuid) {
        return uuid.timestamp() / 10000L + -12219292800000L;
    }

    public static long microsTimestamp(UUID uuid) {
        return uuid.timestamp() / 10L + -12219292800000000L;
    }

    private static long fromUnixTimestamp(long timestamp) {
        return (timestamp - -12219292800000L) * 10000L;
    }

    public static byte[] getTimeUUIDBytes(long timeMillis, int nanos) {
        if (nanos >= 10000) {
            throw new IllegalArgumentException();
        } else {
            return createTimeUUIDBytes(instance.createTimeUnsafe(timeMillis, nanos));
        }
    }

    private static byte[] createTimeUUIDBytes(long msb) {
        byte[] uuidBytes = new byte[16];

        int i;
        for (i = 0; i < 8; ++i) {
            uuidBytes[i] = (byte) ((int) (msb >>> 8 * (7 - i)));
        }

        for (i = 8; i < 16; ++i) {
            uuidBytes[i] = (byte) ((int) (clockSeqAndNode >>> 8 * (7 - i)));
        }

        return uuidBytes;
    }

    public static long getAdjustedTimestamp(UUID uuid) {
        if (uuid.version() != 1) {
            throw new IllegalArgumentException("incompatible with uuid version: " + uuid.version());
        } else {
            return uuid.timestamp() / 10000L + -12219292800000L;
        }
    }

    private static long makeClockSeqAndNode() {
        long clock = (new SecureRandom()).nextLong();
        long lsb = 0L;
        lsb |= Long.MIN_VALUE;
        lsb |= (clock & 16383L) << 48;
        lsb |= makeNode();
        return lsb;
    }

    private static long createTime(long nanosSince) {
        long msb = 0L;
        msb |= (4294967295L & nanosSince) << 32;
        msb |= (281470681743360L & nanosSince) >>> 16;
        msb |= (-281474976710656L & nanosSince) >>> 48;
        msb |= 4096L;
        return msb;
    }

    private static long makeNode() {
        Collection<InetAddress> localAddresses = getAllLocalAddresses();
        if (localAddresses.isEmpty()) {
            throw new UUIDGenerationException("Cannot generate the node component of the UUID because cannot retrieve any IP addresses.");
        } else {
            byte[] hash = hash(localAddresses);
            long node = 0L;

            for (int i = 0; i < Math.min(6, hash.length); ++i) {
                node |= (255L & (long) hash[i]) << (5 - i) * 8;
            }
            assert (-72057594037927936L & node) == 0L;
            return node | 1099511627776L;
        }
    }

    private static byte[] hash(Collection<InetAddress> data) {
        try {
            MessageDigest nsae = MessageDigest.getInstance("SHA-256");
            Iterator<InetAddress> arg1 = data.iterator();

            while (arg1.hasNext()) {
                InetAddress addr = (InetAddress) arg1.next();
                nsae.update(addr.getAddress());
            }

            return nsae.digest();
        } catch (NoSuchAlgorithmException arg3) {
            throw new UUIDGenerationException("SHA-256 digest algorithm is not available", arg3);
        }
    }

    private static Collection<InetAddress> getAllLocalAddresses() {
        HashSet<InetAddress> localAddresses = new HashSet<InetAddress>();
        try {
            Enumeration<?> e = NetworkInterface.getNetworkInterfaces();
            if (e != null) {
                while (e.hasMoreElements()) {
                    localAddresses.addAll(Collections.list(((NetworkInterface) e.nextElement()).getInetAddresses()));
                }
            }

            return localAddresses;
        } catch (SocketException arg1) {
            throw new AssertionError(arg1);
        }
    }

    private synchronized long createTimeSafe() {
        long nanosSince = (System.currentTimeMillis() - -12219292800000L) * 10000L;
        if (nanosSince > this.lastNanos) {
            this.lastNanos = nanosSince;
        } else {
            nanosSince = ++this.lastNanos;
        }

        return createTime(nanosSince);
    }

    private long createTimeUnsafe(long when, int nanos) {
        long nanosSince = (when - -12219292800000L) * 10000L + (long) nanos;
        return createTime(nanosSince);
    }
}