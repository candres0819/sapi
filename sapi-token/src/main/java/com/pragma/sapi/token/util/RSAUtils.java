package com.pragma.sapi.token.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pragma.sapi.token.exception.FirmaNulaException;

import io.jsonwebtoken.impl.TextCodec;

public class RSAUtils {

    private static Logger log = LoggerFactory.getLogger(RSAUtils.class);

    public static final String CLASSPATH_PREFIX = "classpath:";

    private static String getKey(String filename) throws IOException {
        String fileNamelocal;
        if (filename.startsWith("classpath:")) {
            fileNamelocal = stripPrefix(filename);
            fileNamelocal = Objects.requireNonNull(ClassUtils.getClassLoader().getResource(fileNamelocal)).getFile();
        } else {
            fileNamelocal = filename;
        }

        log.trace("Obteniendo key de archivo [{}]", fileNamelocal);
        StringBuilder strKeyPEM = new StringBuilder("");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileNamelocal));
            String line;
            while ((line = br.readLine()) != null) {
                strKeyPEM.append(line).append("\n");
            }
        } catch (Exception e) {
            log.error("Obteniendo key de archivo [{}]", e);
        } finally {
            if (null != br) {
                br.close();
            }
        }
        return strKeyPEM.toString();
    }

    private static String stripPrefix(String resourcePath) {
        return resourcePath.substring(resourcePath.indexOf(58) + 1);
    }

    public static RSAPrivateKey getPrivateKey(String filename) throws FirmaNulaException {
        try {
            String ioe = getKey(filename);
            return getPrivateKeyFromString(ioe);
        } catch (IOException e) {
            log.error("Error obteniendo la clave privada", e);
            throw new FirmaNulaException(e.getMessage(), e);
        }
    }

    private static RSAPrivateKey getPrivateKeyFromString(String key) throws FirmaNulaException {
        try {
            String gse = key.replace("-----BEGIN PRIVATE KEY-----\n", "");
            gse = gse.replace("-----END PRIVATE KEY-----", "");
            byte[] encoded = TextCodec.BASE64.decode(gse);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            return (RSAPrivateKey) kf.generatePrivate(keySpec);
        } catch (GeneralSecurityException e) {
            log.error("Error extrayendo la clave de la cadena", e);
            throw new FirmaNulaException(e.getMessage(), e);
        }
    }

    public static RSAPublicKey getPublicKey(String filename) throws FirmaNulaException {
        try {
            String ioe = getKey(filename);
            return getPublicKeyFromString(ioe);
        } catch (IOException e) {
            log.error("Error obteniendo la clave publica", e);
            throw new FirmaNulaException(e.getMessage(), e);
        }
    }

    private static RSAPublicKey getPublicKeyFromString(String key) throws FirmaNulaException {
        try {
            String gse = key.replace("-----BEGIN PUBLIC KEY-----\n", "");
            gse = gse.replace("-----END PUBLIC KEY-----", "");
            byte[] encoded = TextCodec.BASE64.decode(gse);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(encoded));
        } catch (GeneralSecurityException e) {
            log.error("Error", e);
            throw new FirmaNulaException(e.getMessage(), e);
        }
    }

    public static RSAPrivateKey getPrivateKeyFromKS(String filename, String passwd, String certAlias, String certPasswd)
            throws FirmaNulaException {
        return (RSAPrivateCrtKey) getKeyFromKS(filename, passwd, certAlias, certPasswd);
    }

    public static RSAPublicKey getPublicKeyFromKS(String filename, String passwd, String certAlias) throws FirmaNulaException {
        return (RSAPublicKey) getKeyFromKS(filename, passwd, certAlias, (String) null);
    }

    private static String getKSFileName(String keyStoreFileName) {
        if (null != keyStoreFileName && !"".equals(keyStoreFileName)) {
            String keyStoreFileNameLocal;
            if (keyStoreFileName.startsWith("classpath:")) {
                keyStoreFileNameLocal = stripPrefix(keyStoreFileName);
                keyStoreFileNameLocal = Objects.requireNonNull(ClassUtils.getClassLoader().getResource(keyStoreFileNameLocal)).getFile();
            } else {
                keyStoreFileNameLocal = keyStoreFileName;
            }

            log.trace("Abriendo Keystore [{}]", keyStoreFileNameLocal);
            return keyStoreFileNameLocal;
        } else {
            throw new FirmaNulaException("Argumento keyStoreFile es obligatorio");
        }
    }

    private static Key getKeyFromKS(String keyStoreFileName, String keyStorePwd, String certAlias, String certPwd)
            throws FirmaNulaException {
        try {
            FileInputStream gse = new FileInputStream(getKSFileName(keyStoreFileName));
            Throwable e = null;

            try {
                if (null != certAlias && !"".equals(certAlias)) {
                    boolean isPrivate;
                    isPrivate = certPwd != null;

                    char[] password = keyStorePwd.toCharArray();
                    log.trace("Obteniendo key del alias [{}]", certAlias);
                    KeyStore keystore = KeyStore.getInstance("JKS");
                    keystore.load(gse, password);

                    if (isPrivate) {
                        return keystore.getKey(certAlias, certPwd.trim().toCharArray());
                    } else {
                        Certificate cert = keystore.getCertificate(certAlias);
                        PublicKey publicKey;
                        if (null == cert) {
                            log.warn("Certificado con alias [{}] no encontrado", certAlias);
                            publicKey = null;
                            return null;
                        } else {
                            publicKey = cert.getPublicKey();
                            return publicKey;
                        }
                    }
                } else {
                    throw new FirmaNulaException("Argumento certAlias son obligatorio");
                }
            } catch (Throwable e1) {
                e = e1;
                throw e1;
            } finally {
                try {
                    gse.close();
                } catch (Throwable e2) {
                    e.addSuppressed(e2);
                }
            }
        } catch (IOException e3) {
            log.error("Se presento un error de IO", e3);
            throw new FirmaNulaException("Error de IO", e3);
        } catch (GeneralSecurityException e4) {
            log.error("Error General", e4);
            throw new FirmaNulaException(e4.getMessage(), e4);
        }
    }
}