package com.pragma.sapi.token.util;

public interface ClassUtils {

    static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

}