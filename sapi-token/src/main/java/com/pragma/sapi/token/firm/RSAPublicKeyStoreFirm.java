package com.pragma.sapi.token.firm;

import com.pragma.sapi.token.firm.Firm.KeySigner;
import com.pragma.sapi.token.firm.Firm.KeySigner.Type;

public class RSAPublicKeyStoreFirm extends Firm<KeySigner> {

    private String path;
    private String passwd;
    private String certAlias;

    public RSAPublicKeyStoreFirm() {
        super();
    }

    public RSAPublicKeyStoreFirm(String path) {
        this.path = path;
    }

    public KeySigner getSigner() {
        return new KeySigner(this.path, Type.PUBLIC_JKS, this.passwd, this.certAlias);
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPasswd() {
        return this.passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getCertAlias() {
        return this.certAlias;
    }

    public void setCertAlias(String certAlias) {
        this.certAlias = certAlias;
    }
}