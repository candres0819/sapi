package com.pragma.sapi.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SapiTokenTest {

    final private static Logger log = LoggerFactory.getLogger(SapiTokenTest.class);

    private SapiTokenTest() {
        super();
        log.info("Constructor");
    }

}